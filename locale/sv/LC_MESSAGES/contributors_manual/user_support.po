# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-09 03:10+0200\n"
"PO-Revision-Date: 2019-07-09 19:06+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../contributors_manual/user_support.rst:1
msgid "Introduction to user support."
msgstr "Introduktion till användarsupport."

#: ../../contributors_manual/user_support.rst:20
msgid "Introduction to User Support"
msgstr "Introduktion till användarsupport"

#: ../../contributors_manual/user_support.rst:35
msgid "Contents"
msgstr "Innehåll"

#: ../../contributors_manual/user_support.rst:38
msgid "Tablet Support"
msgstr "Stöd för ritplatta"

#: ../../contributors_manual/user_support.rst:40
msgid ""
"The majority of help requests are about pen pressure and tablet support in "
"general."
msgstr ""
"Majoriteten av hjälpförfrågningarna handlar om penntryck och stöd för "
"ritplattor i allmänhet."

#: ../../contributors_manual/user_support.rst:44
msgid "Quick solutions"
msgstr "Snabba lösningar"

#: ../../contributors_manual/user_support.rst:46
msgid ""
"On Windows: reinstall your driver (Windows Update often breaks tablet driver "
"settings, reinstallation helps)."
msgstr ""
"På Windows: installera om drivrutinen (Windows uppdateringar förstör ofta "
"inställningarna av ritplattans drivrutin, att installera om hjälper)."

#: ../../contributors_manual/user_support.rst:48
msgid ""
"Change API in :menuselection:`Settings --> Configure Krita --> Tablet "
"Settings` (for some devices, especially N-trig ones, Windows Ink work "
"better, for some it's Wintab)."
msgstr ""
"Ändra programmeringsgränssnitt med :menuselection:`Anpassa Krita -> "
"Inställningar av ritplatta` (för vissa enheter, särskilt från N-trig, "
"fungerar Windows Ink bättre, för andra är det Wintab)."

#: ../../contributors_manual/user_support.rst:50
msgid ""
"On Windows, Wacom tablets: if you get straight lines at the beginnings of "
"the strokes, disable/minimize \"double-click distance\" in Wacom settings."
msgstr ""
"På WIndows, för Wacom ritplattor: om man får räta linjer i början av streck, "
"inaktivera eller minimera 'double-click distance' i Wacom inställningarna."

#: ../../contributors_manual/user_support.rst:53
msgid "Gathering information"
msgstr "Informationsinsamling"

#: ../../contributors_manual/user_support.rst:55
msgid "Which OS do you use?"
msgstr "Vilket operativsystem använder du?"

#: ../../contributors_manual/user_support.rst:57
msgid "Which tablet do you have?"
msgstr "Vilken ritplatta har du?"

#: ../../contributors_manual/user_support.rst:59
msgid "What is the version of the tablet driver?"
msgstr "Vilken version har ritplattans drivrutin?"

#: ../../contributors_manual/user_support.rst:61
msgid ""
"Please collect Tablet Tester (:menuselection:`Settings --> Configure Krita --"
"> Tablet Settings`) output, paste it to `Pastebin <https://pastebin.com/>`_ "
"or similar website and give us a link."
msgstr ""
"Samla in utdata från Test av ritplatta (:menuselection:`Anpassa Krita -> "
"Inställningar av ritplatta`), klistra in det på `Pastebin <https://pastebin."
"com/>`_ eller liknande webbplats och ge en länk till oss."

#: ../../contributors_manual/user_support.rst:65
msgid "Additional information for supporters"
msgstr "Ytterligare information för supportpersoner"

#: ../../contributors_manual/user_support.rst:67
msgid ""
"Except for the issue with beginnings of the strokes, Wacom tablets usually "
"work no matter the OS."
msgstr ""
"Utom för problemet med början av strecken, ska Wacom ritplattor oftast "
"fungera oberoende av operativsystem."

#: ../../contributors_manual/user_support.rst:69
msgid ""
"Huion tablets should work on Windows and on Linux, on Mac there might be "
"issues."
msgstr ""
"Huion ritplattor ska fungera på Windows och Linux, på Mac kan det finnas "
"problem."

#: ../../contributors_manual/user_support.rst:71
msgid "XP-Pen tablets and other brands can have issues everywhere."
msgstr "XP-Pen ritplattor och andra märken kan ha problem överallt."

#: ../../contributors_manual/user_support.rst:73
msgid ""
"If someone asks about a tablet to buy, generally a cheaper Wacom or a Huion "
"are the best options as of 2019, if they want to work with Krita. :ref:"
"`list_supported_tablets`"
msgstr ""
"Om någon frågar efter en ritplatta att köpa, är i allmänhet en billigare "
"Wacom eller Huion är de bästa alternativen från och med 2019, om de vill "
"arbeta med Krita. Se :ref:`list_supported_tablets`."

#: ../../contributors_manual/user_support.rst:75
msgid ""
"`Possibly useful instruction in case of XP-Pen tablet issues <https://www."
"reddit.com/r/krita/comments/btzh72/"
"xppen_artist_12s_issue_with_krita_how_to_fix_it/>`_."
msgstr ""
"`Möjligtvis användbara instruktioner i händelse av problem med XP Pen "
"ritplattor <https://www.reddit.com/r/krita/comments/btzh72/"
"xppen_artist_12s_issue_with_krita_how_to_fix_it/>`_."

#: ../../contributors_manual/user_support.rst:79
msgid "Animation"
msgstr "Animering"

#: ../../contributors_manual/user_support.rst:81
msgid ""
"Issues with rendering animation can be of various shapes and colors. First "
"thing to find out is whether the issue happens on Krita's or FFmpeg's side "
"(Krita saves all the frames, then FFmpeg is used to render a video using "
"this sequence of images). To learn that, instruct the user to render as "
"\"Image Sequence\". If the image sequence is correct, FFmpeg (or more often: "
"render options) are at fault. If the image sequence is incorrect, either the "
"options are wrong (if for example not every frame got rendered), or it's a "
"bug in Krita."
msgstr ""
"Problem med att återge animeringar kan ha olika färger och former. Det "
"första att ta reda på är om problemet uppstår på Krita- eller FFmpeg-sidan "
"(Krita sparar alla bildrutor, och sedan används FFmpeg för att återge en "
"video med användning av bildsekvensen). För att ta reda på det, be "
"användaren att återge som \"Bildsekvens\". Om bildsekvensen är korrekt, "
"ligger felet i FFmpeg (eller oftare i  återgivningsväljarna). Om "
"bildsekvensen är felaktig, är antingen alternativen fel (om exempelvis inte "
"alla bildrutor återgavs), eller så är det en fel i Krita."

#: ../../contributors_manual/user_support.rst:85
msgid ""
"If the user opens the Log Viewer docker, turns on logging and then tries to "
"render a video, Krita will print out the whole ffmpeg command to Log Viewer "
"so it can be easily investigated."
msgstr ""
"Om användaren öppnar loggvisningspanelen, sätter på loggning och därefter "
"försöker återge videon, skriver Krita ut hela ffmpeg-kommandot i "
"loggvisningen så att det enkelt kan undersökas."

#: ../../contributors_manual/user_support.rst:87
msgid ""
"There is a log file in the directory that user tries to render to. It can "
"contain information useful to investigation of the issue."
msgstr ""
"Det finns en loggfil i katalogen som användaren försöker återge i. Den kan "
"innehålla information användbar för undersökning av problemet."

#: ../../contributors_manual/user_support.rst:90
msgid "Onion skin issues"
msgstr "Problem med rispapper"

#: ../../contributors_manual/user_support.rst:92
msgid ""
"The great majority of issues with onion skin are just user errors, not bugs. "
"Nonetheless, you need to find out why it happens and direct the user how to "
"use onion skin properly."
msgstr ""
"De allra flesta problemen med rispapper är bara användningsfel, inte "
"programfel. Trots det, måste du ta reda på varför det inträffar och tala om "
"för användaren hur man använder rispapper på ett riktigt sätt."

#: ../../contributors_manual/user_support.rst:96
msgid "Crash"
msgstr "Krasch"

#: ../../contributors_manual/user_support.rst:98
msgid ""
"In case of crash try to determine if the problem is known, if not, instruct "
"user to create a bug report (or create it yourself) with following "
"information:"
msgstr ""
"I fallet med en krasch, försök att avgöra om problemet är känt. Om inte, "
"anvisa användaren att skapa en felrapport (eller skapa den själv) med "
"följande information:"

#: ../../contributors_manual/user_support.rst:100
msgid "What happened, what was being done just before the crash."
msgstr "Vad hände, vad gjordes precis innan kraschen."

#: ../../contributors_manual/user_support.rst:102
msgid ""
"Is it possible to reproduce (repeat)? If yes, provide a step-by-step "
"instruction to get the crash."
msgstr ""
"Är den möjligt att reproducera (upprepa)? Om ja, tillhandahåll en stegvis "
"instruktion för att ge kraschen."

#: ../../contributors_manual/user_support.rst:104
msgid ""
"Backtrace (crashlog) -- the instruction is here: :ref:`dr_minw`, and the "
"debug symbols can be found in the annoucement of the version of Krita that "
"the user has. But it could be easier to just point the user to `https://"
"download.kde.org/stable/krita <https://download.kde.org/stable/krita>`_."
msgstr ""
"Bakåtspårning (kraschlogg): instruktionerna finns här :ref:`dr_minw`, och "
"felsökningssymbolerna finns i tillkännagivandet om den version av Krita som "
"användaren har. Men det skulle kunna vara enklare att bara peka användaren "
"på  `https://download.kde.org/stable/krita <https://download.kde.org/stable/"
"krita>`_."

#: ../../contributors_manual/user_support.rst:108
msgid "Other possible questions with quick solutions"
msgstr "Andra möjliga frågor med snabba lösningar"

#: ../../contributors_manual/user_support.rst:110
msgid ""
"When the user has trouble with anything related to preview or display, ask "
"them to change :guilabel:`Canvas Graphics Acceleration` in :menuselection:"
"`Settings --> Configure Krita --> Display`."
msgstr ""
"När användaren har problem med något relaterat till förhandsgranskning eller "
"visning, be dem att ändra :guilabel:`Dukens grafikacceleration` med :"
"menuselection:`Anpassa Krita -> Bildskärm`."

#: ../../contributors_manual/user_support.rst:116
msgid ""
"When the user has any weird issue, something you've never heard about, ask "
"them to reset the configuration: :ref:`faq_reset_krita_configuration`."
msgstr ""
"När användaren har något konstigt problem, något som du aldrig har hört "
"talas om, be dem att nollställa inställningen: :ref:"
"`faq_reset_krita_configuration`."

#: ../../contributors_manual/user_support.rst:120
msgid "Advices for supporters"
msgstr "Råd till supportpersoner"

#: ../../contributors_manual/user_support.rst:122
msgid ""
"If you don't understand the question, ask for clarification -- asking for a "
"screen recording or a screenshot is perfectly fine."
msgstr ""
"Om du inte förstår frågan, be om en förklaring: det går utmärkt att fråga om "
"en skärminspelning eller skärmbild."

#: ../../contributors_manual/user_support.rst:124
msgid ""
"If you don't know the solution but you know what information will be needed "
"to investigate the issue further, don't hesitate to ask. Other supporters "
"may know the answer, but have too little time to move the user through the "
"whole process, so you're helping a lot just by asking for additional "
"information. This is very much true in case of tablet issues, for example."
msgstr ""
"Om du inte vet lösningen, men vet vilken information som behövs för att "
"undersöka problemet vidare, tveka inte om att fråga. Andra supportpersoner "
"kan veta svaret, men ha för ont om tid för att leda användaren genom hela "
"processen, så du hjälper till en hel del genom att bara fråga efter "
"ytterligare information. Det gäller exempelvis i stor utsträckning för "
"problem med ritplattor."

#: ../../contributors_manual/user_support.rst:126
msgid ""
"If you don't know the answer/solution and the question looks abandoned by "
"other supporters, you can always ask for help on Krita IRC channel. It's "
"#krita on freenote.net: :ref:`the_krita_community`."
msgstr ""
"Om du inte vet svaret eller lösningen, och frågan verkar vara övergiven av "
"andra supportpersoner, kan du alltid fråga efter hjälp på Kritas IRC-kanal. "
"Den är #krita på freenote.net: :ref:`the_krita_community`."

#: ../../contributors_manual/user_support.rst:128
msgid ""
"Explain steps the user needs to make clearly, for example if you need them "
"to change something in settings, clearly state the whole path of buttons and "
"tabs to get there."
msgstr ""
"Förklara tydligt de steg som användaren måste utföra. Om de exempelvis måste "
"ändra någonting i inställningarna, förklara tydligt hela följden av knappar "
"och flikar för att komma dit."

#: ../../contributors_manual/user_support.rst:130
msgid ""
"Instead of :menuselection:`Settings --> Configure Krita` use just :"
"menuselection:`Configure Krita` -- it's easy enough to find and Mac users "
"(where you need to select :menuselection:`Krita --> Settings`) won't get "
"confused."
msgstr ""
"Istället för :menuselection:`Inställningar -> Anpassa Krita` använd bara :"
"menuselection:`Anpassa Krita`: Det är enkelt nog att hitta och Mac-användare "
"(som måste välja :menuselection:`Krita -> Inställningar`) blir inte "
"förvirrade."

#: ../../contributors_manual/user_support.rst:132
msgid ""
"If you ask for an image, mention usage of `Imgur <https://imgur.com>`_ or "
"`Pasteboard <https://pasteboard.co>`_, otherwise Reddit users might create a "
"new post with this image instead of including it to the old conversation."
msgstr ""
"Om du frågar efter en bild, nämn användning av `Imgur <https://imgur.com>`_ "
"eller `Pasteboard <https://pasteboard.co>`_, annars kanske användare av "
"Reddit skapar ett nytt inlägg med bilden istället för att inkludera den i "
"den gamla konversationen."

#: ../../contributors_manual/user_support.rst:134
msgid ""
"If you want to quickly answer someone, just link to the appropriate place in "
"this manual page -- you can click on the little link icon next to the "
"section or subsection title and give the link to the user so they for "
"example know what information about their tablet issue you need."
msgstr ""
"Om du vill svara någon snabbt, länka bara till lämplig plats på den här "
"handbokssidan: du kan klicka på den lilla ikonen intill avsnittets eller "
"delavsnittets titel och ge länken till användaren så att de exempelvis vet "
"vilken information som du behöver om deras problem med ritplattan."

#: ../../contributors_manual/user_support.rst:136
msgid ""
"If the user access the internet from the country or a workplace with some of "
"the websites blocked (like imgur.com or pastebin.com), here is a list of "
"alternatives that works:"
msgstr ""
"Om användaren använder Internet från ett land eller arbetsplats med vissa "
"webbplatser blockerade (som imgur.com eller pastebin.com). Här är en lista "
"av alternativ som fungerar:"

#: ../../contributors_manual/user_support.rst:138
msgid "Images (e.g. screenshots): `Pasteboard <https://pasteboard.co>`_"
msgstr "Bilder (t.ex. skärmbilder): `Pasteboard <https://pasteboard.co>`_"

#: ../../contributors_manual/user_support.rst:140
msgid ""
"Text only: `BPaste <https://bpaste.net>`_, `paste.ubuntu.org.cn <paste."
"ubuntu.org.cn>`_, `paste.fedoraproject.org <https://paste.fedoraproject.org/"
">`_ or `https://invent.kde.org/dashboard/snippets (needs KDE Identity) "
"<https://invent.kde.org/dashboard/snippets>`_."
msgstr ""
"Bara text: `BPaste <https://bpaste.net>`_, `paste.ubuntu.org.cn <paste."
"ubuntu.org.cn>`_, `paste.fedoraproject.org <https://paste.fedoraproject.org/"
">`_, eller `https://invent.kde.org/dashboard/snippets (kräver en KDE-"
"identitet) <https://invent.kde.org/dashboard/snippets>`_."

#: ../../contributors_manual/user_support.rst:142
msgid ""
"``.kra`` and other formats: by mail? Or encode the file using `base64` "
"command on Linux, send by mail or on Pastebin, then decode using the same "
"command."
msgstr ""
"För ``.kra`` och andra format: med e-post? Eller koda filen med kommandot "
"`base64` på Linux, skicka via e-post eller med Pastebin, avkoda sedan med "
"användning av samma kommando."

#: ../../contributors_manual/user_support.rst:147
msgid ""
"If you ask user to store their log or other data on a website, make sure it "
"stays there long enough for you to get it -- for example bpaste.net stores "
"files by default only for a day! And you can extend it only to one week."
msgstr ""
"Om du ber en användare att lagra sin logg eller annan data på en webbplats, "
"säkerställ att den förblir där länga nog så att du kan hämta den. Exempelvis "
"lagrar bpaste.net normalt bara filer ett dygn, och du kan bara utöka det "
"till en vecka."

#: ../../contributors_manual/user_support.rst:149
msgid ""
"Make sure they don't post their personal data. Tablet Tester log is safe, "
"log from the :menuselection:`Help -> Show system information for bug "
"reports` might not be that safe. Maybe you could ask them to send it to you "
"by mail?"
msgstr ""
"Säkerställ att de inte skickar in personlig information. Loggen från Test av "
"ritplatta är säker, medan loggen från :menuselection:`Hjälp -> Visa "
"systeminformation för felrapporter` inte är så säker. Kanske du kan be dem "
"att skicka den till dig via e-post?"
