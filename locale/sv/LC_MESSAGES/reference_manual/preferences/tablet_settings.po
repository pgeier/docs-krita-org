# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:14+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/preferences/tablet_settings.rst:1
msgid "Configuring the tablet in Krita."
msgstr "Anpassa ritplattan i Krita."

#: ../../reference_manual/preferences/tablet_settings.rst:12
#: ../../reference_manual/preferences/tablet_settings.rst:21
msgid "Tablet"
msgstr "Ritplatta"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Preferences"
msgstr "Anpassning"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Settings"
msgstr "Inställningar"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Pressure Curve"
msgstr "Tryckkurva"

#: ../../reference_manual/preferences/tablet_settings.rst:17
msgid "Tablet Settings"
msgstr "Inställningar av ritplatta"

#: ../../reference_manual/preferences/tablet_settings.rst:20
msgid ".. image:: images/preferences/Krita_Preferences_Tablet_Settings.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Tablet_Settings.png"

#: ../../reference_manual/preferences/tablet_settings.rst:22
msgid ""
"Input Pressure Global Curve : This is the global curve setting that your "
"tablet will use in Krita. The settings here will make your tablet feel soft "
"or hard globally."
msgstr ""
"Allmän kurva för inmatningstryck: Det är den allmänna kurvinställningen som "
"ritplattan använder i Krita. Inställningarna här gör att ritplattan i "
"allmänhet känns mjuk eller hård."

#: ../../reference_manual/preferences/tablet_settings.rst:24
msgid ""
"Some tablet devices don't tell us whether the side buttons on a stylus. If "
"you have such a device, you can try activate this workaround. Krita will try "
"to read right and middle-button clicks as if they were coming from a mouse "
"instead of a tablet. It may or may not work on your device (depends on the "
"tablet driver implementation). After changing this option Krita should be "
"restarted."
msgstr ""
"Vissa ritplattor talar inte om för oss om det finns sidoknappar på en penna. "
"Om du har en sådan enhet, kan du försöka aktivera den här lösningen. Krita "
"försöker läsa klick på höger- och mittenknapp som om de kom från en mus "
"istället för en ritplatta. Det kan eventuellt fungera för din enhet "
"(beroende på implementeringen av ritplattans drivrutin). Efter att "
"alternativet har ändrats måste Krita startas om."

#: ../../reference_manual/preferences/tablet_settings.rst:26
msgid "Use Mouse Events for Right and Middle clicks."
msgstr "Använd mushändelser för höger- och mittenklick."

#: ../../reference_manual/preferences/tablet_settings.rst:29
msgid "On Windows 8 or above only."
msgstr "Bara på Windows 8 eller senare."

#: ../../reference_manual/preferences/tablet_settings.rst:31
msgid "WinTab"
msgstr "WinTab"

#: ../../reference_manual/preferences/tablet_settings.rst:32
msgid ""
"Use the WinTab API to receive tablet pen input. This is the API being used "
"before Krita 3.3. This option is recommended for most Wacom tablets."
msgstr ""
"Använd programmeringsgränssnittet WinTab för att ta emot inmatning från "
"ritplattans penna. Det är programmeringsgränssnittet som användes innan "
"Krita 3.3. Alternativet rekommenderas för de flesta Wacom ritplattor."

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid "For Krita 3.3 or later:Tablet Input API"
msgstr ""
"För Krita 3.3 eller senare: Programmeringsgränssnitt för inmatning med "
"ritplatta"

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid "Windows 8+ Pointer Input"
msgstr "Windows 8+ pekarinmatning"

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid ""
"Use the Pointer Input messages to receive tablet pen input. This option "
"depends on Windows Ink support from the tablet driver. This is a relatively "
"new addition so it's still considered to be experimental, but it should work "
"well enough for painting. You should try this if you are using an N-Trig "
"device (e.g. recent Microsoft Surface devices) or if your tablet does not "
"work well with WinTab."
msgstr ""
"Använd pekarinmatningsmeddelanden för att ta emot indata från ritplattans "
"penna. Alternativet beror på stöd för Windows Ink från ritplattans "
"drivrutin. Det är ett relativt nytt tillägg, så det anses fortfarande "
"experimentellt, men det bör fungera bra nog för att måla. Man bör prova det "
"om man använder en N-Trig enhet (t.ex. senare Microsoft Surface enheter) "
"eller om ritplattan inte fungerar bra med WinTab."

#: ../../reference_manual/preferences/tablet_settings.rst:37
msgid "Advanced Tablet Settings for WinTab"
msgstr "Avancerade inställningar av ritplatta för WinTab"

#: ../../reference_manual/preferences/tablet_settings.rst:41
msgid ".. image:: images/preferences/advanced-settings-tablet.png"
msgstr ".. image:: images/preferences/advanced-settings-tablet.png"

#: ../../reference_manual/preferences/tablet_settings.rst:42
msgid ""
"When using multiple monitors or using a tablet that is also a screen, Krita "
"will get conflicting information about how big your screen is, and sometimes "
"if it has to choose itself, there will be a tablet offset. This window "
"allows you to select the appropriate screen resolution."
msgstr ""
"Vid användning av flera bildskärmar eller en ritplatta som också är en "
"skärm, får Krita motstridig information om hur stor skärmen är, och ibland "
"uppstår en förskjutning av ritplattan om det får välja själv. Det här "
"fönstret gör det möjligt att välja lämplig skärmupplösning."

#: ../../reference_manual/preferences/tablet_settings.rst:44
msgid "Use Information Provided by Tablet"
msgstr "Använd information som ritplattan tillhandahåller"

#: ../../reference_manual/preferences/tablet_settings.rst:45
msgid "Use the information as given by the tablet."
msgstr "Använd informationen som anges av ritplattan."

#: ../../reference_manual/preferences/tablet_settings.rst:46
msgid "Map to entire virtual screen"
msgstr "Avbilda på hela virtuella skärmen"

#: ../../reference_manual/preferences/tablet_settings.rst:47
msgid "Use the information as given by Windows."
msgstr "Använd informationen som anges av Windows."

#: ../../reference_manual/preferences/tablet_settings.rst:49
msgid ""
"Type in the numbers manually. Use this when you have tried the other "
"options. You might even need to do trial and error if that is the case, but "
"at the least you can configure it."
msgstr ""
"Skriv in värdena manuellt. Använd det när när de andra alternativen redan "
"har provats. Man måste kanske till och med pröva sig fram om det är fallet, "
"men man kan åtminstone ställa in den."

#: ../../reference_manual/preferences/tablet_settings.rst:51
msgid "Map to Custom Area"
msgstr "Avbilda på eget område"

#: ../../reference_manual/preferences/tablet_settings.rst:51
msgid ""
"If you have a dual monitor setup and only the top half of the screen is "
"reachable, you might have to enter the total width of both screens plus the "
"double height of your monitor in this field."
msgstr ""
"Om man har dubbla bildskärmar och bara den övre halvan av skärmen går att "
"nå, kanske den totala bredden av båda skärmarna plus den dubbla höjden av "
"bildskärmen måste matas in i fältet."

#: ../../reference_manual/preferences/tablet_settings.rst:55
msgid ""
"To access this dialog in Krita versions older than 4.2, you had to do the "
"following:"
msgstr ""
"För att komma åt dialogrutan i versioner av Krita äldre än 4.2, var man "
"tvungen att göra följande:"

#: ../../reference_manual/preferences/tablet_settings.rst:57
msgid "Put your stylus away from the tablet."
msgstr "Avlägsna pennan från ritplattan."

#: ../../reference_manual/preferences/tablet_settings.rst:58
msgid ""
"Start Krita without using a stylus, that is using a mouse or a keyboard."
msgstr ""
"Starta Krita utan att använda en penna, det vill säga genom att använda en "
"mus eller ett tangentbord."

#: ../../reference_manual/preferences/tablet_settings.rst:59
msgid "Press the :kbd:`Shift` key and hold it."
msgstr "Tryck ner tangenten :kbd:`Skift` och håll den nere."

#: ../../reference_manual/preferences/tablet_settings.rst:60
msgid "Touch a tablet with your stylus so Krita would recognize it."
msgstr "Rör en ritplatta med pennan så att Krita känner igen det."

#: ../../reference_manual/preferences/tablet_settings.rst:62
msgid ""
"If adjusting this doesn't work, and if you have a Wacom tablet, an offset in "
"the canvas can be caused by a faulty Wacom preference file which is not "
"removed or replaced by reinstalling the drivers."
msgstr ""
"Om det inte fungerar att justera det, och en Wacom-ritplatta används, kan "
"ett positionsfel på duken orsakas av en felaktig Wacom-inställningsfil, som "
"inte tas bort eller ersätts genom att installera om drivrutinerna."

#: ../../reference_manual/preferences/tablet_settings.rst:64
msgid ""
"To fix it, use the “Wacom Tablet Preference File Utility” to clear all the "
"preferences. This should allow Krita to detect the correct settings "
"automatically."
msgstr ""
"För att rätta det, använd \"Wacom Tablet Preference File Utility\" för att "
"rensa alla inställningar. Det ska låta Krita detektera de riktiga "
"inställningarna automatiskt."

#: ../../reference_manual/preferences/tablet_settings.rst:67
msgid ""
"Clearing all wacom preferences will reset your tablet's configuration, thus "
"you will need to recalibrate/reconfigure it."
msgstr ""
"Att rensa alla Wacom-inställningar nollställer ritplattans inställning, och "
"därmed måste den kalibreras om och ställas in igen."

#: ../../reference_manual/preferences/tablet_settings.rst:70
msgid "Tablet Tester"
msgstr "Test av ritplatta"

#: ../../reference_manual/preferences/tablet_settings.rst:74
msgid ""
"This is a special feature for debugging tablet input. When you click on it, "
"it will open a window with two sections. The left section is the **Drawing "
"Area** and the right is the **Text Output**."
msgstr ""
"Det här är en särskild funktion för att felsöka inmatning från ritplattor. "
"När man klickar på det visas en fönster med två delar. Den vänstra delen är "
"**ritområdet** och den högra är **textutmatning**."

#: ../../reference_manual/preferences/tablet_settings.rst:76
msgid ""
"If you draw over the Drawing Area, you will see a line appear. If your "
"tablet is working it should be both a red and blue line."
msgstr ""
"Om man ritar på ritområdet ser man en linje som dyker upp. Om ritplattan "
"fungerar ska det vara både en röd och blå linje."

#: ../../reference_manual/preferences/tablet_settings.rst:78
msgid ""
"The red line represents mouse events. Mouse events are the most basic events "
"that Krita can pick up. However, mouse events have crude coordinates and "
"have no pressure sensitivity."
msgstr ""
"Den röda linjen representerar mushändelser. Mushändelser är de mest "
"grundläggande händelserna som Krita kan fånga upp. Dock har mushändelser "
"grova koordinater och ingen tryckkänslighet."

#: ../../reference_manual/preferences/tablet_settings.rst:80
msgid ""
"The blue line represents the tablet events. The tablet events only show up "
"when Krita can access your tablet. These have more precise coordinates and "
"access to sensors like pressure sensitivity."
msgstr ""
"Den blåa linjen representerar ritplattehändelser. Ritplattehändelserna visas "
"bara när Krita kan komma åt ritplattan. De har mer precisa koordinater och "
"åtkomst till sensorer som tryckkänslighet."

#: ../../reference_manual/preferences/tablet_settings.rst:84
msgid ""
"If you have no blue line when drawing on the lefthand drawing area, Krita "
"cannot access your tablet. Check out the :ref:`page on drawing tablets "
"<drawing_tablets>` for suggestions on what is causing this."
msgstr ""
"Om det inte syns någon blå linje när man ritar på området till vänster, kan "
"Krita inte komma åt ritplattan. Ta en titt på :ref:`sidan om ritplattor "
"<drawing_tablets>` för förslag om vad som orsakar det."

#: ../../reference_manual/preferences/tablet_settings.rst:86
msgid ""
"When you draw a line, the output on the right will show all sorts of text "
"output. This text output can be attached to a help request or a bug report "
"to figure out what is going on."
msgstr ""
"När man ritar en linje visar utmatningen till höger alla möjliga sorters "
"textutmatning. Textutmatningen kan bifogas till en hjälpbegäran eller "
"felrapport för att kunna räkna ut vad som händer."

#: ../../reference_manual/preferences/tablet_settings.rst:89
msgid "External Links"
msgstr "Externa länkar"

#: ../../reference_manual/preferences/tablet_settings.rst:91
msgid ""
"`David Revoy wrote an indepth guide on using this feature to maximum "
"advantage. <https://www.davidrevoy.com/article182/calibrating-wacom-stylus-"
"pressure-on-krita>`_"
msgstr ""
"`David Revoy skrev en djuplodande handledning om hur funktionen används till "
"största fördel. <https://www.davidrevoy.com/article182/calibrating-wacom-"
"stylus-pressure-on-krita>`_"
