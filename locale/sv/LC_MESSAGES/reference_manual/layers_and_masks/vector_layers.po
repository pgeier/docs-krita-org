# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:10+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:None
msgid ".. image:: images/vector/Fill_rule_even-odd.svg"
msgstr ".. image:: images/vector/Fill_rule_even-odd.svg"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:None
msgid ".. image:: images/vector/Fill_rule_non-zero.svg"
msgstr ".. image:: images/vector/Fill_rule_non-zero.svg"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:1
msgid "How to use vector layers in Krita."
msgstr "Hur man använder vektorlager i Krita."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:15
msgid "Vector"
msgstr "Vektor"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:15
msgid "Layers"
msgstr "Lager"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:20
msgid "Vector Layers"
msgstr "Vektorlager"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:24
msgid ""
"This page is outdated. Check :ref:`vector_graphics` for a better overview."
msgstr ""
"Den här sidan är föråldrad. Titta på :ref:`vector_graphics` för en bättre "
"översikt."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:27
msgid "What is a Vector Layer?"
msgstr "Vad är ett vektorlager?"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:29
msgid ""
"A Vector Layers, also known as a shape layer, is a type of layers that "
"contains only vector elements."
msgstr ""
"Ett vektorlager, också känt som ett formlager, är en sorts lager som bara "
"innehåller vektorelement."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:31
msgid ""
"This is how vector layers will appear in the :program:`Krita` Layers docker."
msgstr "Så här ser vektorlager ut med lagerpanelen i :program:`Krita`."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:34
msgid ".. image:: images/vector/Vectorlayer.png"
msgstr ".. image:: images/vector/Vectorlayer.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:35
msgid ""
"It shows the vector contents of the layer on the left side. The icon showing "
"the page with the red bookmark denotes that it is a vector layer. To the "
"right of that is the layer name. Next are the layer visibility and "
"accessibility icons. Clicking the \"eye\" will toggle visibility. Clicking "
"the lock into a closed position will lock the content and editing will no "
"longer be allowed until it is clicked again and the lock on the layer is "
"released."
msgstr ""
"Den visar lagrets vektorinnehåll på vänster sida. Ikonen som visar sidan med "
"det röda bokmärket anger att det är ett vektorlager. Till höger om det finns "
"lagernamnet. Därefter finns lagrets synlighetsikon och åtkomstikon. Att "
"klicka på \"ögat\" ändrar synligheten. Att klicka så att låset hamnar i "
"stängd position låser innehållet och redigering tillåts inte till det "
"klickas igen och lagrets lås öppnas."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:38
msgid "Creating a vector layer"
msgstr "Skapa ett vektorlager"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:40
msgid ""
"You can create a vector layer in two ways. Using the extra options from the "
"\"Add Layer\" button you can click the \"Vector Layer\" item and it will "
"create a new vector layer. You can also drag a rectangle or ellipse from the "
"**Add shape** dock onto an active Paint Layer.  If the active layer is a "
"Vector Layer then the shape will be added directly to it."
msgstr ""
"Man kan skapa ett vektorlager på två sätt. Genom att använda "
"extraalternativen i knappen \"Lägg till lager\" kan man klicka på "
"alternativet \"Vektorlager\" så skapas ett nytt vektorlager. Man kan också "
"dra en rektangel eller ellips från panelen **Lägg till form** på ett aktivt "
"målarlager. Om det aktiva lagret är ett vektorlager läggs formen direkt till "
"på det."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:43
msgid "Editing Shapes on a Vector Layer"
msgstr "Redigera former på ett vektorlager"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:47
msgid ""
"There's currently a bug with the vector layers that they will always "
"consider themselves to be at 72dpi, regardless of the actual pixel-size. "
"This can make manipulating shapes a little difficult, as the precise input "
"will not allow cm or inch, even though the vector layer coordinate system "
"uses those as a basis."
msgstr ""
"För närvarande finns ett fel med vektorlager som gör att de alltid anses "
"vara 72 punkter/tum, oberoende av den verkliga bildpunktsstorleken. Det kan "
"göra det lite svårt att hantera former, eftersom den exakta inmatningen inte "
"tillåter cm eller tum, även om vektorlagrets koordinatsystem använder dem "
"som bas."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:50
msgid "Basic Shape Manipulation"
msgstr "Grundläggande formhantering"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:52
msgid ""
"To edit the shape and colors of your vector element, you will need to use "
"the basic shape manipulation tool."
msgstr ""
"Man måste använda hanteringsverktyget för grundläggande former för att "
"redigera formen och färgen på vektorelement."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:54
msgid ""
"Once you have selected this tool, click on the element you want to "
"manipulate and you will see guides appear around your shape."
msgstr ""
"När man väl har valt verktyget, klicka på elementet som ska ändras så dyker "
"hjälplinjer upp omkring formen."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:57
msgid ".. image:: images/vector/Vectorguides.png"
msgstr ".. image:: images/vector/Vectorguides.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:58
msgid ""
"There are four ways to manipulate your image using this tool and the guides "
"on your shape."
msgstr ""
"Det finns fyra sätt att ändra bilden med verktyget och hjälplinjerna på "
"formen."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:61
msgid "Transform/Move"
msgstr "Transformera och flytta"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:64
msgid ".. image:: images/vector/Transform.png"
msgstr ".. image:: images/vector/Transform.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:65
msgid ""
"This feature of the tool allows you to move your object by clicking and "
"dragging your shape around the canvas. Holding the :kbd:`Ctrl` key will lock "
"your moves to one axis."
msgstr ""
"Den här verktygsfunktionen gör att man kan flytta objektet genom att klicka "
"och dra formen på duken. Att hålla nere tangenten :kbd:`Ctrl` låser "
"förflyttningarna till en axel."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:68
msgid "Size/Stretch"
msgstr "Storleksändra och sträcka"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:71
msgid ".. image:: images/vector/Resize.png"
msgstr ".. image:: images/vector/Resize.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:72
msgid ""
"This feature of the tool allows you to stretch your shape.  Selecting a "
"midpoint will allow stretching along one axis. Selecting a corner point will "
"allow stretching across both axis. Holding the :kbd:`Shift` key will allow "
"you to scale your object. Holding the :kbd:`Ctrl` key will cause your "
"manipulation to be mirrored across your object."
msgstr ""
"Den här verktygsfunktionen gör att man kan sträcka formen. Att välja en "
"mittpunkt tillåter sträckning längs en axel. Att välja en hörnpunkt tillåter "
"sträckning längs båda axlarna. Att hålla nere tangenten :kbd:`Skift` "
"tillåter att objektet skalas. Att hålla nere tangenten :kbd:`Ctrl` gör att "
"ändringen speglas över objektet."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:75
msgid "Rotate"
msgstr "Rotera"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:78
msgid ".. image:: images/vector/Rotatevector.png"
msgstr ".. image:: images/vector/Rotatevector.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:79
msgid ""
"This feature of the tool will allow you to rotate your object around its "
"center. Holding the :kbd:`Ctrl` key will cause your rotation to lock to 45 "
"degree angles."
msgstr ""
"Den här verktygsfunktionen låter dig rotera objektet omkring dess centrum. "
"Att hålla nere tangenten :kbd:`Ctrl` gör att rotationen låses till 45 "
"graders vinklar."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:82
msgid "Skew"
msgstr "Skjuva"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:85
msgid ".. image:: images/vector/Skew.png"
msgstr ".. image:: images/vector/Skew.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:86
msgid "This feature of the tool will allow you to skew your object."
msgstr "Den här verktygsfunktionen låter dig skjuva objektet."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:90
msgid ""
"At the moment there is no way to scale only one side of your vector object. "
"The developers are aware that this could be useful and will work on it as "
"manpower allows."
msgstr ""
"För närvarande finns det inget sätt att bara skala en sida av "
"vektorobjektet. Utvecklarna är medvetna om att det kan vara användbart och "
"kommer att arbeta på det när arbetskraft finns tillgänglig."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:93
msgid "Point and Curve Shape Manipulation"
msgstr "Hantering av punkter och kurvformer"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:95
msgid ""
"Double-click on a vector object to edit the specific points or curves which "
"make up the shape. Click and drag a point to move it around the canvas. "
"Click and drag along a line to curve it between two points. Holding the :kbd:"
"`Ctrl` key will lock your moves to one axis."
msgstr ""
"Dubbelklicka på ett vektorobjekt för att redigera de specifika punkter eller "
"kurvor som utgör formen. Klicka och dra en punkt för att flytta omkring den "
"på duken. Klicka och dra längs en linje för att böja den mellan två punkter. "
"Att hålla nere tangenten :kbd:`Ctrl` låser förflyttningarna längs en axel."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:98
msgid ".. image:: images/vector/Pointcurvemanip.png"
msgstr ".. image:: images/vector/Pointcurvemanip.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:100
msgid "Stroke and Fill"
msgstr "Streck och fyll"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:102
msgid ""
"In addition to being defined by points and curves, a shape also has two "
"defining properties: **Fill** and **Stroke**. **Fill** defines the color, "
"gradient, or pattern that fills the space inside of the shape object. "
"'**Stroke**' defines the color, gradient, pattern, and thickness of the "
"border along the edge of the shape. These two can be edited using the "
"**Stroke and Fill** dock. The dock has two modes. One for stroke and one for "
"fill. You can change modes by clicking in the dock on the filled square or "
"the black line. The active mode will be shown by which is on top of the "
"other."
msgstr ""
"Förutom att definieras av punkter och kurvor, har en form också två "
"definierande egenskaper: **Fyll** och **Streck**. **Fyll** definierar "
"färgen, toningen eller mönstret som fyller området inne i formobjektet. "
"**Streck** definierar färg, toning, mönster och tjocklek på gränsen längs "
"formens kant. Båda två kan redigeras med panelen **Streck och fyll**. "
"Panelen har två lägen, ett för streck och ett för fyll. Man kan ändra läge "
"genom att klicka på den fyllda fyrkanten eller svarta linjen i panelen. Det "
"aktiva läget visas av vilken som är ovanpå den andra."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:104
msgid ""
"Here is the dock with the fill element active. Notice the red line across "
"the solid white square. This tells us that there is no fill assigned "
"therefore the inside of the shape will be transparent."
msgstr ""
"Här är panelen med fyllelementet aktivt. Observera den röda linjen över den "
"vita fyrkanten. Det talar om att fyll inte är tilldelad, och därför är "
"formens insida genomskinlig."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:107
#: ../../reference_manual/layers_and_masks/vector_layers.rst:129
msgid ".. image:: images/vector/Strokeandfill.png"
msgstr ".. image:: images/vector/Strokeandfill.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:108
msgid "Here is the dock with the stroke element active."
msgstr "Här är panelen med streckelementet aktivt."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:111
msgid ".. image:: images/vector/Strokeandfillstroke.png"
msgstr ".. image:: images/vector/Strokeandfillstroke.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:113
msgid "Editing Stroke Properties"
msgstr "Redigera streckegenskaper"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:115
msgid ""
"The stroke properties dock will allow you to edit a different aspect of how "
"the outline of your vector shape looks."
msgstr ""
"Panelen för streckegenskaper gör det möjligt att redigera en annan aspekt av "
"hur konturen på vektorformen ser ut."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:118
msgid ".. image:: images/vector/Strokeprops.png"
msgstr ".. image:: images/vector/Strokeprops.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:119
msgid ""
"The style selector allows you to choose different patterns and line styles. "
"The width option changes the thickness of the outline on your vector shape. "
"The cap option changes how line endings appear. The join option changes how "
"corners appear."
msgstr ""
"Stilväljaren låter dig välja olika mönster och linjestilar. Alternativet "
"Bredd ändrar tjocklek för vektorformens kontur. Alternativet Ända ändrar hur "
"linjeändar ser ut. Alternativet hopfogning ändar hur hörn ser ut."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:121
msgid ""
"The Miter limit controls how harsh the corners of your object will display. "
"The higher the number the more the corners will be allowed to stretch out "
"past the points. Lower numbers will restrict the stroke to shorter and less "
"sharp corners."
msgstr ""
"Sned gräns bestämmer hur skarpa hörnen på objektet visas. Ju högre värde "
"desto mer tillåts hörnen sträckas ut förbi punkterna. Lägre värden begränsar "
"strecket till kortare och mindre skarpa hörn."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:124
msgid "Editing Fill Properties"
msgstr "Redigera fyllegenskaper"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:126
msgid ""
"All of the fill properties are contained in the **Stroke and Fill** dock."
msgstr "Alla fyllegenskaper finns i panelen **Streck och fyll**."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:130
msgid ""
"The large red **X** button will set the fill to none causing the area inside "
"of the vector shape to be transparent."
msgstr ""
"Den stora röda knappen **X** ställer in ingen fyll vilket gör att området "
"inne i vektorformen blir genomskinligt."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:132
msgid ""
"To the right of that is the solid square. This sets the fill to be a solid "
"color which is displayed in the long button and can be selected by pressing "
"the arrow just to the right of the long button. To the right of the solid "
"square is the gradient button. This will set the fill to display as a "
"gradient. A gradient can be selected by pressing the down arrow next to the "
"long button."
msgstr ""
"Till höger om den finns en ifylld fyrkant. Den ställer in fyllnad till en "
"hel färg som visas i den långa knappen och kan väljas genom att klicka på "
"pilen precis till höger om den långa knappen. Till höger om den ifyllda "
"fyrkanten finns toningsknappen. Den ställer in fyllnad att visas som en "
"toning. En toning kan väljas genom att klicka på neråtpilen intill den långa "
"knappen."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:134
msgid ""
"Under the **X** is a button that shows a pattern. This inside area will be "
"filled with a pattern. A pattern can be chosen by pressing the arrows next "
"to the long button. The two other buttons are for **fill rules**: the way a "
"self-overlapping path is filled."
msgstr ""
"Under **X** finns en knapp som visar ett mönster. Det inre området fylls med "
"ett mönster. Ett mönster kan väljas genom att klicka på pilarna intill den "
"långa knappen. De två andra knapparna är för **fyllnadsregler**: sättet som "
"en kontur som överlappar sig själv fylls i."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:136
msgid ""
"The button with the inner square blank toggles even-odd mode, where every "
"filled region of the path is next to an unfilled one, like this:"
msgstr ""
"Knappen med den inte fyrkanten tom väljer mellan jämnt och udda läge, där "
"varje ifyllt område av konturen är intill ett ofyllt, på detta sätt:"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:142
msgid ""
"The button with the inner square filled toggles non zero mode, where most of "
"the time a self overlapping path is entirely filled except when it overlaps "
"with a sub-path of a different direction that 'decrease the level of "
"overlapping' so that the region between the two is considered outside the "
"path and remain unfilled, like this:"
msgstr ""
"Knappen med den inre fyrkanten ifylld ändrar icke-nolläge, där en kontur som "
"överlappar sig själv fylls i helt och hållet största delen av tiden, utom "
"när den överlappar med en delkontur av en annan riktning som 'minskar graden "
"av överlappning' så att området mellan de två anses vara utanför konturen, "
"och förblir ofyllt, på detta sätt:"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:148
msgid ""
"For more (and better) information about fill rules check the `Inkscape "
"manual <http://tavmjong.free.fr/INKSCAPE/MANUAL/html/Attributes-Fill-Stroke."
"html#Attributes-Fill-Rule>`_."
msgstr ""
"För mer (och bättre) information om fyllregler titta på `Inkscape manual "
"<http://tavmjong.free.fr/INKSCAPE/MANUAL/html/Attributes-Fill-Stroke."
"html#Attributes-Fill-Rule>`_."
