# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:08+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/create_new_document.rst:1
msgid ""
"A simple guide to the first basic steps of using Krita: creating and saving "
"an image."
msgstr ""
"En enkel handledning till de första grundläggande stegen i att använda "
"Krita: skapa och spara en bild."

#: ../../reference_manual/create_new_document.rst:18
msgid "Save"
msgstr "Spara"

#: ../../reference_manual/create_new_document.rst:18
msgid "Load"
msgstr "Läs in"

#: ../../reference_manual/create_new_document.rst:18
msgid "New"
msgstr "Ny"

#: ../../reference_manual/create_new_document.rst:22
msgid "Create New Document"
msgstr "Skapa nytt dokument"

#: ../../reference_manual/create_new_document.rst:24
msgid "A new document can be created as follows."
msgstr "Ett nytt dokument kan skapas på följande sätt."

#: ../../reference_manual/create_new_document.rst:26
msgid "Click on :guilabel:`File` from the application menu at the top."
msgstr "Klicka på :guilabel:`Arkiv` i programmenyn längst upp."

#: ../../reference_manual/create_new_document.rst:27
msgid ""
"Then click on :guilabel:`New`. Or you can do this by pressing the :kbd:`Ctrl "
"+ N` shortcut."
msgstr ""
"Klicka därefter på :guilabel:`Ny`. Eller så kan du göra det genom att "
"använda genvägen :kbd:`Ctrl + N`."

#: ../../reference_manual/create_new_document.rst:28
msgid "Now you will get a New Document dialog box as shown below:"
msgstr "Nu ser du dialogrutan Nytt dokument som visas nedan:"

#: ../../reference_manual/create_new_document.rst:31
msgid ".. image:: images/Krita_newfile.png"
msgstr ".. image:: images/Krita_newfile.png"

#: ../../reference_manual/create_new_document.rst:32
msgid ""
"There are various sections in this dialog box which aid in creation of new "
"document, either using custom document properties or by using contents from "
"clipboard and templates. Following are the sections in this dialog box:"
msgstr ""
"Det finns diverse sektioner i dialogrutan som hjälper till att skapa ett "
"nytt dokument, antingen genom att använda egna dokumentegenskaper eller "
"genom att använda innehåll från klippbordet och mallar. Sektionerna i "
"dialogrutan är följande:"

#: ../../reference_manual/create_new_document.rst:37
msgid "Custom Document"
msgstr "Eget dokument"

#: ../../reference_manual/create_new_document.rst:39
msgid ""
"From this section you can create a document according to your requirements: "
"you can specify the dimensions, color model, bit depth, resolution, etc."
msgstr ""
"I den  här sektionen kan du skapa ett dokument enligt dina egna krav: du kan "
"specificera dimensionerna, färgmodellen, bitdjupet, upplösning, etc."

#: ../../reference_manual/create_new_document.rst:42
msgid ""
"In the top-most field of the :guilabel:`Dimensions` tab, from the Predefined "
"drop-down you can select predefined pixel sizes and PPI (pixels per inch). "
"You can also set custom dimensions and the orientation of the document from "
"the input fields below the predefined drop-down. This can also be saved as a "
"new predefined preset for your future use by giving a name in the Save As "
"field and clicking on the Save button. Below we find the Color section of "
"the new document dialog box, where you can select the color model and the "
"bit-depth. Check :ref:`general_concept_color` for more detailed information "
"regarding color."
msgstr ""
"I det översta fältet under fliken :guilabel:`Dimensioner`, kan du välja "
"fördefinierade bildpunktsstorlekar och bildpunkter per tum (b/t) från den "
"fördefinierade kombinationsrutan. Du kan också ställa in egna dimensioner "
"och dokumentets orientering från inmatningsfälten under den fördefinierade "
"kombinationsrutan. Den kan också sparas som en ny fördefinierad inställning "
"för framtida användning, genom att ge den ett namn i fältet Spara som och "
"klicka på knappen Spara. Nedanför hittar vi färgsektionen i dialogrutan för "
"nytt dokument, där man kan välja färgmodell och bitdjup. Titta i :ref:"
"`general_concept_color`för mer detaljerad information om färg."

#: ../../reference_manual/create_new_document.rst:52
msgid ""
"On the :guilabel:`Content` tab, you can define a name for your new document. "
"This name will appear in the metadata of the file, and Krita will use it for "
"the auto-save functionality as well. If you leave it empty, the document "
"will be referred to as 'Unnamed' by default. You can select the background "
"color and the amount of layers you want in the new document. Krita remembers "
"the amount of layers you picked last time, so be careful."
msgstr ""
"Under fliken :guilabel:`Innehåll` kan du definiera ett namn på det nya "
"dokumentet. Namnet visas i filens metadata, och Krita använder det också för "
"funktionen att spara automatiskt. Om det lämnas tomt kallas dokumentet "
"standardmässigt 'Namnlös'. Du kan välja bakgrundsfärg och antal lager som du "
"vill ha i det nya dokumentet. Krita kommer ihåg antal lager som du valde "
"förra gången, så var försiktig."

#: ../../reference_manual/create_new_document.rst:59
msgid ""
"Finally, there's a description box, useful to note down what you are going "
"to do."
msgstr ""
"Slutligen finns en beskrivningsruta, användbar för att notera vad du ska "
"göra."

#: ../../reference_manual/create_new_document.rst:62
msgid "Create From Clipboard"
msgstr "Skapa från klippbord"

#: ../../reference_manual/create_new_document.rst:64
msgid ""
"This section allows you to create a document from an image that is in your "
"clipboard, like a screenshot. It will have all the fields set to match the "
"clipboard image."
msgstr ""
"Den här sektionen låter dig skapa ett dokument från en bild som finns på "
"klippbordet, som en skärmbild. Det har alla fält inställda för att motsvara "
"bilden på klippbordet."

#: ../../reference_manual/create_new_document.rst:69
msgid "Templates:"
msgstr "Mallar:"

#: ../../reference_manual/create_new_document.rst:71
msgid ""
"These are separate categories where we deliver special defaults. Templates "
"are just .kra files which are saved in a special location, so they can be "
"pulled up by Krita quickly. You can make your own template file from any ."
"kra file, by using :menuselection:`File --> Create Template From Image` in "
"the top menu. This will add your current document as a new template, "
"including all its properties along with the layers and layer contents."
msgstr ""
"De här separata kategorier där vi levererar speciella förval. Mallar är "
"bara .kra-filer som sparats på en särskild plats, så de kan snabbt tas fram "
"av Krita. Du kan skapa egna mallfiler från vilken kra-fil som helst genom "
"att använda :menuselection:`Arkiv --> Skapa mall från bild` i toppmenyn. Det "
"lägger till det aktuella dokumentet som nya mallar, inklusive alla dess "
"egenskaper tillsammans med lagren och lagrens innehåll."

#: ../../reference_manual/create_new_document.rst:78
msgid ""
"Once you have created a new document according to your preference, you "
"should now have a white canvas in front of you (or whichever background "
"color you chose in the dialog)."
msgstr ""
"När du väl har skapat ett nytt dokument enligt dina önskemål, ska det nu "
"finnas en vit duk framför dig (eller med den bakgrundsfärg som du valde i "
"dialogrutan)."
