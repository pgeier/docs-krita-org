# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:24+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/resource_management/paintoppresets.rst:1
msgid "The Brush Presets resource in Krita."
msgstr "Resursen för penselförinställning i Krita."

#: ../../reference_manual/resource_management/paintoppresets.rst:11
msgid "Resources"
msgstr "Resurser"

#: ../../reference_manual/resource_management/paintoppresets.rst:11
msgid "Brush Presets"
msgstr "Penselförinställningar"

#: ../../reference_manual/resource_management/paintoppresets.rst:11
msgid "Brushes"
msgstr "Penslar"

#: ../../reference_manual/resource_management/paintoppresets.rst:11
msgid "Paintop Presets"
msgstr "Målarförinställningar"

#: ../../reference_manual/resource_management/paintoppresets.rst:16
msgid "Brush Preset"
msgstr "Penselförinställning"

#: ../../reference_manual/resource_management/paintoppresets.rst:18
msgid ""
"Paint Op presets store the preview thumbnail, brush-engine, the parameters, "
"the brush tip, and, if possible, the texture. They are saved as .kpp files"
msgstr ""
"Förinställda målaråtgärder lagrar förhandsgranskningsminiatyrbilden, "
"penselgränssnittet, parametrarna, penselspetsen, och om möjligt, strukturen. "
"De sparas som .kpp-filer."

#: ../../reference_manual/resource_management/paintoppresets.rst:20
msgid ""
"For information regarding the brush system, see :ref:`Brushes "
"<loading_saving_brushes>`."
msgstr ""
"För information om penselsystemet, se :ref:`Penslar "
"<loading_saving_brushes>`."

#: ../../reference_manual/resource_management/paintoppresets.rst:23
msgid "The Docker"
msgstr "Panelen"

#: ../../reference_manual/resource_management/paintoppresets.rst:25
msgid ""
"The docker for Paint-op presets is the :ref:`brush_preset_docker`. Here you "
"can tag, add, remove and search paint op presets."
msgstr ""
"Panelen med förinställda målaråtgärder är :ref:`brush_preset_docker`. Här "
"kan förinställda målaråtgärder etiketteras, läggas till, tas bort och "
"eftersökas."

#: ../../reference_manual/resource_management/paintoppresets.rst:28
msgid "Editing the preview thumbnail"
msgstr "Redigera förhandsgranskningsminiatyrbilden"

#: ../../reference_manual/resource_management/paintoppresets.rst:30
msgid ""
"You can edit the preview thumbnail in the brush-scratchpad, but you can also "
"open the \\*.kpp file in Krita to get a 200x200 file to edit to your wishes."
msgstr ""
"Förhandsgranskningsminiatyrbilden i penselns kladdblock kan redigeras, men "
"det går också att öppna \\*.kpp-filen i Krita för att få en 200x200 fil att "
"redigera efter behov."
