# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-05 21:46+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_1.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_1.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_2.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_2.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_3.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_3.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutoria_4.png"
msgstr ".. image:: images/brushes/Krita-normals-tutoria_4.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:1
msgid "The Tangent Normal Brush Engine manual page."
msgstr "Manualsidan för tangensnormalpenselgränssnittet."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:16
msgid "Tangent Normal Brush Engine"
msgstr "Tangensnormalpenselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Penselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Normal Map"
msgstr "Normalavbildning"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:20
msgid ".. image:: images/icons/tangentnormal.svg"
msgstr ".. image:: images/icons/tangentnormal.svg"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:21
msgid ""
"The Tangent Normal Brush Engine is an engine that is specifically designed "
"for drawing normal maps, of the tangent variety. These are in turn used in "
"3d programs and game engines to do all sorts of lightning trickery. Common "
"uses of normal maps include faking detail where there is none, and to drive "
"transformations (Flow Maps)."
msgstr ""
"Tangensnormalpenselgränssnittet är ett gränssnitt som är särskilt "
"konstruerat för att rita normalavbildningar av tangensversion. De används i "
"sin tur för att tredimensionsprogram och spelgränssnitt för att göra alla "
"möjliga trick med blixtar. Vanliga användningar av normalavbildningar "
"omfattar att hitta på detaljer där inga finns, och att styra "
"transformeringar (flödesavbildningar)."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:23
msgid ""
"A Normal map is an image that holds information for vectors. In particular, "
"they hold information for Normal Vectors, which is the information for how "
"the light bends on a surface. Because Normal Vectors are made up of 3 "
"coordinates, just like colors, we can store and see this information as "
"colors."
msgstr ""
"En normalavbildning är en bild som innehåller information för vektorer. I "
"synnerhet innehåller den information för normalvektorer, vilket är "
"informationen om hur ljus böjs vid en yta. Eftersom normalvektorer består av "
"tre koordinater, precis som färger, kan vi lagra informationen som färger."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:25
msgid ""
"Normals can be seen similar to the stylus on your tablet. Therefore, we can "
"use the tilt-sensors that are available to some tablets to generate the "
"color of the normals, which can then be used by a 3d program to do lighting "
"effects."
msgstr ""
"Normaler kan betraktas som om de liknar ritplattans penna. Därför kan vi "
"använda lutningssensorerna som är tillgängliga på vissa ritplattor för att "
"generera normalernas färger, vilka sedan kan användas av "
"tredimensionsprogram för att göra ljussättningseffekter."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:27
msgid "In short, you will be able to paint with surfaces instead of colors."
msgstr "Kort sagt, kan man måla med ytor istället för färger."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:29
msgid "The following options are available to the tangent normal brush engine:"
msgstr ""
"Följande alternativ är tillgängliga för tangensnormalpenselgränssnittet:"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:31
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:32
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:33
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:34
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:35
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:36
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:37
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:38
msgid ":ref:`option_softness`"
msgstr ":ref:`option_softness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:39
msgid ":ref:`option_sharpness`"
msgstr ":ref:`option_sharpness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:40
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:41
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:42
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:43
msgid ":ref:`option_texture`"
msgstr ":ref:`option_texture`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:46
msgid "Specific Parameters to the Tangent Normal Brush Engine"
msgstr "Specifika parametrar för tangensnormalpenselgränssnittet"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:50
msgid "Tangent Tilt"
msgstr "Tangenslutning"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:52
msgid ""
"These are the options that determine how the normals are calculated from "
"tablet input."
msgstr ""
"Det är alternativen som bestämmer hur normaler beräknas från ritplattans "
"inmatning."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:54
msgid "Tangent Encoding"
msgstr "Tangenskodning"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:55
msgid ""
"This allows you to set what each color channel means. Different programs set "
"different coordinates to different channels, a common version is that the "
"green channel might need to be inverted (-Y), or that the green channel is "
"actually storing the x-value (+X)."
msgstr ""
"Låter dig ställa in vad varje färgkanal betyder. Olika program lagrar olika "
"koordinater i olika kanaler, en vanlig version är att den gröna kanalen kan "
"behöva inverteras (-Y), eller att den gröna kanalen faktiskt lagrar x-värdet "
"(+X)."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:56
msgid "Tilt Options"
msgstr "Lutningsalternativ"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:57
msgid "Allows you to choose which sensor is used for the X and Y."
msgstr "Låter dig välja vilken sensor som används för X och Y."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:58
msgid "Tilt"
msgstr "Lutning"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:59
msgid "Uses Tilt for the X and Y."
msgstr "Använd lutning för X och Y."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:60
msgid "Direction"
msgstr "Riktning"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:61
msgid ""
"Uses the drawing angle for the X and Y and Tilt-elevation for the Z, this "
"allows you to draw flowmaps easily."
msgstr ""
"Använd ritvinkel för X och Y samt lutningshöjd för Z. Det gör det möjligt "
"att enkelt rita flödesavbildningar."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:62
msgid "Rotation"
msgstr "Rotation"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:63
msgid ""
"Uses rotation for the X and Y, and tilt-elevation for the Z. Only available "
"for specialized Pens."
msgstr ""
"Använd rotation för X och Y samt lutningshöjd för Z. Bara tillgänglig för "
"specialiserade pennor."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid "Elevation Sensitivity"
msgstr "Höjdkänslighet"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid ""
"Allows you to change the range of the normal that are outputted. At 0 it "
"will only paint the default normal, at 1 it will paint all the normals in a "
"full hemisphere."
msgstr ""
"Låter dig ändra intervall på normalen som matas ut. Med 0 målas bara "
"standardnormalen, med 1 målas alla normalerna i ett helt halvklot."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:68
msgid "Usage"
msgstr "Användning"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:70
msgid ""
"The Tangent Normal Map Brush Engine is best used with the Tilt Cursor, which "
"can be set in :menuselection:`Settings --> Configure Krita --> General --> "
"Outline Shape --> Tilt Outline`."
msgstr ""
"Tangensnormalpenselgränssnittet används bäst med lutningsmarkören, vilket "
"kan ställas in med :menuselection:`Inställningar --> Anpassa Krita --> "
"Allmänt --> Konturform --> Lutningskontur`."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:73
msgid "Normal Map authoring workflow"
msgstr "Arbetsflöde med normalavbildning"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:75
msgid "Create an image with a background color of (128, 128, 255) blue/purple."
msgstr "Skapa en bild med bakgrundsfärgen (128, 128, 255) blå/violett."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:80
msgid "Setting up a background with the default color."
msgstr "Ställa in en bakgrund med standardfärg."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:82
msgid ""
"Set up group with a :guilabel:`Phong Bumpmap` filter mask. Use the :guilabel:"
"`Use Normal map` checkbox on the filter to make it use normals."
msgstr ""
"Skapa en grupp med filtermasken :guilabel:`Phong bulavbildning`. Använd "
"kryssrutan :guilabel:`Använd normalavbildning` i filtret för att få det att "
"använda normaler."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:87
msgid ""
"Creating a phong bump map filter layer, make sure to check 'Use Normal map'."
msgstr ""
"Skapa ett filterlager med Phong bulavbildning, och säkerställ att 'Använd "
"normalavbildning' är markerat."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:92
msgid ""
"These settings give a nice daylight-esque lighting setup, with light 1 being "
"the sun, light 3 being the light from the sky, and light 2 being the light "
"from the ground."
msgstr ""
"Inställningarna ger en snygg dagsljusliknande ljussättning, där ljus 1 är "
"solen, ljus 3 är himmelsljuset och ljus 2 är markljuset."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:94
msgid ""
"Make a :guilabel:`Normalize` filter layer or mask to normalize the normal "
"map before feeding it into the Phong bumpmap filter for the best results."
msgstr ""
"Skapa ett filterlager eller mask med :guilabel:`Normalisera` för att "
"normalisera normalavbildningen innan den skickas till Phong "
"bulavbildningsfiltret för bästa resultat."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:95
msgid "Then, paint on layers in the group to get direct feedback."
msgstr "Måla därefter på lager i gruppen för att få direkt återmatning."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:100
msgid ""
"Paint on the layer beneath the filters with the tangent normal brush to have "
"them be converted in real time."
msgstr ""
"Måla på lagret under filtren med tangensnormalpenseln för att konvertera dem "
"i realtid."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:102
msgid ""
"Finally, when done, hide the Phong bumpmap filter layer (but keep the "
"Normalize filter layer!), and export the normal map for use in 3d programs."
msgstr ""
"Till sist, när allt är klart, dölj filterlagret Phong bulavbildning (men "
"behåll filterlagret Normalisera), och exportera normalavbildningen för "
"användning av tredimensionsprogram."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:105
msgid "Drawing Direction Maps"
msgstr "Rita riktningsavbildningar"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:107
msgid ""
"Direction maps are made with the :guilabel:`Direction` option in the :"
"guilabel:`Tangent Tilt` options. These normal maps are used to distort "
"textures in a 3d program (to simulate for example, the flow of water) or to "
"create maps that indicate how hair and brushed metal is brushed. Krita can't "
"currently give feedback on how a given direction map will influence a "
"distortion or shader, but these maps are a little easier to read."
msgstr ""
"Riktningsavbildningar skapas med alternativet :guilabel:`Riktning` under "
"alternativen guilabel:`Tangenslutning`. Sådana normalavbildningar används "
"för att förvränga strukturer i ett tredimensionsprogram (för att exempelvis "
"simulera vattenflöde) eller för att skapa avbildningar som anger hur hår "
"eller borstad metall ska borstas. Krita kan för närvarande inte ge "
"återmatning om hur en given riktningsavbildning påverkar en förvrängning "
"eller skuggning, men sådana avbildningar är lite enklare att läsa."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:109
msgid ""
"Just set the :guilabel:`Tangent Tilt` option to :guilabel:`Direction`, and "
"draw. The direction your brush draws in will be the direction that is "
"encoded in the colors."
msgstr ""
"Ställ bara in :guilabel:`Tangenslutning` till :guilabel:`Riktning`, och "
"måla. RIktningen som penseln målar blir riktningen som kodas i färgerna."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:112
msgid "Only editing a single channel"
msgstr "Redigera endast en kanal"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:114
msgid ""
"Sometimes you only want to edit a single channel. In that case set the "
"blending mode of the brush to :guilabel:`Copy <channel>`, with <channel> "
"replaced with red, green or blue. These are under the :guilabel:`Misc` "
"section of the blending modes."
msgstr ""
"Ibland vill man bara redigera en enskild kanal. I sådana fall ställ in "
"penselns blandningsläge till :guilabel:`Kopiera <kanal>`, med <kanal> ersatt "
"med röd, grön eller blå. De finns under blandningslägenas sektion :guilabel:"
"`Diverse`."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:116
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to :guilabel:`Copy Red`."
msgstr ""
"Om man vill att penseln bara ska påverka den röda kanalen, ställ in "
"blandningsläget till 'Kopiera röd'`."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid ".. image:: images/brushes/Krita_Filter_layer_invert_greenchannel.png"
msgstr ".. image:: images/brushes/Krita_Filter_layer_invert_greenchannel.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr ""
"Blandningslägena för att kopiera röd, grön, och blå fungerar också för "
"filterlager."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:123
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with :guilabel:`Copy "
"Green` above it."
msgstr ""
"Det kan också göras med filterlager. Om man alltså snabbt vill byta den "
"gröna kanalen på ett lager, ska man skapa ett inverterat filterlager med "
"'Kopiera grön' ovanför det."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:126
msgid "Mixing Normal Maps"
msgstr "Kombinera normalavbildning"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:128
msgid ""
"For mixing two normal maps, Krita has the :guilabel:`Combine Normal Map` "
"blending mode under :guilabel:`Misc`."
msgstr ""
"Krita har blandningsläget :guilabel:`Kombinera normalavbildning` under :"
"guilabel:`Diverse` för att blanda två normalavbildningar."
