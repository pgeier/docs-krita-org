# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-05 12:35+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/linux_command_line.rst:None
msgid "New in version 3.3:"
msgstr "Nytt i version 3.3:"

#: ../../reference_manual/linux_command_line.rst:1
msgid "Overview of Krita's command line options."
msgstr "Översikt av Kritas kommandoradsväljare."

#: ../../reference_manual/linux_command_line.rst:11
msgid "Command Line"
msgstr "Kommandorad"

#: ../../reference_manual/linux_command_line.rst:16
msgid "Linux Command Line"
msgstr "Linux kommandorad"

#: ../../reference_manual/linux_command_line.rst:20
msgid ""
"As a native Linux program, Krita allows you to do operations on images "
"without opening the program when using the Terminal. This option was "
"disabled on Windows and OSX, but with 3.3 it is enabled for them!"
msgstr ""
"Såsom inbyggt Linux-program, låter Krita dig utföra åtgärder på bilder utan "
"att visa programmet när terminalen används. Alternativet var inaktiverat på "
"Windows och OSX, men med 3.3 är det aktiverat för dem."

#: ../../reference_manual/linux_command_line.rst:22
msgid ""
"This is primarily used in bash or shell scripts, for example, to mass "
"convert kra files into pngs."
msgstr ""
"Det används i huvudsak med bash och skalskript, exempelvis för att "
"konvertera många kra-filer till png:er."

#: ../../reference_manual/linux_command_line.rst:25
msgid "Export"
msgstr "Exportera"

#: ../../reference_manual/linux_command_line.rst:27
msgid "This allows you to quickly convert files via the terminal:"
msgstr "Låter dig snabbt konvertera filer via terminalen:"

#: ../../reference_manual/linux_command_line.rst:29
msgid "``krita importfilename --export --export-filename exportfilename``"
msgstr "``krita importfilnamn --export --export-filename exportfilnamn``"

#: ../../reference_manual/linux_command_line.rst:34
msgid "importfilename"
msgstr "importfilnamn"

#: ../../reference_manual/linux_command_line.rst:34
msgid "Replace this with the filename of the file you want to manipulate."
msgstr "Ersätt det med filnamnet på filen som ska manipuleras."

#: ../../reference_manual/linux_command_line.rst:38
msgid "Export a file selects the export option."
msgstr "Exportera en fil använder exportväljaren."

#: ../../reference_manual/linux_command_line.rst:42
msgid ""
"Export filename says that the following word is the filename it should be "
"exported to."
msgstr ""
"Exportfilnamnet anger att följande argument är filnamnet som det ska "
"exporteras till."

#: ../../reference_manual/linux_command_line.rst:45
msgid "exportfilename"
msgstr "exportfilnamn"

#: ../../reference_manual/linux_command_line.rst:45
msgid ""
"Replace this with the name of the output file. Use a different extension to "
"change the file format."
msgstr ""
"Ersätt det med namnet på utdatafilen. Använd en annan filändelse för att "
"ändra filformatet."

#: ../../reference_manual/linux_command_line.rst:47
msgid "Example:"
msgstr "Exempel:"

#: ../../reference_manual/linux_command_line.rst:49
msgid "``krita file.png --export --export-filename final.jpg``"
msgstr "``krita fil.png --export --export-filename slutresultat.jpg``"

#: ../../reference_manual/linux_command_line.rst:51
msgid "This line takes the file ``file.png`` and saves it as ``file.jpg``."
msgstr "Raden tar filen ``fil.png`` och sparar den som ``slutresultat.jpg``."

#: ../../reference_manual/linux_command_line.rst:57
msgid "Export animation to the given filename and exit"
msgstr "Exportera animering till angivet filnamn och avsluta"

#: ../../reference_manual/linux_command_line.rst:59
msgid ""
"If a KRA file has no animation, then this command prints \"This file has no "
"animation.\" error and does nothing."
msgstr ""
"Om en KRA-fil inte har någon animering, skriver kommandot ut \"Den här filen "
"har ingen animering\" och gör ingenting."

#: ../../reference_manual/linux_command_line.rst:61
msgid "``krita --export-sequence --export-filename file.png test.kra``"
msgstr "``krita --export-sequence --export-filename fil.png test.kra``"

#: ../../reference_manual/linux_command_line.rst:63
msgid ""
"This line takes the animation in test.kra, and uses the value of --export-"
"filename (file.png), to determine the sequence fileformat('png') and the "
"frame prefix ('file')."
msgstr ""
"Raden tar animeringen i test.kra och använder värdet på --export-filename "
"(fil.png) för att bestämma sekvensens filformat ('png') och bildruteprefixet "
"('fil')."

#: ../../reference_manual/linux_command_line.rst:67
msgid "PDF export"
msgstr "PDF-export"

#: ../../reference_manual/linux_command_line.rst:69
msgid "Pdf export looks a bit different, using the ``--export-pdf`` option."
msgstr ""
"PDF-export ser lita annorlunda ut, genom att väljaren ``--export-pdf`` "
"används."

#: ../../reference_manual/linux_command_line.rst:71
msgid "``krita file.png --export-pdf --export-filename final.pdf``"
msgstr "``krita fil.png --export-pdf --export-filename slutresultat.pdf``"

#: ../../reference_manual/linux_command_line.rst:73
msgid "export-pdf exports the file ``file.png`` as a pdf file."
msgstr "export-pdf exporterar filen ``fil.png`` som en PDF-fil."

#: ../../reference_manual/linux_command_line.rst:77
msgid "This has been removed from 3.1 because the results were incorrect."
msgstr "Det har tagits bort från 3.1 eftersom resultatet var felaktigt."

#: ../../reference_manual/linux_command_line.rst:80
msgid "Open with Custom Screen DPI"
msgstr "Öppna med egen skärmupplösning i punkter/tum"

#: ../../reference_manual/linux_command_line.rst:82
#: ../../reference_manual/linux_command_line.rst:88
msgid "Open Krita with specified Screen DPI."
msgstr "Öppna med angiven skärmupplösning i punkter/tum."

#: ../../reference_manual/linux_command_line.rst:90
msgid "For example:"
msgstr "Exempelvis:"

#: ../../reference_manual/linux_command_line.rst:92
msgid "``krita --dpi <72,72>``"
msgstr "``krita --dpi <72,72>``"

#: ../../reference_manual/linux_command_line.rst:95
msgid "Open template"
msgstr "Öppna mall"

#: ../../reference_manual/linux_command_line.rst:97
msgid ""
"Open krita and automatically open the given template(s). This allows you to, "
"for example, create a shortcut to Krita that opens a given template, so you "
"can get to work immediately!"
msgstr ""
"Öppnar Krita och öppnar automatiskt den angivna mallen eller mallarna. Det "
"låter dig exempelvis skapa en genväg till Krita som öppnar en angiven mall, "
"så att du kan börja arbeta omedelbart."

#: ../../reference_manual/linux_command_line.rst:99
msgid "``krita --template templatename.desktop``"
msgstr "``krita --template mallnamn.desktop``"

#: ../../reference_manual/linux_command_line.rst:105
msgid "Selects the template option."
msgstr "Använder mallväljaren."

#: ../../reference_manual/linux_command_line.rst:107
msgid ""
"All templates are saved with the .desktop extension. You can find templates "
"in the .local/share/krita/template or in the install folder of Krita."
msgstr ""
"Alla mallar sparas med filändelsen .desktop. Mallarna finns i .local/share/"
"krita/template eller i Kritas installationskatalog."

#: ../../reference_manual/linux_command_line.rst:109
msgid "``krita --template BD-EuroTemplate.desktop``"
msgstr "``krita --template BD-EuroTemplate.desktop``"

#: ../../reference_manual/linux_command_line.rst:111
msgid "This opens the European BD comic template with Krita."
msgstr "Öppnar seriemallen European BD med Krita."

#: ../../reference_manual/linux_command_line.rst:113
msgid "``krita --template BD-EuroTemplate.desktop BD-EuroTemplate.desktop``"
msgstr "``krita --template BD-EuroTemplate.desktop BD-EuroTemplate.desktop``"

#: ../../reference_manual/linux_command_line.rst:115
msgid "This opens the European BD template twice, in separate documents."
msgstr "Öppnar mallen European BD två gånger, i separata dokument."

#: ../../reference_manual/linux_command_line.rst:118
msgid "Start up"
msgstr "Start"

#: ../../reference_manual/linux_command_line.rst:126
msgid "Starts krita without showing the splash screen."
msgstr "Startar Krita utan att visa startskärmen."

#: ../../reference_manual/linux_command_line.rst:130
msgid "Starts krita in canvasonly mode."
msgstr "Startar Krita med enbart duk."

#: ../../reference_manual/linux_command_line.rst:134
msgid "Starts krita in fullscreen mode."
msgstr "Startar Kirta i fullskärmsläge."

#: ../../reference_manual/linux_command_line.rst:138
msgid "Starts krita with the given workspace. So for example..."
msgstr "Startar Krita med angiven arbetsrymd. Så exempelvis ..."

#: ../../reference_manual/linux_command_line.rst:140
msgid "``krita --workspace Animation``"
msgstr "``krita --workspace Animering``"

#: ../../reference_manual/linux_command_line.rst:142
msgid "Starts Krita in the Animation workspace."
msgstr "Startar Krita med arbetsrymden Animering."
