# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-14 16:09+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/projection/perspective.rst:None
msgid ""
".. image:: images/category_projection/Projection_Lens1_from_wikipedia.svg"
msgstr ""
".. image:: images/category_projection/Projection_Lens1_from_wikipedia.svg"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection-cube_12.svg"
msgstr ".. image:: images/category_projection/projection-cube_12.svg"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection-cube_13.svg"
msgstr ".. image:: images/category_projection/projection-cube_13.svg"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_31.png"
msgstr ".. image:: images/category_projection/projection_image_31.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_animation_03.gif"
msgstr ".. image:: images/category_projection/projection_animation_03.gif"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_32.png"
msgstr ".. image:: images/category_projection/projection_image_32.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_33.png"
msgstr ".. image:: images/category_projection/projection_image_33.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_34.png"
msgstr ".. image:: images/category_projection/projection_image_34.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_35.png"
msgstr ".. image:: images/category_projection/projection_image_35.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_36.png"
msgstr ".. image:: images/category_projection/projection_image_36.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_37.png"
msgstr ".. image:: images/category_projection/projection_image_37.png"

#: ../../general_concepts/projection/perspective.rst:1
msgid "Perspective projection."
msgstr "Perspektivprojektion."

#: ../../general_concepts/projection/perspective.rst:10
msgid ""
"This is a continuation of the :ref:`axonometric tutorial "
"<projection_axonometric>`, be sure to check it out if you get confused!"
msgstr ""
"Det här är en fortsättning av den :ref:`axonometriska handledningen "
"<projection_axonometric>`, var noga med att kolla den om du blir förvirrad."

#: ../../general_concepts/projection/perspective.rst:12
#: ../../general_concepts/projection/perspective.rst:16
msgid "Perspective Projection"
msgstr "Perspektivprojektion"

#: ../../general_concepts/projection/perspective.rst:12
msgid "Projection"
msgstr "Projektion"

#: ../../general_concepts/projection/perspective.rst:12
msgid "Perspective"
msgstr "Perspektiv"

#: ../../general_concepts/projection/perspective.rst:18
msgid ""
"So, up till now we’ve done only parallel projection. This is called like "
"that because all the projection lines we drew were parallel ones."
msgstr ""
"Hittills har vi bara gjort parallell projektion. Den kallas det eftersom "
"alla projektionslinjer som vi ritade var parallella."

#: ../../general_concepts/projection/perspective.rst:20
msgid ""
"However, in real life we don’t have parallel projection. This is due to the "
"lens in our eyes."
msgstr ""
"Dock har man inte parallell projektion i verkliga livet. Det beror på "
"ögonlinsen."

#: ../../general_concepts/projection/perspective.rst:25
msgid ""
"Convex lenses, as this lovely image from `wikipedia <https://en.wikipedia."
"org/wiki/Lens_%28optics%29>`_ shows us, have the ability to turn parallel "
"lightrays into converging ones."
msgstr ""
"Konvexa linser, som den här vackra bilden från `wikipedia <https://sv."
"wikipedia.org/wiki/Lins>`_ visar, har möjlighet att omvandla parallella "
"ljusstrålar till konvergerande."

#: ../../general_concepts/projection/perspective.rst:27
msgid ""
"The point where all the rays come together is called the focal point, and "
"the vanishing point in a 2d drawing is related to it as it’s the expression "
"of the maximum distortion that can be given to two parallel lines as they’re "
"skewed toward the focal point."
msgstr ""
"Punkten där alla stålarna sammanfaller kallas brännpunkten, och flyktpunkten "
"i en tvådimensionell teckning är relaterad till den eftersom den är "
"uttrycket av den maximala förvrängningen som kan ges till två parallella "
"linjer när de samlas i brännpunkten."

#: ../../general_concepts/projection/perspective.rst:29
msgid ""
"As you can see from the image, the focal point is not an end-point of the "
"rays. Rather, it is where the rays cross before diverging again… The only "
"difference is that the resulting image will be inverted. Even in our eyes "
"this inversion happens, but our brains are used to this awkwardness since "
"childhood and turn it around automatically."
msgstr ""
"Som man kan se i bilden är brännpunkten inte strålarnas slutpunkt. Istället "
"är det punkten där strålarna korsar varandra innan de sprids ut igen. Den "
"enda skillnaden är att den resulterande bilden blir omvänd. Den här "
"omvändningen sker också i våra ögon, men hjärnan är van vid den här "
"konstigheten sedan barndomen och vänder automatiskt på den."

#: ../../general_concepts/projection/perspective.rst:31
msgid "Let’s see if we can perspectively project our box now."
msgstr "Låt oss se om vi kan perspektivprojicera vår ruta nu."

#: ../../general_concepts/projection/perspective.rst:36
msgid ""
"That went pretty well. As you can see we sort of *merged* the two sides into "
"one (resulting into the purple side square) so we had an easier time "
"projecting. The projection is limited to one or two vanishing point type "
"projection, so only the horizontal lines get distorted. We can also distort "
"the vertical lines"
msgstr ""
"Det gick rätt bra. Som man kan se har vi på sätt och vis 'sammanfogat' de "
"två sidorna till en (vilket ger fyrkanten med lila kanter) så att det blev "
"enklare att projicera. Projektionen är begränsad till en eller två typer av "
"flyktpunktsprojektion, så bara de horisontella linjerna blir förvrängda. Vi "
"kan också förvränga de vertikala linjerna."

#: ../../general_concepts/projection/perspective.rst:41
msgid ""
"… to get three-point projection, but this is a bit much. (And I totally made "
"a mistake in there…)"
msgstr ""
"... för att få en trepunktsprojektion, men det är rätt mycket. (Och jag "
"gjorde verkligen ett misstag där inne ...)"

#: ../../general_concepts/projection/perspective.rst:43
msgid "Let’s setup our perspective projection again…"
msgstr "Låt oss ställa in vår perspektivprojektion igen ..."

#: ../../general_concepts/projection/perspective.rst:48
msgid ""
"We’ll be using a single vanishing point for our focal point. A guide line "
"will be there for the projection plane, and we’re setting up horizontal and "
"vertical parallel rules to easily draw the straight lines from the view "
"plane to where they intersect."
msgstr ""
"Vi använder en enda flyktpunkt som brännpunkt. En hjälplinje är där för "
"projektionsplanet, och vi ställer in horisontella och vertikala parallella "
"linjaler för att enkelt kunna rita de räta linjerna från vyplanet till "
"platsen där de korsar varandra."

#: ../../general_concepts/projection/perspective.rst:50
msgid ""
"And now the workflow in GIF format… (don’t forget you can rotate the canvas "
"with the :kbd:`4` and :kbd:`6` keys)"
msgstr ""
"Och nu arbetsflödet med GIF-format ... (glöm inte bort att man kan rotera "
"duken med tangenterna :kbd:`4` och :kbd:`6`)"

#: ../../general_concepts/projection/perspective.rst:55
msgid "Result:"
msgstr "Resultat:"

#: ../../general_concepts/projection/perspective.rst:60
msgid "Looks pretty haughty, doesn’t he?"
msgstr "Ser ganska högfärdig ut, inte sant?"

#: ../../general_concepts/projection/perspective.rst:62
msgid "And again, there’s technically a simpler setup here…"
msgstr "Och återigen, finns det teknisk sett en enklare inställning här ..."

#: ../../general_concepts/projection/perspective.rst:64
msgid "Did you know you can use Krita to rotate in 3d? No?"
msgstr ""
"Visste du att man kan använda Krita för att rotera i tre dimensioner? Inte?"

#: ../../general_concepts/projection/perspective.rst:69
msgid "Well, now you do."
msgstr "Ja, nu vet du det."

#: ../../general_concepts/projection/perspective.rst:71
msgid "The ortho graphics are being set to 45 and 135 degrees respectively."
msgstr "Ortografin är inställd till respektive 45 och 135 grader."

#: ../../general_concepts/projection/perspective.rst:73
msgid ""
"We draw horizontal lines on the originals, so that we can align vanishing "
"point rulers to them."
msgstr ""
"Vi ritar horisontella linjer över originalen, så att vi kan linjera upp "
"flyktpunktslinjaler till dem."

#: ../../general_concepts/projection/perspective.rst:78
msgid ""
"And from this, like with the shearing method, we start drawing. (Don’t "
"forget the top-views!)"
msgstr ""
"Och från det, liksom med skjuvningsmetoden, börjar vi rita. (Glöm inte bort "
"toppvyerna!)"

#: ../../general_concepts/projection/perspective.rst:80
msgid "Which should get you something like this:"
msgstr "Vilket ska ge någonting som liknar det här:"

#: ../../general_concepts/projection/perspective.rst:85
msgid "But again, the regular method is actually a bit easier..."
msgstr "Återigen är den vanliga metoden faktiskt lite enklare ..."

#: ../../general_concepts/projection/perspective.rst:87
msgid ""
"But now you might be thinking: gee, this is a lot of work… Can’t we make it "
"easier with the computer somehow?"
msgstr ""
"Men nu kanske du tänker: jösses, det där är massor med jobb ... Kan vi inte "
"göra det enklare med datorn på något sätt?"

#: ../../general_concepts/projection/perspective.rst:89
msgid ""
"Uhm, yes, that’s more or less why people spent time on developing 3d "
"graphics technology:"
msgstr ""
"Jojomän, det är mer eller mindre därför folk spenderat tid på att utveckla "
"tredimensionell grafikteknologi:"

#: ../../general_concepts/projection/perspective.rst:97
msgid ""
"(The image above is sculpted in blender using our orthographic reference)"
msgstr ""
"(Bilden ovan är skulpterad i Blender med användning av vår ortografiska "
"referens)"

#: ../../general_concepts/projection/perspective.rst:99
msgid ""
"So let us look at what this technique can be practically used for in the "
"next part..."
msgstr ""
"Så låt oss ta en titt på hur tekniken kan användas i praktiken i nästa "
"del ..."
