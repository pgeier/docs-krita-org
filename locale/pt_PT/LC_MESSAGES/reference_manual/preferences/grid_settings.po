# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-21 03:28+0200\n"
"PO-Revision-Date: 2019-06-21 12:19+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: ref gridsandguidesdocker Krita images image en\n"
"X-POFile-SpellExtra: KritaPreferencesGrid preferences guilabel\n"
"X-POFile-SpellExtra: menuselection\n"

#: ../../<generated>:1
msgid "Subdivision"
msgstr "Sub-divisão"

#: ../../reference_manual/preferences/grid_settings.rst:1
msgid "Grid settings in Krita."
msgstr "Configuração da grelha no Krita."

#: ../../reference_manual/preferences/grid_settings.rst:15
msgid "Grid Settings"
msgstr "Configuração da Grelha"

#: ../../reference_manual/preferences/grid_settings.rst:19
msgid "Deprecated in 3.0, use the :ref:`grids_and_guides_docker` instead."
msgstr ""
"Descontinuado no 3.0; use como alternativa a :ref:`grids_and_guides_docker`."

#: ../../reference_manual/preferences/grid_settings.rst:21
msgid "Use :menuselection:`Settings --> Configure Krita --> Grid` menu item."
msgstr ""
"Use o item de menu :menuselection:`Configuração -> Configurar o Krita -> "
"Grelha`."

#: ../../reference_manual/preferences/grid_settings.rst:24
msgid ".. image:: images/preferences/Krita_Preferences_Grid.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Grid.png"

#: ../../reference_manual/preferences/grid_settings.rst:25
msgid "Fine tune the settings of the grid-tool grid here."
msgstr "Ajuste aqui as definições da grelha da ferramenta."

#: ../../reference_manual/preferences/grid_settings.rst:28
msgid "Placement"
msgstr "Colocação"

#: ../../reference_manual/preferences/grid_settings.rst:30
msgid "The user can set various settings of the grid over here."
msgstr "O utilizador poderá definir várias configurações grelha aqui."

#: ../../reference_manual/preferences/grid_settings.rst:32
msgid "Horizontal Spacing"
msgstr "Intervalo Horizontal"

#: ../../reference_manual/preferences/grid_settings.rst:33
msgid ""
"The number in Krita units, the grid will be spaced in the horizontal "
"direction."
msgstr ""
"O número de unidades do Krita com que a grelha ficará espaçada na direcção "
"horizontal."

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid "Vertical Spacing"
msgstr "Intervalo Vertical"

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid ""
"The number in Krita units, the grid will be spaced in the vertical "
"direction. The images below will show the usage of these settings."
msgstr ""
"O número de unidades do Krita com que a grelha ficará espaçada na direcção "
"vertical. As imagens abaixo irão mostrar a utilização dessas definições."

#: ../../reference_manual/preferences/grid_settings.rst:37
msgid "X Offset"
msgstr "Deslocamento em X"

#: ../../reference_manual/preferences/grid_settings.rst:38
msgid "The number to offset the grid in the X direction."
msgstr "O número de unidades para desviar a grelha na direcção X."

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "Y Offset"
msgstr "Deslocamento em Y"

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "The number to offset the grid in the Y direction."
msgstr "O número de unidades para desviar a grelha na direcção Y."

#: ../../reference_manual/preferences/grid_settings.rst:42
msgid ""
"Some examples are shown below, look at the edge of the image to see the "
"offset."
msgstr ""
"São apresentados alguns exemplos abaixo; veja o extremo da imagem para "
"perceber o deslocamento."

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid "Subdivisions"
msgstr "Sub-divisões"

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid ""
"Here the user can set the number of times the grid is subdivided. Some "
"examples are shown below."
msgstr ""
"Aqui o utilizador poderá definir o número de vezes que a grelha poderá ser "
"subdividida. Aparecem alguns exemplos em baixo."

#: ../../reference_manual/preferences/grid_settings.rst:48
msgid "Style"
msgstr "Estilo"

#: ../../reference_manual/preferences/grid_settings.rst:50
msgid "Main"
msgstr "Principal"

#: ../../reference_manual/preferences/grid_settings.rst:51
msgid ""
"The user can set how the main lines of the grid are shown. Options available "
"are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""
"O utilizador poderá definir como é que são apresentadas as linhas principais "
"da grelha. As opções disponíveis são Linhas, Linhas Tracejadas, Pontos. "
"Também poderá definir aqui a cor."

#: ../../reference_manual/preferences/grid_settings.rst:53
msgid ""
"The user can set how the subdivision lines of the grid are shown. Options "
"available are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""
"O utilizador poderá definir como é que são apresentadas as linhas das sub-"
"divisões da grelha. As opções disponíveis são Linhas, Linhas Tracejadas, "
"Pontos. Também poderá definir aqui a cor."
