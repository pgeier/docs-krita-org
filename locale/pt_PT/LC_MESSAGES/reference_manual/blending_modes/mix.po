# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 15:02+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: image BlendingmodesAlphaDarkenSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesGrainMergeSampleimagewithdots hardmixps\n"
"X-POFile-SpellExtra: BlendingmodesOverlaySampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesHardOverlaySampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesNormal gif blendingmodes\n"
"X-POFile-SpellExtra: BlendingmodesHardMixSampleimagewithdots Photoshop\n"
"X-POFile-SpellExtra: images OpacitySampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesGeometricMeanSampleimagewithdots\n"
"X-POFile-SpellExtra: Greaterblendmode Krita\n"
"X-POFile-SpellExtra: BlendingmodesGrainExtractSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesEraseSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesAllanonSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesBehindSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesParallelSampleimagewithdots Allanon\n"
"X-POFile-SpellExtra: BlendingmodesPenumbraASampleimagewithdots co\n"
"X-POFile-SpellExtra: BlendingmodesInterpolationX\n"
"X-POFile-SpellExtra: BlendingmodesPenumbraDSampleimagewithdots msitura mix\n"
"X-POFile-SpellExtra: BlendingmodesPenumbraBSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesInterpolationSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesPenumbraCSampleimagewithdots\n"
"X-POFile-SpellExtra: Sampleimagewithdots\n"

#: ../../reference_manual/blending_modes/mix.rst:None
msgid ".. image:: images/blending_modes/mix/Greaterblendmode.gif"
msgstr ".. image:: images/blending_modes/mix/Greaterblendmode.gif"

#: ../../reference_manual/blending_modes/mix.rst:1
msgid ""
"Page about the mix blending modes in Krita: Allanon, Alpha Darken, Behind, "
"Erase, Geometric Mean, Grain Extract, Grain Merge, Greater, Hard Mix, Hard "
"Overlay, Interpolation, Interpolation2x, Normal, Overlay, Parallel, Penumbra "
"A, B, C and D."
msgstr ""
"Página sobre os modos de mistura no Krita: Allanon, Escurecimento do Alfa, "
"Atrás, Apagar, Média Geométrica, Extracção do Grão, Junção do Grão, Maior, "
"Mistura Forte, Sobreposição Forte, Normal, Sobreposição, Paralela, Penumbra "
"A, B, C e D."

#: ../../reference_manual/blending_modes/mix.rst:16
msgid "Mix"
msgstr "Mistura"

#: ../../reference_manual/blending_modes/mix.rst:18
#: ../../reference_manual/blending_modes/mix.rst:22
msgid "Allanon"
msgstr "Allanon"

#: ../../reference_manual/blending_modes/mix.rst:24
msgid ""
"Blends the upper layer as half-transparent with the lower. (It add the two "
"layers together and then halves the value)."
msgstr ""
"Mistura a camada superior de forma semi-transparente com a inferior. "
"(Adiciona as duas camadas em conjunto e depois calcula a metade do valor)."

#: ../../reference_manual/blending_modes/mix.rst:29
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Allanon_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Allanon_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:29
msgid "Left: **Normal**. Right: **Allanon**."
msgstr "Esquerda: **Normal**. Direita: **Allanon**."

#: ../../reference_manual/blending_modes/mix.rst:31
#: ../../reference_manual/blending_modes/mix.rst:35
msgid "Interpolation"
msgstr "Interpolação"

#: ../../reference_manual/blending_modes/mix.rst:37
msgid ""
"Subtract 0.5f by 1/4 of cosine of base layer subtracted by 1/4 of cosine of "
"blend layer assuming 0-1 range. The result is similar to Allanon mode, but "
"with more contrast and functional difference to 50% opacity."
msgstr ""
"Subtrai 0,5f em 1/4 do co-seno da camada de base subtraída em 1/4 do co-seno "
"da camada de mistura, assumindo um intervalo de 0-1. O resultado é "
"semelhante ao modo Allanon, mas com mais contraste e diferenças funcionais "
"com 50% de opacidade."

#: ../../reference_manual/blending_modes/mix.rst:43
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Interpolation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Interpolation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:43
msgid "Left: **Normal**. Right: **Interpolation**."
msgstr "Esquerda: **Normal**. Direita: **Interpolação**."

#: ../../reference_manual/blending_modes/mix.rst:45
msgid "Interpolation2x"
msgstr "Interpolação2x"

#: ../../reference_manual/blending_modes/mix.rst:49
msgid "Interpolation - 2X"
msgstr "Interpolação - 2X"

#: ../../reference_manual/blending_modes/mix.rst:51
msgid ""
"Applies Interpolation blend mode to base and blend layers, then duplicate to "
"repeat interpolation blending."
msgstr ""
"Aplica o modo de mistura por Interpolação Às camadas de base e de msitura, "
"duplicando então para repetir a mistura da interpolação."

#: ../../reference_manual/blending_modes/mix.rst:56
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Interpolation_X2_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Interpolation_X2_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:56
msgid "Left: **Normal**. Right: **Interpolation - 2X**."
msgstr "Esquerda: **Normal**. Direita: **Interpolação - 2X**."

#: ../../reference_manual/blending_modes/mix.rst:58
#: ../../reference_manual/blending_modes/mix.rst:62
msgid "Alpha Darken"
msgstr "Escurecer o Alfa"

#: ../../reference_manual/blending_modes/mix.rst:64
msgid ""
"As far as I can tell this seems to premultiply the alpha, as is common in "
"some file-formats."
msgstr ""
"Tanto quanto se sabe, isto parece pré-multiplicar o alfa, dado que é comum "
"em alguns formatos de ficheiros."

#: ../../reference_manual/blending_modes/mix.rst:69
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Alpha_Darken_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Alpha_Darken_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:69
msgid "Left: **Normal**. Right: **Alpha Darken**."
msgstr "Esquerda: **Normal**. Direita: **Escurecimento do Alfa**."

#: ../../reference_manual/blending_modes/mix.rst:71
#: ../../reference_manual/blending_modes/mix.rst:75
msgid "Behind"
msgstr "Atrás"

#: ../../reference_manual/blending_modes/mix.rst:77
msgid ""
"Does the opposite of normal, and tries to have the upper layer rendered "
"below the lower layer."
msgstr ""
"Faz o oposto do normal e tentar ter a camada superior desenhada atrás da "
"camada actual."

#: ../../reference_manual/blending_modes/mix.rst:82
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Behind_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Behind_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:82
msgid "Left: **Normal**. Right: **Behind**."
msgstr "Esquerda: **Normal**. Direita: **Atrás**."

#: ../../reference_manual/blending_modes/mix.rst:84
msgid "Erase (Blending Mode)"
msgstr "Apagar (Modo de Mistura)"

#: ../../reference_manual/blending_modes/mix.rst:88
msgid "Erase"
msgstr "Apagar"

#: ../../reference_manual/blending_modes/mix.rst:90
msgid ""
"This subtracts the opaque pixels of the upper layer from the lower layer, "
"effectively erasing."
msgstr ""
"Isto subtrai os pixels opacos da camada superior da camada inferior, "
"apagando de facto."

#: ../../reference_manual/blending_modes/mix.rst:95
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Erase_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Erase_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:95
msgid "Left: **Normal**. Right: **Erase**."
msgstr "Esquerda: **Normal**. Direita: **Apagar**."

#: ../../reference_manual/blending_modes/mix.rst:97
#: ../../reference_manual/blending_modes/mix.rst:101
msgid "Geometric Mean"
msgstr "Média Geométrica"

#: ../../reference_manual/blending_modes/mix.rst:103
msgid ""
"This blending mode multiplies the top layer with the bottom, and then "
"outputs the square root of that."
msgstr ""
"Este modo de mistura multiplica a camada superior com a inferior, calculando "
"depois a raiz quadrada do valor."

#: ../../reference_manual/blending_modes/mix.rst:108
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Geometric_Mean_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Geometric_Mean_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:108
msgid "Left: **Normal**. Right: **Geometric Mean**."
msgstr "Esquerda: **Normal**. Direita: **Média Geométrica**."

#: ../../reference_manual/blending_modes/mix.rst:110
#: ../../reference_manual/blending_modes/mix.rst:114
msgid "Grain Extract"
msgstr "Extracção do Grão"

#: ../../reference_manual/blending_modes/mix.rst:116
msgid ""
"Similar to subtract, the colors of the upper layer are subtracted from the "
"colors of the lower layer, and then 50% gray is added."
msgstr ""
"Similar à subtracção, sendo que as cores da camada superior são subtraídas à "
"camada inferior, sendo depois adicionado 50% de cinzento."

#: ../../reference_manual/blending_modes/mix.rst:121
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Grain_Extract_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Grain_Extract_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:121
msgid "Left: **Normal**. Right: **Grain Extract**."
msgstr "Esquerda: **Normal**. Direita: **Extracção do Grão**."

#: ../../reference_manual/blending_modes/mix.rst:123
#: ../../reference_manual/blending_modes/mix.rst:127
msgid "Grain Merge"
msgstr "Junção do Grão"

#: ../../reference_manual/blending_modes/mix.rst:129
msgid ""
"Similar to addition, the colors of the upper layer are added to the colors, "
"and then 50% gray is subtracted."
msgstr ""
"Similar à adição, sendo que as cores da camada superior são somadas à camada "
"inferior, sendo depois subtraído 50% de cinzento."

#: ../../reference_manual/blending_modes/mix.rst:134
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Grain_Merge_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Grain_Merge_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:134
msgid "Left: **Normal**. Right: **Grain Merge**."
msgstr "Esquerda: **Normal**. Direita: **Junção do Grão**."

#: ../../reference_manual/blending_modes/mix.rst:136
msgid "Greater (Blending Mode)"
msgstr "Maior (Modo de Mistura)"

#: ../../reference_manual/blending_modes/mix.rst:140
msgid "Greater"
msgstr "Maior"

#: ../../reference_manual/blending_modes/mix.rst:142
msgid ""
"A blending mode which checks whether the painted color is painted with a "
"higher opacity than the existing colors. If so, it paints over them, if not, "
"it doesn't paint at all."
msgstr ""
"Um modo de mistura que verifica se a cor pintada é pintada com uma opacidade "
"maior que as existentes. Se for o caso, pinta sobre elas, caso contrário não "
"pinta de todo."

#: ../../reference_manual/blending_modes/mix.rst:147
#: ../../reference_manual/blending_modes/mix.rst:151
msgid "Hard Mix"
msgstr "Mistura Forte"

#: ../../reference_manual/blending_modes/mix.rst:153
msgid "Similar to Overlay."
msgstr "Similar à Camada Sobreposta."

#: ../../reference_manual/blending_modes/mix.rst:155
msgid ""
"Mixes both Color Dodge and Burn blending modes. If the color of the upper "
"layer is darker than 50%, the blending mode will be Burn, if not the "
"blending mode will be Color Dodge."
msgstr ""
"Mistura ambos os modos de Desvio de Cor e Queimadura. Se a cor da camada "
"superior for mais escura que 50%, o modo de mistura será a Queimadura; caso "
"contrário, será o Desvio de Cor."

#: ../../reference_manual/blending_modes/mix.rst:161
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Hard_Mix_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Hard_Mix_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:161
msgid "Left: **Normal**. Right: **Hard Mix**."
msgstr "Esquerda: **Normal**. Direita: **Mistura Forte**."

#: ../../reference_manual/blending_modes/mix.rst:166
msgid "Hard Mix (Photoshop)"
msgstr "Mistura Forte (Photoshop)"

#: ../../reference_manual/blending_modes/mix.rst:168
msgid "This is the hard mix blending mode as it is implemented in photoshop."
msgstr ""
"Este é o modo de mistura forte, tal como está implementado no Photoshop."

#: ../../reference_manual/blending_modes/mix.rst:174
msgid ".. image:: images/blending_modes/mix/Krita_4_0_hard_mix_ps.png"
msgstr ".. image:: images/blending_modes/mix/Krita_4_0_hard_mix_ps.png"

#: ../../reference_manual/blending_modes/mix.rst:174
msgid ""
"**Left**: Dots are mixed in with the normal blending mode, on the **Right**: "
"Dots are mixed in with hardmix."
msgstr ""
"**Esquerda**: Os pontos estão misturados com o modo de mistura normal. "
"**Direita**: Os pontos estão misturados com a mistura forte."

#: ../../reference_manual/blending_modes/mix.rst:176
msgid ""
"This add the two values, and then checks if the value is above the maximum. "
"If so it will output the maximum, otherwise the minimum."
msgstr ""
"Isto adiciona os dois valores, verificando depois se o valor está acima do "
"máximo. Se for o caso, devolve o valor máximo; caso contrário, devolve o "
"mínimo."

#: ../../reference_manual/blending_modes/mix.rst:178
msgid "Hard OVerlay"
msgstr "Sobreposição Forte"

#: ../../reference_manual/blending_modes/mix.rst:182
msgid "Hard Overlay"
msgstr "Sobreposição Forte"

#: ../../reference_manual/blending_modes/mix.rst:186
msgid ""
"Similar to Hard light but hard light use Screen when the value is above 50%. "
"Divide gives better results than Screen, especially on floating point images."
msgstr ""
"Semelhante à Luz Forte, mas a Luz Forte usa o Ecrã quando o valor for acima "
"de 50%. A Divisão gera melhores resultados que o Ecrã, especialmente nas "
"imagens de vírgula flutuante."

#: ../../reference_manual/blending_modes/mix.rst:191
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Hard_Overlay_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Hard_Overlay_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:191
msgid "Left: **Normal**. Right: **Hard Overlay**."
msgstr "Esquerda: **Normal**. Direita: **Sobreposição Forte**."

#: ../../reference_manual/blending_modes/mix.rst:193
msgid "Normal (Blending Mode)"
msgstr "Normal (Modo de Mistura)"

#: ../../reference_manual/blending_modes/mix.rst:193
msgid "Source Over"
msgstr "Origem Sobreposta"

#: ../../reference_manual/blending_modes/mix.rst:197
msgid "Normal"
msgstr "Normal"

#: ../../reference_manual/blending_modes/mix.rst:199
msgid ""
"As you may have guessed this is the default Blending mode for all layers."
msgstr ""
"Como poderá ter percebido, este é o modo de mistura predefinido para todas "
"as camadas."

#: ../../reference_manual/blending_modes/mix.rst:201
msgid ""
"In this mode, the computer checks on the upper layer how transparent a pixel "
"is, which color it is, and then mixes the color of the upper layer with the "
"lower layer proportional to the transparency."
msgstr ""
"Neste modo, o computador verifica na camada superior quão transparente é um "
"dado pixel, qual a sua cor, e então mistura a cor da camada superior com a "
"camada inferior, na proporção da sua transparência."

#: ../../reference_manual/blending_modes/mix.rst:206
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Normal_50_Opacity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Normal_50_Opacity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:206
msgid "Left: **Normal** 100% Opacity. Right: **Normal** 50% Opacity."
msgstr ""
"Esquerda: **Normal** 100% Opacidade. Direita: **Normal** 50% Opacidade."

#: ../../reference_manual/blending_modes/mix.rst:208
msgid "Overlay (Blending Mode)"
msgstr "Sobreposição (Modo de Mistura)"

#: ../../reference_manual/blending_modes/mix.rst:212
msgid "Overlay"
msgstr "Sobreposição"

#: ../../reference_manual/blending_modes/mix.rst:214
msgid ""
"A combination of the Multiply and Screen blending modes, switching between "
"both at a middle-lightness."
msgstr ""
"Uma combinação dos modos de mistura Multiplicação e Ecrã, alternando entre "
"ambos a meio da luminosidade."

#: ../../reference_manual/blending_modes/mix.rst:216
msgid ""
"Overlay checks if the color on the upperlayer has a lightness above 0.5. If "
"so, the pixel is blended like in Screen mode, if not the pixel is blended "
"like in Multiply mode."
msgstr ""
"A Sobreposição verifica se a cor na camada superior tem uma luminosidade "
"acima de 0,5. Se for o caso, o pixel é misturado como no modo do Ecrã; caso "
"contrário, o pixel é misturado como no modo de Multiplicação."

#: ../../reference_manual/blending_modes/mix.rst:218
msgid "This is useful for deepening shadows and highlights."
msgstr "Isto é útil para aprofundar as sombras e os tons claros."

#: ../../reference_manual/blending_modes/mix.rst:223
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Overlay_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Overlay_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:223
msgid "Left: **Normal**. Right: **Overlay**."
msgstr "Esquerda: **Normal**. Direita: **Sobreposição**."

#: ../../reference_manual/blending_modes/mix.rst:225
#: ../../reference_manual/blending_modes/mix.rst:229
msgid "Parallel"
msgstr "Paralela"

#: ../../reference_manual/blending_modes/mix.rst:231
msgid ""
"This one first takes the percentage in two decimal behind the comma for both "
"layers. It then adds the two values. Divides 2 by the sum."
msgstr ""
"Este modo primeiro extrai a percentagem com duas casas decimais atrás da "
"vírgula para ambas as camadas. Depois adiciona os dois valores e finalmente "
"divide 2 pela soma."

#: ../../reference_manual/blending_modes/mix.rst:238
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Parallel_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Parallel_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:238
msgid "Left: **Normal**. Right: **Parallel**."
msgstr "Esquerda: **Normal**. Direita: **Paralela**."

#: ../../reference_manual/blending_modes/mix.rst:240
#: ../../reference_manual/blending_modes/mix.rst:244
msgid "Penumbra A"
msgstr "Penumbra A"

#: ../../reference_manual/blending_modes/mix.rst:246
msgid ""
"Creates a linear penumbra falloff. This means most tones will be in the "
"midtone ranges."
msgstr ""
"Cria um decaimento linear da penumbra. Isto significa que a maioria dos tons "
"estarão na gama dos tons intermédios."

#: ../../reference_manual/blending_modes/mix.rst:251
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_A_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_A_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:251
msgid "Left: **Normal**. Right: **Penumbra A**."
msgstr "Esquerda: **Normal**. Direita: **Penumbra A**."

#: ../../reference_manual/blending_modes/mix.rst:253
#: ../../reference_manual/blending_modes/mix.rst:257
msgid "Penumbra B"
msgstr "Penumbra B"

#: ../../reference_manual/blending_modes/mix.rst:259
msgid "Penumbra A with source and destination layer swapped."
msgstr "Penumbra A com a camada de origem e de destino trocadas."

#: ../../reference_manual/blending_modes/mix.rst:264
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_B_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_B_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:264
msgid "Left: **Normal**. Right: **Penumbra B**."
msgstr "Esquerda: **Normal**. Direita: **Penumbra B**."

#: ../../reference_manual/blending_modes/mix.rst:266
#: ../../reference_manual/blending_modes/mix.rst:270
msgid "Penumbra C"
msgstr "Penumbra C"

#: ../../reference_manual/blending_modes/mix.rst:272
msgid ""
"Creates a penumbra-like falloff using arc-tangent formula. This means most "
"tones will be in the midtone ranges."
msgstr ""
"Cria um decaimento semelhante à penumbra com uma fórmula de arco-tangente. "
"Isto significa que a maioria dos tons estarão na gama dos tons intermédios."

#: ../../reference_manual/blending_modes/mix.rst:277
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_C_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_C_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:277
msgid "Left: **Normal**. Right: **Penumbra C**."
msgstr "Esquerda: **Normal**. Direita: **Penumbra C**."

#: ../../reference_manual/blending_modes/mix.rst:279
#: ../../reference_manual/blending_modes/mix.rst:283
msgid "Penumbra D"
msgstr "Penumbra D"

#: ../../reference_manual/blending_modes/mix.rst:285
msgid "Penumbra C with source and destination layer swapped."
msgstr "Penumbra C com a camada de origem e de destino trocadas."

#: ../../reference_manual/blending_modes/mix.rst:290
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_D_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_D_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:290
msgid "Left: **Normal**. Right: **Penumbra D**."
msgstr "Esquerda: **Normal**. Direita: **Penumbra D**."
