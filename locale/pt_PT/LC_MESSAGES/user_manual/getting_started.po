# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-08 17:37+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: basicconcepts installation referencemanual Krita\n"
"X-POFile-SpellExtra: navigation usermanual\n"
"X-POFile-SpellExtra: introductionfromothersoftware startingwithkrita ref\n"
"X-POFile-SpellExtra: generalconcepts\n"

#: ../../user_manual/getting_started.rst:5
msgid "Getting Started"
msgstr "Começar"

#: ../../user_manual/getting_started.rst:7
msgid ""
"Welcome to the Krita Manual! In this section, we'll try to get you up to "
"speed."
msgstr ""
"Bem-vindo(a) ao Manual do Krita! Nesta secção, iremos tentar informá-lo "
"rapidamente."

#: ../../user_manual/getting_started.rst:9
msgid ""
"If you are familiar with digital painting, we recommend checking out the :"
"ref:`introduction_from_other_software` category, which contains guides that "
"will help you get familiar with Krita by comparing its functions to other "
"software."
msgstr ""
"Se estiver familiarizado com a pintura digital, recomendamos que consulte a "
"categoria :ref:`introduction_from_other_software`, que contém alguns guias "
"que o informam rapidamente sobre o Krita, comparando as suas funções com as "
"dos outros programas."

#: ../../user_manual/getting_started.rst:11
msgid ""
"If you are new to digital art, just start with :ref:`installation`, which "
"deals with installing Krita, and continue on to :ref:`starting_with_krita`, "
"which helps with making a new document and saving it, :ref:`basic_concepts`, "
"in which we'll try to quickly cover the big categories of Krita's "
"functionality, and finally, :ref:`navigation`, which helps you find basic "
"usage help, such as panning, zooming and rotating."
msgstr ""
"Se a arte digital for uma novidade para si, pode começar pela :ref:"
"`installation`, que lida com a instalação do Krita e passar depois a :ref:"
"`starting_with_krita`, o que o irá ajudar a criar um novo documento e a "
"gravá-lo; veja os :ref:`basic_concepts`, no qual iremos tentar cobrir "
"rapidamente as grandes categorias de funcionalidades do Krita e, finalmente, "
"a :ref:`navigation`, que o ajuda a descobrir alguma ajuda básica na "
"utilização, como o posicionamento, a ampliação e a rotação."

#: ../../user_manual/getting_started.rst:13
msgid ""
"When you have mastered those, you can look into the dedicated introduction "
"pages for functionality in the :ref:`user_manual`, read through the "
"overarching concepts behind (digital) painting in the :ref:"
"`general_concepts` section, or just search the :ref:`reference_manual` for "
"what a specific button does."
msgstr ""
"Quando já dominar esses conceitos, poderá olhar para as páginas "
"introdutórias dedicadas a essas funcionalidades no :ref:`user_manual`; leia "
"os conceitos por trás da pintura (digital) na secção de  :ref:"
"`general_concepts`, ou simplesmente pesquise no :ref:`reference_manual` o "
"que faz um determinado botão."

#: ../../user_manual/getting_started.rst:15
msgid "Contents:"
msgstr "Conteúdo:"
