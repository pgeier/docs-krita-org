# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-20 00:05+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: KRA ODG en icons Krita image desagrupá\n"
"X-POFile-SpellExtra: Kritamouseright design images alt brushstroke\n"
"X-POFile-SpellExtra: Inkscape mouseright guilabel\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../user_manual/vector_graphics.rst:1
msgid "Overview of vector graphics in Krita."
msgstr "Introdução aos gráficos vectoriais no Krita."

#: ../../user_manual/vector_graphics.rst:11
msgid "Vector"
msgstr "Vector"

#: ../../user_manual/vector_graphics.rst:16
msgid "Vector Graphics"
msgstr "Gráficos Vectoriais"

#: ../../user_manual/vector_graphics.rst:18
msgid ""
"Krita 4.0 has had a massive rewrite of the vector tools. So here's a page "
"explaining the vector tools:"
msgstr ""
"O Krita 4.0 recebeu uma grande remodelação das ferramentas vectoriais. Como "
"tal, existe uma página que explica em detalhe as ferramentas vectoriais:"

#: ../../user_manual/vector_graphics.rst:21
msgid "What are vector graphics?"
msgstr "O que são os gráficos vectoriais?"

#: ../../user_manual/vector_graphics.rst:23
msgid ""
"Krita is primarily a raster graphics editing tool, which means that most of "
"the editing changes the values of the pixels on the raster that makes up the "
"image."
msgstr ""
"O Krita é principalmente uma ferramenta de edição gráfica rasterizada, o que "
"significa que a maioria das tarefas de edição modifica os valores dos pixels "
"que compõem a imagem."

#: ../../user_manual/vector_graphics.rst:26
msgid ".. image:: images/Pixels-brushstroke.png"
msgstr ".. image:: images/Pixels-brushstroke.png"

#: ../../user_manual/vector_graphics.rst:27
msgid ""
"Vector graphics on the other hand use mathematics to describe a shape. "
"Because it uses a formula, vector graphics can be resized to any size."
msgstr ""
"Os gráficos vectoriais, por outro lado, usam matemática para descrever uma "
"forma. Como usa uma fórmula, os gráficos vectoriais podem ser dimensionados "
"para qualquer tamanho."

#: ../../user_manual/vector_graphics.rst:29
msgid ""
"On one hand, this makes vector graphics great for logos and banners. On the "
"other hand, raster graphics are much easier to edit, so vectors tend to be "
"the domain of deliberate design, using a lot of precision."
msgstr ""
"Por um lado, isto torna os gráficos vectoriais para logótipos e separadores. "
"Por outro lado, os gráficos vectoriais são muito mais simples de editar, "
"pelo que os vectores tendem a dominar o 'design' deliberado, usando bastante "
"precisão."

#: ../../user_manual/vector_graphics.rst:32
msgid "Tools for making shapes"
msgstr "Ferramentas para criar formas"

#: ../../user_manual/vector_graphics.rst:34
msgid ""
"You can start making vector graphics by first making a vector layer (press "
"the arrow button next to the :guilabel:`+` in the layer docker to get extra "
"layer types). Then, all the usual drawing tools outside of the freehand, "
"dynamic and the multibrush tool can be used to draw shapes."
msgstr ""
"Poderá começar a criar gráficos vectoriais, criando primeiro uma camada "
"vectorial (carregue no botão da seta a seguir ao :guilabel:`+`, na área de "
"camadas, para obter os tipos de camadas extra). Depois, todas as ferramentas "
"de desenho normais, que não o desenho à mão-livre, o multi-pincel ou o "
"pincel dinâmico, podem ser usados para desenhar formas."

#: ../../user_manual/vector_graphics.rst:36
msgid ""
"The path and polyline tool are the tools you used most often on a vector "
"layer, as they allow you to make the most dynamic of shapes."
msgstr ""
"A ferramenta do caminho e da linha poligonal são as ferramentas que usará "
"com maior frequência numa camada vectorial, já que lhe permitem gerar o "
"máximo de dinâmica das formas."

#: ../../user_manual/vector_graphics.rst:38
msgid ""
"On the other hand, the :guilabel:`Ellipse` and :guilabel:`Rectangle` tools "
"allow you to draw special shapes, which then can be edited to make special "
"pie shapes, or for easy rounded rectangles."
msgstr ""
"Por outro lado, as ferramentas da :guilabel:`Elipse` e do :guilabel:"
"`Rectângulo` permitem-lhe desenhar formas especiais, que por sua vez podem "
"ser editadas para criar formas de tarte especiais ou rectângulos "
"arredondados simples."

#: ../../user_manual/vector_graphics.rst:40
msgid ""
"The calligraphy and text tool also make special vectors. The calligraphy "
"tool is for producing strokes that are similar to brush strokes, while the "
"text tool makes a text object that can be edited afterwards."
msgstr ""
"A ferramenta de caligrafia e de texto também geram vectores especiais. A "
"ferramenta de caligrafia serve para produzir traços que sejam semelhantes "
"aos traços do pincel, enquanto a ferramenta de texto cria um objecto de "
"texto que pode ser editado posteriormente."

#: ../../user_manual/vector_graphics.rst:42
msgid ""
"All of these will use the current brush size to determine stroke thickness, "
"as well as the current foreground and background color."
msgstr ""
"Todas estas ferramentas irão usar o tamanho actual do pincel para definir a "
"espessura do traço, assim como as cores principal e de fundo."

#: ../../user_manual/vector_graphics.rst:44
msgid ""
"There is one last way to make vectors: the :guilabel:`Vector Image` tool.  "
"It allows you to add shapes that have been defined in an SVG file as "
"symbols. Unlike the other tools, these have their own fill and stroke."
msgstr ""
"Existe uma última forma de criar vectores: a área da guilabel:`Biblioteca de "
"Vectores`. Permite-lhe adicionar formas que foram definidas num ficheiro SVG "
"como símbolos. Ao contrário das outras ferramentas, estas têm o seu próprio "
"preenchimento e traços."

#: ../../user_manual/vector_graphics.rst:47
msgid "Arranging Shapes"
msgstr "Organizar as Formas"

#: ../../user_manual/vector_graphics.rst:49
msgid ""
"A vector layer has its own hierarchy of shapes, much like how the whole "
"image has a hierarchy of layers. So shapes can be in front of one another. "
"This can be modified with the arrange docker, or with the :guilabel:`Select "
"Shapes` tool."
msgstr ""
"Uma camada vectorial tem a sua própria hierarquia de formas, assim como a "
"imagem completa tem a sua própria hierarquia de formas. Como tal, as formas "
"podem estar à frente umas das outras. Isto pode ser modificado com a área de "
"reorganização ou com a Ferramenta de :guilabel:`Selecção de Formas`."

#: ../../user_manual/vector_graphics.rst:51
msgid ""
"The arrange docker also allows you to group and ungroup shapes. It also "
"allows you to precisely align shapes, for example, have them aligned to the "
"center, or have an even spacing between all the shapes."
msgstr ""
"A área de reorganização também lhe permite agrupar e desagrupar as formas. "
"Permite ainda alinhar com precisão as formas, como por exemplo alinhá-las ao "
"centro, ou ainda ter um espaçamento proporcional entre todas as formas."

#: ../../user_manual/vector_graphics.rst:54
msgid "Editing shapes"
msgstr "Editar as formas"

#: ../../user_manual/vector_graphics.rst:56
msgid ""
"Editing of vector shapes is done with the :guilabel:`Select Shapes` tool and "
"the :guilabel:`Edit Shapes` tool."
msgstr ""
"A edição das formas vectoriais é feita com a Ferramenta de guilabel:"
"`Selecção de Formas` e a ferramenta de guilabel:`Edição de Formas`."

#: ../../user_manual/vector_graphics.rst:58
msgid ""
"The :guilabel:`Select Shapes` tool can be used to select vector shapes, to "
"group them (via |mouseright|), ungroup them, to use booleans to combine or "
"subtract shapes from one another (via |mouseright|), to move them up and "
"down, or to do quick transforms."
msgstr ""
"A ferramenta para :guilabel:`Seleccionar as Formas` pode ser usada para "
"seleccionar as formas vectoriais, para as agrupar (com o |mouseright|), "
"desagrupá-las, usar lógica booleana para combinar ou subtrair as formas umas "
"com as outras (com o |mouseright|), elevá-las ou baixá-las ou fazer "
"transformações rápidas."

#: ../../user_manual/vector_graphics.rst:61
msgid "Fill"
msgstr "Preenchimento"

#: ../../user_manual/vector_graphics.rst:63
msgid ""
"You can change the fill of a shape by selecting it and changing the active "
"foreground color."
msgstr ""
"Poderá alterar o preenchimento de uma forma, seleccionando-a e alterando a "
"cor principal activa."

#: ../../user_manual/vector_graphics.rst:65
msgid ""
"You can also change it by going into the tool options of the :guilabel:"
"`Select Shapes` tool and going to the :guilabel:`Fill` tab."
msgstr ""
"Também o poderá alterar se for às opções da Ferramenta de :guilabel:"
"`Selecção de Formas` e for à página :guilabel:`Preenchimento`."

#: ../../user_manual/vector_graphics.rst:67
msgid ""
"Vector shapes can be filled with a solid color, a gradient or a pattern."
msgstr ""
"As formas vectoriais podem ser preenchidas com uma cor única, um gradiente "
"ou um padrão."

#: ../../user_manual/vector_graphics.rst:70
msgid "Stroke"
msgstr "Traço"

#: ../../user_manual/vector_graphics.rst:72
msgid "Strokes can be filled with the same things as fills."
msgstr ""
"Os traços podem ser preenchidos com as mesmas coisas que os preenchimentos "
"normais."

#: ../../user_manual/vector_graphics.rst:74
msgid ""
"However, they can also be further changed. For example, you can add dashes "
"and markers to the line."
msgstr ""
"Contudo, também podem ser mais alterados. Por exemplo, poderá adicionar "
"traços e marcações à linha."

#: ../../user_manual/vector_graphics.rst:77
msgid "Coordinates"
msgstr "Coordenadas"

#: ../../user_manual/vector_graphics.rst:79
msgid ""
"Shapes can be moved with the :guilabel:`Select Shapes` tool, and in the tool "
"options you can specify the exact coordinates."
msgstr ""
"Pode mover as formas com a ferramenta de :guilabel:`Selecção de Formas` e, "
"nas opções da ferramenta, poderá indicar as coordenadas exactas."

#: ../../user_manual/vector_graphics.rst:82
msgid "Editing nodes and special parameters"
msgstr "Edição dos nós e parâmetros especiais"

#: ../../user_manual/vector_graphics.rst:84
msgid ""
"If you have a shape selected, you can double click it to get to the "
"appropriate tool to edit it. Usually this is the :guilabel:`Edit Shape` "
"tool, but for text this is the :guilabel:`Text` tool."
msgstr ""
"Se tiver uma forma seleccionada, poderá fazer duplo-click sobre ela para "
"aceder à ferramenta apropriada para a editar. Normalmente costuma ser a "
"ferramenta para :guilabel:`Editar a Forma`, mas no caso do texto poderá ser "
"a ferramenta de :guilabel:`Texto`."

#: ../../user_manual/vector_graphics.rst:86
msgid ""
"In the :guilabel:`Edit Shape` tool, you can move around nodes on the canvas "
"for regular paths. For special paths, like the ellipse and the rectangle, "
"you can move nodes and edit the specific parameters in the :guilabel:`Tool "
"Options` docker."
msgstr ""
"Na ferramenta para :guilabel:`Editar a Forma`, poderá mover os nós na área "
"de desenho para os caminhos normais. Para os caminhos especiais, como a "
"elipse e o rectângulo, poderá mover os nós e editar os parâmetros "
"específicos na área de :guilabel:`Opções da Ferramenta`."

#: ../../user_manual/vector_graphics.rst:89
msgid "Working together with other programs"
msgstr "Trabalhar em conjunto com outros programas"

#: ../../user_manual/vector_graphics.rst:91
msgid ""
"One of the big things Krita 4.0 brought was moving from ODG to SVG. What "
"this means is that Krita saves as SVG inside KRA files, and that means we "
"can open SVGs just fine. This is important as SVG is the most popular vector "
"format."
msgstr ""
"Um dos pontos principais que o Krita 4.0 trouxe foi mover-se do ODG para o "
"SVG. O que isto significa é que o Krita grava como SVG dentro dos ficheiros "
"KRA, o que também significa que é possível abrir perfeitamente os SVG's. "
"Isto é importante, dado que o SVG é o formato vectorial mais conhecido."

#: ../../user_manual/vector_graphics.rst:94
msgid "Inkscape"
msgstr "Inkscape"

#: ../../user_manual/vector_graphics.rst:96
msgid ""
"You can copy and paste vectors from Krita to Inkscape, or from Inkscape to "
"Krita. Only the SVG 1.1 features are supported, so don't be surprised if a "
"mesh gradient doesn't cross over very well."
msgstr ""
"Pode copiar e colar os vectores do Krita para o Inkscape, ou do Inkscape "
"para o Krita. Só são suportadas as funcionalidades do SVG 1.1, por isso não "
"se admire se um gradiente em malha não for migrado correctamente."
