# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:35+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en guilabel image Csvspreadsheet frames images Calc\n"
"X-POFile-SpellExtra: Separated Csvtvpcsvexport program Studio Comma csv\n"
"X-POFile-SpellExtra: ref filepng LibreOffice Krita TVPaint sRGB Values\n"
"X-POFile-SpellExtra: Moho Blender\n"

#: ../../general_concepts/file_formats/file_csv.rst:1
msgid "The CSV file format as exported by Krita."
msgstr "O formato de ficheiros CSV exportado pelo Krita."

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "*.csv"
msgstr "*.csv"

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "CSV"
msgstr "CSV"

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "Comma Separated Values"
msgstr "Valores Separados por Vírgulas"

#: ../../general_concepts/file_formats/file_csv.rst:15
msgid "\\*.csv"
msgstr "\\*.csv"

#: ../../general_concepts/file_formats/file_csv.rst:17
msgid ""
"``.csv`` is the abbreviation for Comma Separated Values. It is an open, "
"plain text spreadsheet format. Since the CSV format is a plain text itself, "
"it is possible to use a spreadsheet program or even a text editor to edit "
"the ``*.csv`` file."
msgstr ""
"O ``.csv`` é a abreviatura de Comma Separated Values (Valores Separados por "
"Vírgulas). É um formato de folhas de cálculo em texto simples e aberto. Dado "
"que o formato .csv é um texto simples por si só, é possível usar um programa "
"de folhas de cálculo ou mesmo um editor de texto para editar o ficheiro ``*."
"csv``."

#: ../../general_concepts/file_formats/file_csv.rst:19
msgid ""
"Krita supports the CSV version used by TVPaint, to transfer layered "
"animation between these two softwares and probably with others, like "
"Blender. This is not an image sequence format, so use the document loading "
"and saving functions in Krita instead of the :guilabel:`Import animation "
"frames` and :guilabel:`Render Animation` menu items."
msgstr ""
"O Krita suporta a versão do CSV usada pelo TVPaint para transferir uma "
"animação por camadas entre estas duas aplicações e provavelmente para "
"outras, como o Blender. Isto não um formato de sequência de imagens, por "
"isso use as funções de carregamento e gravação do Krita em vez de usar as "
"opções do menu :guilabel:`Importar as imagens da animação` e :guilabel:"
"`Exportar a Animação`."

#: ../../general_concepts/file_formats/file_csv.rst:21
msgid ""
"The format consists of a text file with ``.csv`` extension, together with a "
"folder under the same name and a ``.frames`` extension. The CSV file and the "
"folder must be on the same path location. The text file contains the "
"parameters for the scene, like the field resolution and frame rate, and also "
"contains the exposure sheet for the layers. The folder contains :ref:"
"`file_png` picture files. Unlike image sequences, a key frame instance is "
"only a single file and the exposure sheet links it to one or more frames on "
"the timeline."
msgstr ""
"O formato consiste num ficheiro de texto com a extensão ``.csv``, em "
"conjunto com uma pasta com o mesmo nome e uma extensão ``.frames``. O "
"ficheiro ``.csv`` e a pasta devem estar no mesmo local. O ficheiro de texto "
"contém os parâmetros da cena, como a resolução do campo e a taxa de imagens, "
"contendo também a tabela de exposição das camadas. A pasta contém ficheiros "
"de imagem em :ref:`file_png`. Ao contrário das sequências  de imagens, uma "
"instância de uma imagem-chave é apenas um único ficheiro e a tabela de "
"exposições associadas associa-a a uma ou mais imagens na linha temporal."

#: ../../general_concepts/file_formats/file_csv.rst:26
msgid ".. image:: images/Csv_spreadsheet.png"
msgstr ".. image:: images/Csv_spreadsheet.png"

#: ../../general_concepts/file_formats/file_csv.rst:26
msgid "A ``.csv`` file as a spreadsheet in :program:`LibreOffice Calc`."
msgstr ""
"Um ficheiro ``.csv`` como una folha de cálculo no :program:`LibreOffice "
"Calc`."

#: ../../general_concepts/file_formats/file_csv.rst:28
msgid ""
"Krita can both export and import this format. It is recommended to use 8bit "
"sRGB color space because that's the only color space for :program:`TVPaint`. "
"Layer groups and layer masks are also not supported."
msgstr ""
"O Krita consegue tanto exportar como importar este formato. Recomenda-se que "
"use o espaço de cores sRGB a 8 bits, dado que é o único espaço de cores para "
"o :program:`TVPaint`. As camadas de grupos e as máscaras de camadas também "
"não são suportadas."

#: ../../general_concepts/file_formats/file_csv.rst:30
msgid ""
"TVPaint can only export this format by itself. In :program:`TVPaint 11`, use "
"the :guilabel:`Export to...` option of the :guilabel:`File` menu, and on the "
"upcoming :guilabel:`Export footage` window, use the :guilabel:`Clip: Layers "
"structure` tab."
msgstr ""
"O TVPaint só consegue exportar este formato por si só. No :program:`TVPaint "
"11`, use a opção :guilabel:`Exportar para...` do menu :guilabel:` e na "
"janela:guilabel:`Exportar a imagem` que aparece, use a página para :guilabel:"
"`Recortar as Camadas` t."

#: ../../general_concepts/file_formats/file_csv.rst:35
msgid ".. image:: images/Csv_tvp_csvexport.png"
msgstr ".. image:: images/Csv_tvp_csvexport.png"

#: ../../general_concepts/file_formats/file_csv.rst:35
msgid "Exporting into ``.csv`` in TVPaint."
msgstr "Exportar para ``.csv`` no TVPaint."

#: ../../general_concepts/file_formats/file_csv.rst:37
msgid ""
"To import this format back into TVPaint there is a George language script "
"extension. See the \"Packs, Plugins, Third party\" section on the TVPaint "
"community forum for more details and also if you need support for other "
"softwares. Moho/Anime Studio and Blender also have plugins to import this "
"format."
msgstr ""
"Para importar este formato de volta para o TVPaint, existe uma extensão de "
"um programa na linguagem George. Veja a secção de \"Pacotes, Plugins, "
"Terceiros\" no fórum da comunidade do TVPaint para saber mais detalhes e "
"também se precisa de suporte para outras aplicações. O Moho/Anime Studio e o "
"Blender também têm 'plugins' para importar este formato."

#: ../../general_concepts/file_formats/file_csv.rst:42
msgid ""
"`CSV import script for TVPaint <https://forum.tvpaint.com/viewtopic.php?"
"f=26&t=9759>`_."
msgstr ""
"`programa de importação de CSV do TVPaint <https://forum.tvpaint.com/"
"viewtopic.php?f=26&t=9759>`_."

#: ../../general_concepts/file_formats/file_csv.rst:43
msgid ""
"`CSV import script for Moho/Anime Studio <https://forum.tvpaint.com/"
"viewtopic.php?f=26&t=10050>`_."
msgstr ""
"`programa de importação de CSV para o Moho/Anime Studio <https://forum."
"tvpaint.com/viewtopic.php?f=26&t=10050>`_."

#: ../../general_concepts/file_formats/file_csv.rst:44
msgid ""
"`CSV import script for Blender <https://developer.blender.org/T47462>`_."
msgstr ""
"`programa de importação de CSV para o Blender <https://developer.blender.org/"
"T47462>`_."
