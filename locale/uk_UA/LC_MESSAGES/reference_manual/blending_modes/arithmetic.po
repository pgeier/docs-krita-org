# Translation of docs_krita_org_reference_manual___blending_modes___arithmetic.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___blending_modes___arithmetic\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:21+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/blending_modes/arithmetic.rst:1
msgid ""
"Page about the arithmetic blending modes in Krita: Addition, Divide, Inverse "
"Subtract, Multiply and Subtract."
msgstr ""
"Сторінка, присвячена режимам арифметичного злиття у Krita: додаванню, "
"діленню, зворотному відніманню, множенню та відніманню."

#: ../../reference_manual/blending_modes/arithmetic.rst:14
msgid "Arithmetic"
msgstr "Арифметика"

#: ../../reference_manual/blending_modes/arithmetic.rst:16
msgid "These blending modes are based on simple maths."
msgstr "Ці режими злиття засновано на простих математичних принципах."

#: ../../reference_manual/blending_modes/arithmetic.rst:18
msgid "Addition (Blending Mode)"
msgstr "Додавання (режим змішування)"

#: ../../reference_manual/blending_modes/arithmetic.rst:22
msgid "Addition"
msgstr "Додавання"

#: ../../reference_manual/blending_modes/arithmetic.rst:24
msgid "Adds the numerical values of two colors together:"
msgstr "Додає числові значення двох кольорів:"

#: ../../reference_manual/blending_modes/arithmetic.rst:26
msgid "Yellow(1, 1, 0) + Blue(0, 0, 1) = White(1, 1, 1)"
msgstr "Жовтий(1, 1, 0) + Синій(0, 0, 1) = Білий(1, 1, 1)"

#: ../../reference_manual/blending_modes/arithmetic.rst:28
msgid ""
"Darker Gray(0.4, 0.4, 0.4) + Lighter Gray(0.5, 0.5, 0.5) = Even Lighter Gray "
"(0.9, 0.9, 0.9)"
msgstr ""
"Темно-сірий(0.4, 0.4, 0.4) + Світло-сірий(0.5, 0.5, 0.5) = Ще-світліший-"
"сірий(0.9, 0.9, 0.9)"

#: ../../reference_manual/blending_modes/arithmetic.rst:33
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:33
#: ../../reference_manual/blending_modes/arithmetic.rst:40
#: ../../reference_manual/blending_modes/arithmetic.rst:47
#: ../../reference_manual/blending_modes/arithmetic.rst:54
msgid "Left: **Normal**. Right: **Addition**."
msgstr "Ліворуч: **Звичайне**. Праворуч: **Додавання**."

#: ../../reference_manual/blending_modes/arithmetic.rst:35
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) + Orange(1, 0.5961, 0.0706) = (1.1608, "
"1.2235, 0.8980) → Very Light Yellow(1, 1, 0.8980)"
msgstr ""
"Світло-синій(0.1608, 0.6274, 0.8274) + Помаранчевий(1, 0.5961, 0.0706) = "
"(1.1608, 1.2235, 0.8980) → Дуже світлий жовтий(1, 1, 0.8980)"

#: ../../reference_manual/blending_modes/arithmetic.rst:40
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:42
msgid "Red(1, 0, 0) + Gray(0.5, 0.5, 0.5) = Pink(1, 0.5, 0.5)"
msgstr "Червоний(1, 0, 0) + Сірий(0.5, 0.5, 0.5) = Рожевий(1, 0.5, 0.5)"

#: ../../reference_manual/blending_modes/arithmetic.rst:47
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Red_plus_gray.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Red_plus_gray.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:49
msgid ""
"When the result of the addition is more than 1, white is the color "
"displayed. Therefore, white plus any other color results in white. On the "
"other hand, black plus any other color results in the added color."
msgstr ""
"Якщо результат додавання перевищуватиме 1, буде показано білий колір. Через "
"це, додавання білого кольору до будь-якого іншого кольору дає білий. З "
"іншого боку, додавання чорного до будь-якого кольору не змінить значення "
"цього кольору."

#: ../../reference_manual/blending_modes/arithmetic.rst:54
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:59
msgid "Divide"
msgstr "Ділення"

#: ../../reference_manual/blending_modes/arithmetic.rst:61
msgid "Divides the numerical value from the lower color by the upper color."
msgstr ""
"Ділить числове значення для кольору нижчого шару на числове значення для "
"кольору вищого шару."

#: ../../reference_manual/blending_modes/arithmetic.rst:63
msgid "Red(1, 0, 0) / Gray(0.5, 0.5, 0.5) = (2, 0, 0) → Red(1, 0, 0)"
msgstr ""
"Червоний(1, 0, 0) / Сірий(0.5, 0.5, 0.5) = (2, 0, 0) → Червоний(1, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:65
msgid ""
"Darker Gray(0.4, 0.4, 0.4) / Lighter Gray(0.5, 0.5, 0.5) = Even Lighter Gray "
"(0.8, 0.8, 0.8)"
msgstr ""
"Темно-сірий(0.4, 0.4, 0.4) + Світло-сірий(0.5, 0.5, 0.5) = Ще-світліший-"
"сірий(0.8, 0.8, 0.8)"

#: ../../reference_manual/blending_modes/arithmetic.rst:70
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:70
#: ../../reference_manual/blending_modes/arithmetic.rst:77
#: ../../reference_manual/blending_modes/arithmetic.rst:82
msgid "Left: **Normal**. Right: **Divide**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Ділення**."

#: ../../reference_manual/blending_modes/arithmetic.rst:72
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) / Orange(1, 0.5961, 0.0706) = (0.1608, "
"1.0525, 11.7195) → Aqua(0.1608, 1, 1)"
msgstr ""
"Світло-синій(0.1608, 0.6274, 0.8274) / Помаранчевий(1, 0.5961, 0.0706) = "
"(0.1608, 1.0525, 11.7195) → Аквамарин(0.1608, 1, 1)"

#: ../../reference_manual/blending_modes/arithmetic.rst:77
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:82
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:87
msgid "Inverse Subtract"
msgstr "Зворотне віднімання"

#: ../../reference_manual/blending_modes/arithmetic.rst:89
msgid ""
"This inverts the lower layer before subtracting it from the upper layer."
msgstr ""
"Інвертує колір нижчого шару перед відніманням його від кольору вищого шару."

#: ../../reference_manual/blending_modes/arithmetic.rst:91
msgid ""
"Lighter Gray(0.5, 0.5, 0.5)_(1_Darker Gray(0.4, 0.4, 0.4)) = (-0.1, -0.1, "
"-0.1) → Black(0, 0, 0)"
msgstr ""
"Світло-сірий(0.5, 0.5, 0.5)_(1_Темно-сірий(0.4, 0.4, 0.4)) = (-0.1, -0.1, "
"-0.1) → Чорний(0, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:96
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:96
#: ../../reference_manual/blending_modes/arithmetic.rst:103
#: ../../reference_manual/blending_modes/arithmetic.rst:108
msgid "Left: **Normal**. Right: **Inverse Subtract**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Зворотне віднімання**."

#: ../../reference_manual/blending_modes/arithmetic.rst:98
msgid ""
"Orange(1, 0.5961, 0.0706)_(1_Light Blue(0.1608, 0.6274, 0.8274)) = (0.1608, "
"0.2235, -0.102) → Dark Green(0.1608, 0.2235, 0)"
msgstr ""
"Помаранчевий(1, 0.5961, 0.0706)_(1_Світло-синій(0.1608, 0.6274, 0.8274)) = "
"(0.1608, 0.2235, -0.102) → Темно-зелений(0.1608, 0.2235, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:103
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:108
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:113
msgid "Multiply"
msgstr "Множення"

#: ../../reference_manual/blending_modes/arithmetic.rst:115
msgid ""
"Multiplies the two colors with each other, but does not go beyond the upper "
"limit."
msgstr ""
"Множить кольори, але не обмежує результат певним значенням верхньої межі."

#: ../../reference_manual/blending_modes/arithmetic.rst:117
msgid ""
"This is often used to color in a black and white lineart. One puts the black "
"and white lineart on top, and sets the layer to 'Multiply', and then draw in "
"color on a layer beneath. Multiply will all the color to go through."
msgstr ""
"Цим режимом часто користуються для розфарбовування чорно-білої графіки. Для "
"цього слід розташувати чорно-білу графіку у шарі згори, встановити для шару "
"режим змішування «Множення», а потім розфарбувати шар під ним. Множення "
"пропустить усі кольори до остаточного зображення."

#: ../../reference_manual/blending_modes/arithmetic.rst:120
msgid "White(1,1,1) x White(1, 1, 1) = White(1, 1, 1)"
msgstr "Білий(1,1,1) * Білий(1, 1, 1) = Білий(1, 1, 1)"

#: ../../reference_manual/blending_modes/arithmetic.rst:122
msgid "White(1, 1, 1) x Gray(0.5, 0.5, 0.5) = Gray(0.5, 0.5, 0.5)"
msgstr "Білий(1, 1, 1) * Сірий(0.5, 0.5, 0.5) = Сірий(0.5, 0.5, 0.5)"

#: ../../reference_manual/blending_modes/arithmetic.rst:124
msgid ""
"Darker Gray(0.4, 0.4, 0.4) x Lighter Gray(0.5, 0.5, 0.5) = Even Darker Gray "
"(0.2, 0.2, 0.2)"
msgstr ""
"Темно-сірий(0.4, 0.4, 0.4) X Світло-сірий(0.5, 0.5, 0.5) = Ще-темніший-"
"сірий(0.2, 0.2, 0.2)"

#: ../../reference_manual/blending_modes/arithmetic.rst:129
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:129
#: ../../reference_manual/blending_modes/arithmetic.rst:136
#: ../../reference_manual/blending_modes/arithmetic.rst:141
msgid "Left: **Normal**. Right: **Multiply**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Множення**."

#: ../../reference_manual/blending_modes/arithmetic.rst:131
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) x Orange(1, 0.5961, 0.0706) = "
"Green(0.1608, 0.3740, 0.0584)"
msgstr ""
"Світло-синій(0.1608, 0.6274, 0.8274) x Помаранчевий(1, 0.5961, 0.0706) = "
"Зелений(0.1608, 0.3740, 0.0584)"

#: ../../reference_manual/blending_modes/arithmetic.rst:136
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:141
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:146
msgid "Subtract"
msgstr "Віднімання"

#: ../../reference_manual/blending_modes/arithmetic.rst:148
msgid "Subtracts the top layer from the bottom layer."
msgstr "Віднімає від нижнього шару верхній шар."

#: ../../reference_manual/blending_modes/arithmetic.rst:150
msgid "White(1, 1, 1)_White(1, 1, 1) = Black(0, 0, 0)"
msgstr "Білий(1, 1, 1)_Білий(1, 1, 1) = Чорний(0, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:152
msgid "White(1, 1, 1)_Gray(0.5, 0.5, 0.5) = Gray(0.5, 0.5, 0.5)"
msgstr "Білий(1, 1, 1)_Сірий(0.5, 0.5, 0.5) = Сірий(0.5, 0.5, 0.5)"

#: ../../reference_manual/blending_modes/arithmetic.rst:154
msgid ""
"Darker Gray(0.4, 0.4, 0.4)_Lighter Gray(0.5, 0.5, 0.5) = (-0.1, -0.1, -0.1) "
"→ Black(0, 0, 0)"
msgstr ""
"Темно-сірий(0.4, 0.4, 0.4)_Світло-сірий(0.5, 0.5, 0.5) = (-0.1, -0.1, -0.1) "
"→ Чорний(0, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:159
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:159
#: ../../reference_manual/blending_modes/arithmetic.rst:166
#: ../../reference_manual/blending_modes/arithmetic.rst:171
msgid "Left: **Normal**. Right: **Subtract**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Віднімання**."

#: ../../reference_manual/blending_modes/arithmetic.rst:161
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) - Orange(1, 0.5961, 0.0706) = (-0.8392, "
"0.0313, 0.7568) → Blue(0, 0.0313, 0.7568)"
msgstr ""
"Світло-синій(0.1608, 0.6274, 0.8274) - Помаранчевий(1, 0.5961, 0.0706) = "
"(-0.8392, 0.0313, 0.7568) → Синій(0, 0.0313, 0.7568)"

#: ../../reference_manual/blending_modes/arithmetic.rst:166
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:171
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Sample_image_with_dots.png"
