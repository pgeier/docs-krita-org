# Translation of docs_krita_org_reference_manual___brushes___brush_settings___options.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_settings___options\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 08:25+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/brushes/Krita_3_0_1_Brush_engine_ratio.png"
msgstr ".. image:: images/brushes/Krita_3_0_1_Brush_engine_ratio.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:1
msgid "Krita's Brush Engine options overview."
msgstr "Огляд параметрів рушіїв пензлів Krita."

#: ../../reference_manual/brushes/brush_settings/options.rst:18
msgid "Options"
msgstr "Налаштування"

#: ../../reference_manual/brushes/brush_settings/options.rst:20
#: ../../reference_manual/brushes/brush_settings/options.rst:24
msgid "Airbrush"
msgstr "Аерограф"

#: ../../reference_manual/brushes/brush_settings/options.rst:27
msgid ".. image:: images/brushes/Krita_2_9_brushengine_airbrush.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_airbrush.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:28
msgid ""
"If you hold the brush still, but are still pressing down, this will keep "
"adding color onto the canvas. The lower the rate, the quicker the color gets "
"added."
msgstr ""
"Якщо ви утримуватимете пензель на полотні і тиснутимете на нього, програма "
"додаватиме колір на полотно. Чим повільніше рухатиметься пензель, тим швидше "
"додаватиметься колір."

#: ../../reference_manual/brushes/brush_settings/options.rst:30
#: ../../reference_manual/brushes/brush_settings/options.rst:34
msgid "Mirror"
msgstr "Віддзеркалити"

#: ../../reference_manual/brushes/brush_settings/options.rst:37
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Mirror.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Mirror.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:38
msgid "This allows you to mirror the Brush-tip with Sensors."
msgstr "Надає змогу віддзеркалити кінчик пензля з використанням датчиків."

#: ../../reference_manual/brushes/brush_settings/options.rst:40
msgid "Horizontal"
msgstr "Горизонтально"

#: ../../reference_manual/brushes/brush_settings/options.rst:41
msgid "Mirrors the mask horizontally."
msgstr "Віддзеркалює маску горизонтально."

#: ../../reference_manual/brushes/brush_settings/options.rst:43
msgid "Vertical"
msgstr "Вертикально"

#: ../../reference_manual/brushes/brush_settings/options.rst:43
msgid "Mirrors the mask vertically."
msgstr "Віддзеркалює маску вертикально."

#: ../../reference_manual/brushes/brush_settings/options.rst:46
msgid ".. image:: images/brushes/Krita_2_9_brushengine_mirror.jpg"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_mirror.jpg"

#: ../../reference_manual/brushes/brush_settings/options.rst:47
msgid ""
"Some examples of mirroring and using it in combination with :ref:"
"`option_rotation`."
msgstr ""
"Декілька прикладів віддзеркалення та використання віддзеркалення у поєднанні "
"з ефектом :ref:`option_rotation`."

#: ../../reference_manual/brushes/brush_settings/options.rst:52
msgid "Rotation"
msgstr "Обертання"

#: ../../reference_manual/brushes/brush_settings/options.rst:54
msgid "This allows you to affect Angle of your brush-tip with Sensors."
msgstr "Надає змогу впливати датчикам на кут вашого кінчика пензля."

#: ../../reference_manual/brushes/brush_settings/options.rst:57
msgid ".. image:: images/brushes/Krita_2_9_brushengine_rotation.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_rotation.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:59
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Rotation.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Rotation.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:60
msgid "In the above example, several applications of the parameter."
msgstr ""
"У наведеному вище прикладі можна бачити декілька застосувань цього параметра."

#: ../../reference_manual/brushes/brush_settings/options.rst:62
msgid ""
"Drawing Angle -- A common one, usually used in combination with rake-type "
"brushes. Especially effect because it does not rely on tablet-specific "
"sensors. Sometimes, Tilt-Direction or Rotation is used to achieve a similar-"
"more tablet focused effect, where with Tilt the 0° is at 12 o'clock, Drawing "
"angle uses 3 o'clock as 0°."
msgstr ""
"Кут малювання — загальний кут, зазвичай використовується у поєднанні із "
"граблеподібними пензлями. Особливо ефективний, оскільки не покладається на "
"специфічні для планшета датчики. Іноді, напрямок нахилу або обертання "
"використовується для отримання ефекту фокусування, подібнішого для планшета, "
"де нахил 0° відповідає напрямку 12 годин на табло годинника, а кут малювання "
"0° відповідає 3 годині."

#: ../../reference_manual/brushes/brush_settings/options.rst:63
msgid ""
"Fuzzy -- Also very common, this gives a nice bit of randomness for texture."
msgstr ""
"Неточний — теж доволі загальна річ, яка надає певної випадковості текстурі."

#: ../../reference_manual/brushes/brush_settings/options.rst:64
msgid ""
"Distance -- With careful editing of the Sensor curve, you can create nice "
"patterns."
msgstr ""
"Відстань — за допомогою вдумливого редагування кривої датчика ви можете "
"створювати чудові візерунки."

#: ../../reference_manual/brushes/brush_settings/options.rst:65
msgid "Fade -- This slowly fades the rotation from one into another."
msgstr ""
"Згасання — уможливлює повільне перетікання обертання з одного стану у інший."

#: ../../reference_manual/brushes/brush_settings/options.rst:66
msgid ""
"Pressure -- An interesting one that can create an alternative looking line."
msgstr ""
"Тиск — цікава залежність, яка може бути використана для створення "
"альтернативних варіантів ліній."

#: ../../reference_manual/brushes/brush_settings/options.rst:71
msgid "Scatter"
msgstr "Розкидати"

#: ../../reference_manual/brushes/brush_settings/options.rst:73
msgid ""
"This parameter allows you to set the random placing of a brush-dab. You can "
"affect them with Sensors."
msgstr ""
"За допомогою цього параметра можна визначити випадкове розташування мазка "
"пензлем. Можна також визначити вплив на випадкове розташування датчиків "
"стила."

#: ../../reference_manual/brushes/brush_settings/options.rst:75
msgid "X"
msgstr "X"

#: ../../reference_manual/brushes/brush_settings/options.rst:76
msgid "The scattering on the angle you are drawing from."
msgstr "Дисперсія за кутом, під яким ви малюєте."

#: ../../reference_manual/brushes/brush_settings/options.rst:78
msgid "Y"
msgstr "Y"

#: ../../reference_manual/brushes/brush_settings/options.rst:78
msgid ""
"The scattering, perpendicular to the drawing angle (has the most effect)."
msgstr ""
"Дисперсія перпендикулярно до кута малювання (найбільше впливає на вигляд "
"мазка)."

#: ../../reference_manual/brushes/brush_settings/options.rst:81
msgid ".. image:: images/brushes/Krita_2_9_brushengine_scatter.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_scatter.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:85
msgid "Sharpness"
msgstr "Різкість"

#: ../../reference_manual/brushes/brush_settings/options.rst:88
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Sharpness.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Sharpness.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:89
msgid ""
"Puts a threshold filter over the brush mask. This can be used for brush like "
"strokes, but it also makes for good pixel art brushes."
msgstr ""
"Накладає пороговий фільтр на маску пензля. Цим можна скористатися для "
"пензлеподібних мазків, але також це корисно і для пензлів для піксель-арту."

#: ../../reference_manual/brushes/brush_settings/options.rst:91
msgid "Strength"
msgstr "Потужність"

#: ../../reference_manual/brushes/brush_settings/options.rst:92
msgid "Controls the threshold, and can be controlled by the sensors below."
msgstr ""
"Керує пороговим значенням, можна використовувати для керування за допомогою "
"визначених нижче датчиків."

#: ../../reference_manual/brushes/brush_settings/options.rst:94
#: ../../reference_manual/brushes/brush_settings/options.rst:120
msgid "Softness"
msgstr "М’якість"

#: ../../reference_manual/brushes/brush_settings/options.rst:94
msgid ""
"Controls the extra non-fully opaque pixels. This adds a little softness to "
"the stroke."
msgstr ""
"Керує додатковими неповністю непрозорими пікселями. Додає м'якості мазку."

#: ../../reference_manual/brushes/brush_settings/options.rst:98
msgid ""
"The sensors now control the threshold instead of the subpixel precision, "
"softness slider was added."
msgstr ""
"Датчики тепер керують пороговим значенням, а не субпіксельною точністю. "
"Додано повзунок м'якості."

#: ../../reference_manual/brushes/brush_settings/options.rst:103
msgid "Size"
msgstr "Розмір"

#: ../../reference_manual/brushes/brush_settings/options.rst:106
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Size.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Size.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:107
msgid ""
"This parameter is not the diameter itself, but rather the curve for how it's "
"affected."
msgstr ""
"Цей параметр не є самим діаметром, а, краще сказати, кривою впливу на нього."

#: ../../reference_manual/brushes/brush_settings/options.rst:109
msgid ""
"So, if you want to lock the diameter of the brush, lock the Brush-tip. "
"Locking the size parameter will only lock this curve. Allowing this curve to "
"be affected by the Sensors can be very useful to get the right kind of "
"brush. For example, if you have trouble drawing fine lines, try to use a "
"concave curve set to pressure. That way you'll have to press hard for thick "
"lines."
msgstr ""
"Отже, якщо ви хочете заблокувати діаметр пензля, заблокуйте кінчик пензля. "
"Блокування параметра розміру заблокує лише цю криву. Уможливлення впливу "
"датчиків на цю криву може бути корисним для отримання правильного типу "
"пензля. Наприклад, якщо у вас виникають проблеми із малюванням тонких ліній, "
"спробуйте скористатися увігнутою кривою для тиску. З такою кривою вам "
"доведеться тиснути сильніше, щоб отримати товщі лінії."

#: ../../reference_manual/brushes/brush_settings/options.rst:112
msgid ".. image:: images/brushes/Krita_2_9_brushengine_size_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_size_01.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:113
msgid ""
"Also popular is setting the size to the sensor fuzzy or perspective, with "
"the later in combination with a :ref:`assistant_perspective`."
msgstr ""
"Також популярним є встановлення розміру для нечіткості або перспективи "
"датчика, із останнім у поєднанні з :ref:`assistant_perspective`."

#: ../../reference_manual/brushes/brush_settings/options.rst:116
msgid ".. image:: images/brushes/Krita_2_9_brushengine_size_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_size_02.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:122
msgid "This allows you to affect Fade with Sensors."
msgstr "Надає змогу впливати датчикам на згасання."

#: ../../reference_manual/brushes/brush_settings/options.rst:125
msgid ".. image:: images/brushes/Krita_2_9_brushengine_softness.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_softness.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:126
msgid ""
"Has a slight brush-decreasing effect, especially noticeable with soft-brush, "
"and is overall more noticeable on large brushes."
msgstr ""
"Має певний вплив на зменшення пензля, особливо помітний із м'якими пензлями "
"і, загалом, більш помітний для великих пензлів."

#: ../../reference_manual/brushes/brush_settings/options.rst:131
msgid "Source"
msgstr "Джерело"

#: ../../reference_manual/brushes/brush_settings/options.rst:133
msgid "Picks the source-color for the brush-dab."
msgstr "Вибирає колір джерела для мазка пензлем."

#: ../../reference_manual/brushes/brush_settings/options.rst:135
msgid "Plain Color"
msgstr "Звичайний колір"

#: ../../reference_manual/brushes/brush_settings/options.rst:136
msgid "Current foreground color."
msgstr "Поточний колір переднього плану."

#: ../../reference_manual/brushes/brush_settings/options.rst:137
#: ../../reference_manual/brushes/brush_settings/options.rst:169
msgid "Gradient"
msgstr "Градієнтний"

#: ../../reference_manual/brushes/brush_settings/options.rst:138
msgid "Picks active gradient."
msgstr "Вибирає активний градієнт."

#: ../../reference_manual/brushes/brush_settings/options.rst:139
msgid "Uniform Random"
msgstr "Однорідний випадковий"

#: ../../reference_manual/brushes/brush_settings/options.rst:140
msgid "Gives a random color to each brush dab."
msgstr "Надає кожному мазку пензля випадкового кольору."

#: ../../reference_manual/brushes/brush_settings/options.rst:141
msgid "Total Random"
msgstr "Повністю випадковий"

#: ../../reference_manual/brushes/brush_settings/options.rst:142
msgid "Random noise pattern is now painted."
msgstr "Вмикає режим малювання випадковим візерунком."

#: ../../reference_manual/brushes/brush_settings/options.rst:143
msgid "Pattern"
msgstr "Візерунок"

#: ../../reference_manual/brushes/brush_settings/options.rst:144
msgid "Uses active pattern, but alignment is different per stroke."
msgstr ""
"Використовує активний візерунок, але міняє вирівнювання для кожного штриха."

#: ../../reference_manual/brushes/brush_settings/options.rst:146
msgid "Locked Pattern"
msgstr "Фіксований візерунок"

#: ../../reference_manual/brushes/brush_settings/options.rst:146
msgid "Locks the pattern to the brushdab."
msgstr "Фіксує візерунок для мазка пензлем."

#: ../../reference_manual/brushes/brush_settings/options.rst:151
msgid "Mix"
msgstr "Змішування"

#: ../../reference_manual/brushes/brush_settings/options.rst:153
msgid ""
"Allows you to affect the mix of the :ref:`option_source` color with Sensors. "
"It will work with Plain Color and Gradient as source. If Plain Color is "
"selected as source, it will mix between foreground and background colors "
"selected in color picker. If Gradient is selected, it chooses a point on the "
"gradient to use as painting color according to the sensors selected."
msgstr ""
"Надає вам змогу впливати на суміш кольору :ref:`option_source` із датчиками. "
"Працюватиме із простим кольором або градієнтом як джерелом. Якщо як джерело "
"вибрано простий колір, програма змішуватиме кольори переднього та заднього "
"планів, які вибрано у засобі вибору кольорів. Якщо вибрано градієнт, "
"програма вибирає точку на градієнті, яку буде використано як колір "
"малювання, відповідно до вибраних датчиків."

#: ../../reference_manual/brushes/brush_settings/options.rst:156
msgid ".. image:: images/brushes/Krita_2_9_brushengine_mix_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_mix_01.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:158
msgid "Uses"
msgstr "Використання"

#: ../../reference_manual/brushes/brush_settings/options.rst:161
msgid ".. image:: images/brushes/Krita_2_9_brushengine_mix_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_mix_02.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:163
msgid ""
"The above example uses a :program:`Krita` painted flowmap in the 3D program :"
"program:`Blender`. A brush was set to :menuselection:`Source --> Gradient` "
"and :menuselection:`Mix --> Drawing angle`. The gradient in question "
"contained the 360° for normal map colors. Flow maps are used in several "
"Shaders, such as brushed metal, hair and certain river-shaders."
msgstr ""
"У наведеному вище прикладі використано намальовану у :program:`Krita` карту "
"потоків у програмі для тривимірного моделювання :program:`Blender`. Для "
"пензля встановлено :menuselection`Джерело --> Градієнт` та :menuselection:"
"`Суміш --> Кут малювання`. Розглянутий градієнт містить 360° для кольорів "
"нормальної карти. Карти потоків використовуються у декількох шейдерах, "
"зокрема карті подряпаного шліфуванням метала, волосся та деяких річкових "
"шейдерах."

#: ../../reference_manual/brushes/brush_settings/options.rst:164
msgid "Flow map"
msgstr "Карта потоку"

#: ../../reference_manual/brushes/brush_settings/options.rst:171
msgid ""
"Exactly the same as using :menuselection:`Source --> Gradient` with :"
"guilabel:`Mix`, but only available for the Color Smudge Brush."
msgstr ""
"Те саме, що і використання :menuselection:`Джерело --> Градієнт` з :guilabel:"
"`Суміш`, але доступне лише для пензля розмазування кольорів."

#: ../../reference_manual/brushes/brush_settings/options.rst:173
#: ../../reference_manual/brushes/brush_settings/options.rst:177
msgid "Spacing"
msgstr "Інтервал"

#: ../../reference_manual/brushes/brush_settings/options.rst:180
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Spacing.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Spacing.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:181
msgid "This allows you to affect :ref:`option_brush_tip` with :ref:`sensors`."
msgstr ""
"Надає змогу впливати :ref:`датчикам <sensors>` на значення :ref:`параметра "
"кінчика пензля <option_brush_tip>`."

#: ../../reference_manual/brushes/brush_settings/options.rst:184
msgid ".. image:: images/brushes/Krita_2_9_brushengine_spacing_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_spacing_02.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:186
msgid "Isotropic spacing"
msgstr "Ізотропний інтервал"

#: ../../reference_manual/brushes/brush_settings/options.rst:186
msgid ""
"Instead of the spacing being related to the ratio of the brush, it will be "
"on diameter only."
msgstr ""
"Усуває залежність інтервалу від співвідношення розмірів пензля. Інтервал "
"залежатиме лише від діаметра пензля."

#: ../../reference_manual/brushes/brush_settings/options.rst:189
msgid ".. image:: images/brushes/Krita_2_9_brushengine_spacing_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_spacing_01.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:190
#: ../../reference_manual/brushes/brush_settings/options.rst:194
msgid "Ratio"
msgstr "Пропорція"

#: ../../reference_manual/brushes/brush_settings/options.rst:196
msgid ""
"Allows you to change the ratio of the brush and bind it to parameters. This "
"also works for predefined brushes."
msgstr ""
"Надає вам змогу змінити пропорції пензля і пов'язати їх із параметрами. "
"Працює і для стандартних пензлів."

#~ msgid "Puts a threshold filter over the brush mask."
#~ msgstr "Накладає пороговий фільтр на маску пензля."
