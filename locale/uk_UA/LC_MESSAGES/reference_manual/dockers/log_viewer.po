# Translation of docs_krita_org_reference_manual___dockers___log_viewer.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___dockers___log_viewer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 08:29+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Configure Logging"
msgstr "Налаштовування ведення журналу"

#: ../../reference_manual/dockers/log_viewer.rst:1
msgid "Overview of the log viewer docker."
msgstr "Огляд бічної панелі перегляду журналу."

#: ../../reference_manual/dockers/log_viewer.rst:10
#: ../../reference_manual/dockers/log_viewer.rst:16
msgid "Log Viewer"
msgstr "Перегляд журналу"

#: ../../reference_manual/dockers/log_viewer.rst:10
msgid "Debug"
msgstr "Діагностика"

#: ../../reference_manual/dockers/log_viewer.rst:18
msgid ""
"The log viewer docker allows you to see debug output without access to a "
"terminal. This is useful when trying to get a tablet log or to figure out if "
"Krita is spitting out errors while a certain thing is happening."
msgstr ""
"За допомогою бічної панелі перегляду журналу ви можете переглядати "
"діагностичні повідомлення без потреби у доступі до термінала. Ця панель "
"стане у пригоді, якщо ви намагаєтеся отримати журнал роботи із планшетом або "
"визначити причину помилок у Krita під час виконання певних дій."

#: ../../reference_manual/dockers/log_viewer.rst:20
msgid ""
"The log docker is used by pressing the :guilabel:`enable logging` button at "
"the bottom."
msgstr ""
"Увімкнути бічну панель журналу можна за допомогою натискання кнопки :"
"guilabel:`Увімкнути журнал`, розташованої у нижній частині панелі."

#: ../../reference_manual/dockers/log_viewer.rst:24
msgid ""
"When enabling logging, this output will not show up in the terminal. If you "
"are missing debug output in the terminal, check that you didn't have the log "
"docker enabled."
msgstr ""
"Якщо увімкнено виведення журналу на панель, виведені повідомлення не буде "
"показано у терміналі. Якщо ви не бачите певних діагностичних повідомлень у "
"терміналі, перевірте, чи увімкнено виведення повідомлень на бічну панель "
"журналу."

#: ../../reference_manual/dockers/log_viewer.rst:26
msgid ""
"The docker is composed of a log area which shows the debug output, and four "
"buttons at the bottom."
msgstr ""
"Вміст бічної панелі складається із області журналу, де буде показано "
"діагностичні повідомлення, та чотирьох кнопок, розташованих у нижній частині "
"панелі."

#: ../../reference_manual/dockers/log_viewer.rst:29
msgid "Log Output Area"
msgstr "Область виведення даних журналу"

#: ../../reference_manual/dockers/log_viewer.rst:31
msgid "The log output is formatted as follows:"
msgstr "Виведені дані журналу форматовано таким чином:"

#: ../../reference_manual/dockers/log_viewer.rst:33
msgid "White"
msgstr "Білий"

#: ../../reference_manual/dockers/log_viewer.rst:34
msgid "This is just a regular debug message."
msgstr "Це звичайне діагностичне повідомлення."

#: ../../reference_manual/dockers/log_viewer.rst:35
msgid "Yellow"
msgstr "Жовтий"

#: ../../reference_manual/dockers/log_viewer.rst:36
msgid "This is a info output."
msgstr "Це інформаційне повідомлення."

#: ../../reference_manual/dockers/log_viewer.rst:37
msgid "Orange"
msgstr "Помаранчевий"

#: ../../reference_manual/dockers/log_viewer.rst:38
msgid "This is a warning output."
msgstr "Це попередження."

#: ../../reference_manual/dockers/log_viewer.rst:40
msgid "Red"
msgstr "Червоний"

#: ../../reference_manual/dockers/log_viewer.rst:40
msgid "This is a critical error. When this is bolded, it is a fatal error."
msgstr ""
"Це критична помилка. Якщо використано напівжирний шрифт, помилка завершилася "
"аварією у програмі."

#: ../../reference_manual/dockers/log_viewer.rst:43
msgid "Options"
msgstr "Налаштування"

#: ../../reference_manual/dockers/log_viewer.rst:45
msgid "There's four buttons at the bottom:"
msgstr "У нижній частині панелі розташовано чотири кнопки:"

#: ../../reference_manual/dockers/log_viewer.rst:47
msgid "Enable Logging"
msgstr "Увімкнути журнал"

#: ../../reference_manual/dockers/log_viewer.rst:48
msgid "Enable the docker to start logging. This caries over between sessions."
msgstr ""
"Увімкнути бічну панель для того, щоб розпочати запис журналу. Журнал може "
"зберігатися протягом декількох сеансів роботи з програмою."

#: ../../reference_manual/dockers/log_viewer.rst:49
msgid "Clear the Log"
msgstr "Спорожнити журнал"

#: ../../reference_manual/dockers/log_viewer.rst:50
msgid "This empties the log output area."
msgstr "Спорожняє область виведення повідомлено журналу."

#: ../../reference_manual/dockers/log_viewer.rst:51
msgid "Save the Log"
msgstr "Зберегти журнал"

#: ../../reference_manual/dockers/log_viewer.rst:52
msgid "Save the log to a text file."
msgstr "Зберегти журнал до текстового файла."

#: ../../reference_manual/dockers/log_viewer.rst:54
msgid ""
"Configure which kind of debug is added. By default only warnings and simple "
"debug statements are logged. You can enable the special debug messages for "
"each area here."
msgstr ""
"За допомогою цього параметра можна визначити тип діагностичних повідомлень. "
"Типово, до журналу записуватимуться лише попередження та прості діагностичні "
"повідомлення. Ви можете увімкнути показ спеціалізованих діагностичних "
"повідомлень для кожної області."

#: ../../reference_manual/dockers/log_viewer.rst:56
msgid "General"
msgstr "Загальне"

#: ../../reference_manual/dockers/log_viewer.rst:57
msgid "Resource Management"
msgstr "Керування ресурсами"

#: ../../reference_manual/dockers/log_viewer.rst:58
msgid "Image Core"
msgstr "Ядро обробки зображення"

#: ../../reference_manual/dockers/log_viewer.rst:59
msgid "Registries"
msgstr "Списки"

#: ../../reference_manual/dockers/log_viewer.rst:60
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/dockers/log_viewer.rst:61
msgid "Tile Engine"
msgstr "Рушій плиток"

#: ../../reference_manual/dockers/log_viewer.rst:62
msgid "Filters"
msgstr "Фільтри"

#: ../../reference_manual/dockers/log_viewer.rst:63
msgid "Plugin Management"
msgstr "Керування додатками"

#: ../../reference_manual/dockers/log_viewer.rst:64
msgid "User Interface"
msgstr "Інтерфейс користувача"

#: ../../reference_manual/dockers/log_viewer.rst:65
msgid "File Loading and Saving"
msgstr "Звантаження і збереження файлів"

#: ../../reference_manual/dockers/log_viewer.rst:66
msgid "Mathematics and Calculations"
msgstr "Математика і обчислення"

#: ../../reference_manual/dockers/log_viewer.rst:67
msgid "Image Rendering"
msgstr "Обробка зображення"

#: ../../reference_manual/dockers/log_viewer.rst:68
msgid "Scripting"
msgstr "Скрипти"

#: ../../reference_manual/dockers/log_viewer.rst:69
msgid "Input Handling"
msgstr "Обробка вхідних даних"

#: ../../reference_manual/dockers/log_viewer.rst:70
msgid "Actions"
msgstr "Дії"

#: ../../reference_manual/dockers/log_viewer.rst:71
msgid "Tablet Handing"
msgstr "Робота з планшетом"

#: ../../reference_manual/dockers/log_viewer.rst:72
msgid "GPU Canvas"
msgstr "Графічний процесор"

#: ../../reference_manual/dockers/log_viewer.rst:73
msgid "Metadata"
msgstr "Метадані"

#: ../../reference_manual/dockers/log_viewer.rst:74
msgid "Color Management"
msgstr "Керування кольорами"
