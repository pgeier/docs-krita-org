# Translation of docs_krita_org_tutorials___making_an_azalea_with_the_transformation_masks.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_tutorials___making_an_azalea_with_the_transformation_masks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-14 08:47+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../<rst_epilog>:46
msgid ""
".. image:: images/icons/transform_tool.svg\n"
"   :alt: tooltransform"
msgstr ""
".. image:: images/icons/transform_tool.svg\n"
"   :alt: tooltransform"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Krita-screencast-azaleas.png\n"
"   :alt: making azalea with transform masks"
msgstr ""
".. image:: images/making-azalea/Krita-screencast-azaleas.png\n"
"   :alt: Створюємо азалію за допомогою масок перетворення"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_01_trunk-.png\n"
"   :alt: starting with the trunk and reference image"
msgstr ""
".. image:: images/making-azalea/Azelea_01_trunk-.png\n"
"   :alt: Розпочинаємо зі стебла та еталонного зображення"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_02_drawing-flowers.png\n"
"   :alt: making the outline of the flowers"
msgstr ""
".. image:: images/making-azalea/Azelea_02_drawing-flowers.png\n"
"   :alt: Створення контуру квітів"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_03_filling-flowers.png\n"
"   :alt: coloring the details and filling the flowers"
msgstr ""
".. image:: images/making-azalea/Azelea_03_filling-flowers.png\n"
"   :alt: Розфарбовування деталей та заповнення квітів кольором"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_04_finished-setup.png\n"
"   :alt: finished setup for making azalea"
msgstr ""
".. image:: images/making-azalea/Azelea_04_finished-setup.png\n"
"   :alt: Завершення налаштовування для створення азалій"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_05_clonelayer.png\n"
"   :alt: create clone layers of the flowers"
msgstr ""
".. image:: images/making-azalea/Azelea_05_clonelayer.png\n"
"   :alt: Створення шарів клонування квітів"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_06_transformmask.png\n"
"   :alt: adding transform masks to the cloned layers"
msgstr ""
".. image:: images/making-azalea/Azelea_06_transformmask.png\n"
"   :alt: Додавання масок перетворення до клонованих шарів"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_07_clusters.png\n"
"   :alt: adding more clusters"
msgstr ""
".. image:: images/making-azalea/Azelea_07_clusters.png\n"
"   :alt: Додавання кластерів"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_08_leaves.png\n"
"   :alt: making leaves"
msgstr ""
".. image:: images/making-azalea/Azelea_08_leaves.png\n"
"   :alt: Створення листя"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_09_paintingoriginals.png\n"
"   :alt: painting originals"
msgstr ""
".. image:: images/making-azalea/Azelea_09_paintingoriginals.png\n"
"   :alt: Малювання оригіналів"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_10_alphainheritance_1.png\n"
"   :alt: using the alpha inheritance"
msgstr ""
".. image:: images/making-azalea/Azelea_10_alphainheritance_1.png\n"
"   :alt: Використання успадковування прозорості"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_11_alphainheritance_2.png\n"
"   :alt: clipping the cluster with alpha inheritance"
msgstr ""
".. image:: images/making-azalea/Azelea_11_alphainheritance_2.png\n"
"   :alt: Обрізання кластера із успадковуванням прозорості"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_12_alphainheritance_3.png\n"
"   :alt: activate alpha inheritance"
msgstr ""
".. image:: images/making-azalea/Azelea_12_alphainheritance_3.png\n"
"   :alt: Активація успадковування прозорості"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_13_alphainheritance_4.png\n"
"   :alt: multiplying the clipped shape"
msgstr ""
".. image:: images/making-azalea/Azelea_13_alphainheritance_4.png\n"
"   :alt: Множення обрізаної форми"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_14_alphainheritance_5.png\n"
"   :alt: remove extra areas with the eraser"
msgstr ""
".. image:: images/making-azalea/Azelea_14_alphainheritance_5.png\n"
"   :alt: Вилучення зайвих ділянок за допомогою гумки"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_15_alphainheritance_6.png\n"
"   :alt: add shadows and highlights with alpha inheritance technique"
msgstr ""
".. image:: images/making-azalea/Azelea_15_alphainheritance_6.png\n"
"   :alt: Додавання тіней і виблисків за допомогою методики успадковування "
"прозорості"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:1
msgid "Tutorial for making azalea with the help of transform masks"
msgstr "Підручник зі створення азалій за допомогою масок перетворення"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:13
msgid "Making An Azalea With The Transformation Masks"
msgstr "Створення азалій за допомогою масок перетворення"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:18
msgid "This page was ported from the original post on the main page"
msgstr "Цю сторінку було портовано з початкового допису на головній сторінці"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:20
msgid ""
"Okay, so I’ve wanted to do a tutorial for transform masks for a while now, "
"and this is sorta ending up to be a flower-drawing tutorial. Do note that "
"this tutorial requires you to use **Krita 2.9.4 at MINIMUM**. It has a "
"certain speed-up that allows you to work with transform masks reliably!"
msgstr ""
"Гаразд, автору давно хотілося створити підручник щодо масок перетворення. "
"Результатом зусиль щодо цього став цей підручник з малювання квіток. "
"Зауважте, що для виконання настанов з цього підручника вам знадобиться "
"**принаймні Krita 2.9.4**. У цій версії програми реалізовано певне "
"пришвидшення роботи, яке надасть вам змогу працювати із масками перетворення "
"у надійний спосіб!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:22
msgid ""
"I like drawing flowers because they are a bit of an unappreciated subject, "
"yet allow for a lot of practice in terms of rendering. Also, you can explore "
"cool tricks in Krita with them."
msgstr ""
"Автору подобається малювати квіти, оскільки вони є дещо недооціненим "
"об'єктом, хоча надають змогу попрактикуватися у сенсі обробки. Крім того, за "
"їх допомогою ви зможете оволодіти чудовими трюками у Krita."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:24
msgid ""
"Today’s flower is the Azalea flower. These flowers are usually pink to red "
"and appear in clusters, the clusters allow me to exercise with transform "
"masks!"
msgstr ""
"Сьогоднішньою квіткою будуть азалії. Ці квіти мають рожево-червоне "
"забарвлення і цвітуть суцвіттями. Суцвіття дозволять нам повправлятися із "
"масками перетворення!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:26
msgid ""
"I got an image from Wikipedia for reference, mostly because it’s public "
"domain, and as an artist I find it important to respect other artists. You "
"can copy it and, if you already have a canvas, :menuselection:`Edit --> "
"Paste into New Image` or :menuselection:`New --> Create from Clipboard`."
msgstr ""
"Еталонне зображення автор запозичив з Вікіпедії, оскільки його було надано у "
"загальне користування, а авто завжди поважав авторські права інших "
"художників. Ви можете скопіювати його і, якщо полотно вже створено, "
"скористатися пунктом меню :menuselection:`Зміни --> Вставити до нового "
"зображення` або :menuselection:`Створити --> Створити на основі вмісту "
"буфера обміну`."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:28
msgid ""
"Then, if you didn’t have a new canvas make one. I made an A5 300dpi canvas. "
"This is not very big, but we’re only practicing. I also have the background "
"color set to a yellow-grayish color (#CAC5B3), partly because it reminds me "
"of paper, and partly because bright screen white can strain the eyes and "
"make it difficult to focus on values and colors while painting. Also, due to "
"the lack of strain on the eyes, you’ll find yourself soothed a bit. Other "
"artists use #c0c0c0, or even more different values."
msgstr ""
"Далі, якщо полотно не відкрито, створіть нове. Автор створити полотно "
"розміру A5 із роздільністю 300 точок на дюйм. Таке полотно є не дуже "
"великим, але достатнім для того, щоб попрактикуватися. Також було вибрано "
"жовто-сірий колір тла (#CAC5B3), частково через те, що такий колір нагадує "
"колір паперу, а частково через те, що яскравий білий колір з екрана втомлює "
"очі і ускладнює для художника зосередження на кольорах під час малювання. "
"Крім того, без навантаження на очі ви почуватиметеся дещо розслабленіше. "
"Інші художники використовують #c0c0c0 або якийсь інший колір тла."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:30
msgid ""
"So, if you go to :menuselection:`Window --> Tile`, you will find that now "
"your reference image and your working canvas are side by side. The reason I "
"am using this instead of the docker is because I am lazy and don’t feel like "
"saving the wikipedia image. We’re not going to touch the image much."
msgstr ""
"Отже, якщо ви скористаєтеся пунктом меню :menuselection:`Вікно --> Мозаїка`, "
"еталонне зображення і ваше робоче полотно буде розташовано поруч. Причина "
"використання такого компонування замість бічної панелі полягає у тому, що "
"автор лінується і не хоче зберігати зображення з Вікіпедії. Ми не збираємося "
"надто його змінювати."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:33
msgid "Let’s get to drawing!"
msgstr "Переходимо до малювання!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:38
msgid ""
"First we make a bunch of branches. I picked a slightly darker color here "
"than usual, because I know that I’ll be painting over these branches with "
"the lighter colors later on. Look at the reference how branches are formed."
msgstr ""
"Спочатку створімо декілька гілок. Тут автор вибрав дещо темніший колір, ніж "
"звичайно, оскільки пізніше він малюватиме на цих гілках світлішими "
"кольорами. Погляньте на еталонне зображення, щоб зрозуміти, як формуються "
"гілки."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:43
msgid ""
"Then we make an approximation of a single flower on a layer. We make a few "
"of these, all on separate layers. We also do not color pick the red, but we "
"guess at it. This is good practice, so we can learn to analyze a color as "
"well as how to use our color selector. If we’d only pick colors, it would be "
"difficult to understand the relationship between them, so it’s best to "
"attempt matching them by eye."
msgstr ""
"Далі, намалюймо приблизно одну квітку на шарі. Нам знадобиться декілька "
"квіток, усі на окремих шарах. Також не будемо брати з еталонного зображення "
"червоний, але самі спробуємо вибрати відповідний колір. Це чудова вправа, "
"яка надасть нам змогу навчитися аналізувати колір, а також користуватися "
"засобом вибору кольорів. Якби ми просто взяли колір зображення, було б важко "
"зрозуміти зв'язок між кольорами, тому краще спробувати знайти "
"найвідповідніший колір, покладаючись на власні очі."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:48
msgid ""
"I chose to make the flower shape opaque quickly by using the *behind* "
"blending mode. This’ll mean Krita is painting the new pixels behind the old "
"ones. Very useful for quickly filling up shapes, just don’t forget to go "
"back to *normal* once you’re done."
msgstr ""
"Автор швидко створив форму квітки з використанням режиму змішування *Задній "
"план*. У цьому режимі Krita малює нові пікселі за наявними. Режим є дуже "
"корисним для швидкого заповнення форм. Не забудьте перемкнутися на "
"*Звичайний*, коли закінчите малювання."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:53
msgid ""
"Now, we’ll put the flowers in the upper left corner, and group them. You can "
"group by making a group layer, and selecting the flower layers in your "
"docker with the :kbd:`Ctrl +` |mouseleft| shortcut and dragging them into "
"the group. The reason why we’re putting them in the upper left corner is "
"because we’ll be selecting them a lot, and Krita allows you to select layers "
"with the :kbd:`R +` |mouseleft| shortcut on the canvas quickly. Just hold "
"the :kbd:`R` key and |mouseleft| the pixels belonging to the layer you want, "
"and Krita will select the layer in the Layer docker."
msgstr ""
"Тепер ми пересунемо квітки до верхнього лівого кута і згрупуємо їх. Ви "
"можете виконати групування створенням шару групування із наступним "
"позначенням пунктів шарів квіток на бічній панелі за допомогою комбінації :"
"kbd:`Ctrl` + |mouseleft| і перетягуванням їх до групи. Причина розташування "
"квіток у верхньому лівому куті полотна полягає у тому, що ми багато разів "
"позначатимемо їх, а Krita надає змогу швидко позначати шари на полотні за "
"допомогою комбінації kbd:`R` + |mouseleft|. Просто утримуйте натиснутою "
"клавішу :kbd:`R` і клацніть |mouseleft| на пікселях, які належать до "
"потрібного вам шару, і Krita позначить відповідний пункт на бічній панелі "
"списку шарів."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:56
msgid "Clone Layers"
msgstr "Клонування шарів"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:58
msgid ""
"Now, we will make clusters. What we’ll be doing is that we select a given "
"flower and then make a new clone layer. A clone layer is a layer that is "
"literally a clone of the original. They can’t be edited themselves, but edit "
"the original and the clone layer will follow suit. Clone Layers, and File "
"layers, are our greatest friends when it comes to transform masks, and "
"you’ll see why in a moment."
msgstr ""
"Тепер створімо суцвіття. Ми позначатимемо квітку і створюватимемо на її "
"основі шар клонування. Шар клонування — шар, який є повною копією "
"початкового. Такі шари не можна редагувати окремо, але будь-які зміни у "
"початковому шарі одразу відтворюватимуться у його клоні. Шари клонування та "
"файлові шари — найзручніші інструменти, якщо йдеться про маски перетворення. "
"Чому? Зараз зрозумієте."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:63
msgid ""
"You’ll quickly notice that our flowers are not good enough for a cluster: we "
"need far more angles on the profile for example. If only there was a way to "
"transform them… but we can’t do that with clone layers. Or can we?"
msgstr ""
"Доволі просто зауважити, що наші квіти не дуже пасують для створення "
"суцвіття: нам, наприклад, потрібні квітки, які повернуто на певні кути, а не "
"лише у профіль. От, якби був спосіб перетворити їх… але ми не можемо цього "
"зробити із шарами клонування. Чи можемо?"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:66
msgid "Enter Transform Masks!"
msgstr "Почнімо користуватися масками перетворення!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:68
msgid ""
"Transform Masks are a really powerful feature introduced in 2.9. They are in "
"fact so powerful, that when you first use them, you can’t even begin to "
"grasp where to use them."
msgstr ""
"Маски перетворення є насправді потужною можливістю, яку реалізовано у "
"програмі, починаючи з версії 2.9. Фактично, вони такі потужні, що коли ви "
"вперше ними скористаєтеся, ви лише почнете розуміти, де ними можна "
"скористатися."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:70
msgid ""
"Transform masks allow us to do a transform operation onto a layer, any given "
"layer, and have it be completely dynamic! This includes our clone layer "
"flowers!"
msgstr ""
"Маски перетворення надають нам змогу виконувати дію з перетворення над "
"шаром, будь-яким заданим шаром, і виконувати її повністю динамічно! Це "
"стосується навіть наших квітів з шару клонування!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:72
msgid "How to use them:"
msgstr "Як ними користуватися:"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:74
msgid ""
"|mouseright| the layer you want to do the transform on, and add a "
"**Transform mask.**"
msgstr ""
"Клацніть |mouseright| на пункті шару, над яким слід виконати перетворення, і "
"виберіть у контекстному меню пункт :guilabel:`Маска перетворення`."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:76
msgid ""
"A transform mask should now have been added. You can recognize them by the "
"little ‘scissor’ icon."
msgstr ""
"Тепер додано маску перетворення. Ви можете розпізнати її за маленькою "
"піктограмою ножиць."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:81
msgid ""
"Now, with the transform mask selected, select the |tooltransform|, and "
"rotate our clone layer. Apply the transform. You know you’re successful when "
"you can hide the transform mask, and the layer goes back to its original "
"state!"
msgstr ""
"Тепер, з вибраною маскою перетворення, виберіть |tooltransform| і поверніть "
"шар клонування. Застосуйте перетворення. Про успішність перетворення можна "
"судити за тим, що маску перетворення можна приховати, і шар повернеться до "
"початкового стану!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:83
msgid ""
"You can even go and edit your transform! Just activate the |tooltransform| "
"again while on a transform mask, and you will see the original transform so "
"you can edit it. If you go to a different transform operation however, you "
"will reset the transform completely, so watch out."
msgstr ""
"Ви навіть можете редагувати перетворення! Просто активуйте |tooltransform| "
"знову, спочатку позначивши маску перетворення, і ви побачите початкове "
"перетворення, яке зможете редагувати. Втім, якщо ви виберете іншу дію з "
"перетворення, попереднє перетворення буде скинуто. Майте це на увазі."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:88
msgid ""
"We’ll be only using affine transformations in this tutorial (which are the "
"regular and perspective transform), but this can also be done with warp, "
"cage and liquify, which’ll have a bit of a delay (3 seconds to be precise). "
"This is to prevent your computer from being over-occupied with these more "
"complex transforms, so you can keep on painting."
msgstr ""
"У цьому підручнику ми використовуватимемо лише афінні перетворення (звичайне "
"перетворення та перетворення перспективи), але ви можете використовувати "
"перетворення викривлення, перетворення за кліткою та розплавлення, для "
"застосування яких потрібна певна пауза (3 секунди, якщо бути точним). Таким "
"чином програма захищає ваш комп'ютер від перевантаження складними "
"перетвореннями і уможливлює неперервне малювання."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:90
msgid "We continue on making our clusters till we have a nice arrangement."
msgstr ""
"Ми продовжуємо створювати наші суцвіття, аж доки не досягнемо красивого "
"компонування."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:95
msgid "Now do the same thing for the leaves."
msgstr "Тепер зробіть те саме для листків."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:100
msgid ""
"Now, if you select the original paint layers and draw on them, you can see "
"that all clone masks are immediately updated!"
msgstr ""
"Тепер, якщо ви виберете початкові шари малювання і малюватимете на них, усі "
"маски у шарах клонування буде негайно оновлено!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:102
msgid ""
"Above you can see there’s been a new view added so we can focus on painting "
"the flower and at the same time see how it’ll look. You can make a new view "
"by going :menuselection:`Window --> New View` and selecting the name of your "
"current canvas (save first!). Views can be rotated and mirrored differently."
msgstr ""
"Вище ви можете бачити, що ми додали нову панель перегляду, щоб зосередитися "
"на малюванні квітки і одночасно бачити, як виглядає малюнок загалом. Нову "
"панель перегляду можна створити за допомогою пункту меню :menuselection:"
"`Вікно --> Нова панель перегляду`: просто виберіть назву поточного полотна "
"(перед цим збережіть результати роботи!). Панелі перегляду можна довільно "
"обертати і віддзеркалювати незалежно від початкового зображення."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:104
msgid ""
"Now continue painting the original flowers and leaves, and we’ll move over "
"to adding extra shadow to make it seem more lifelike!"
msgstr ""
"Тепер продовжимо малювання початкових квіток і листя. Ми перейдемо до "
"додавання додаткової тіні для надання реалістичності зображенню!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:109
msgid ""
"We’re now going to use *Alpha Inheritance*. Alpha inheritance is an ill-"
"understood concept, because a lot of programs use *clipping masks* instead, "
"which clip the layer’s alpha using only the alpha of the first next layer."
msgstr ""
"Тепер, скористаймося *успадкуванням прозорості*. Щоб розібратися із "
"успадкуванням прозорості, потрібен певний час, оскільки у багатьох інших "
"програма замість нього використовують *маски обрізання*, які обрізають "
"прозорість шару на основі лише прозорості першого наступного шару."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:111
msgid ""
"Alpha inheritance, however, uses all layers in a stack, so all the layers in "
"the group that haven’t got alpha inheritance active themselves, or all the "
"layers in the stack when the layer isn’t in a group. Because most people "
"have an opaque layer at the bottom of their layer stack, alpha inheritance "
"doesn’t seem to do much."
msgstr ""
"На відміну від інших програм, у Krita для успадкування прозорості "
"використовують усі шари у стосі, отже усі шари у групі, у яких не активовано "
"успадкування прозорості, або усі шари у стосі, якщо шар не належить до "
"групи. Оскільки у більшості художників нижній шар у стосі шарів є "
"непрозорим, успадкування прозорості не працює."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:113
msgid ""
"But for us, alpha inheritance is useful, because we can use all clone-layers "
"in a cluster (if you grouped them), transformed or not, for clipping. Just "
"draw a light blue square over all the flowers in a given cluster."
msgstr ""
"Але у нашому випадку успадкування прозорості працюватиме, оскільки ми можемо "
"скористатися групою усіх шарів-клонів у суцвітті (якщо ви згрупували їх), "
"перетворених і неперетворених, для обрізання. Достатньо намалювати світлий "
"синій прямокутник навколо усіх квіток у суцвітті."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:118
msgid ""
"Then press the last icon in the layer stack, the alpha-inherit button, to "
"activate alpha-inheritance."
msgstr ""
"Потім натиснемо останню піктограму у стосі шарів, кнопку успадкування "
"прозорості, для вмикання успадкування прозорості."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:123
msgid ""
"Set the layer to *multiply* then, so it’ll look like everything’s darker "
"blue."
msgstr ""
"Далі, встановіть режим змішування :guilabel:`Множення`, щоб тони синього "
"виглядали темнішими."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:128
msgid ""
"Then, with multiply and alpha inheritance on, use an eraser to remove the "
"areas where there should be no shadow."
msgstr ""
"Тепер, коли увімкнено режим множення та успадкування прозорості, "
"скористайтеся гумкою для вилучення ділянок, де не повинно бути тіней."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:133
msgid ""
"For the highlights use exactly the same method, AND exactly the same color, "
"but instead set the layer to Divide (you can find this amongst the "
"Arithmetic blending modes). Using Divide has exactly the opposite effect as "
"using multiply with the same color. The benefit of this is that you can "
"easily set up a complementary harmony in your shadows and highlights using "
"these two."
msgstr ""
"Для виблисків скористайтеся тим самим методом і тим самим кольором, але "
"встановіть для шару режим змішування «Ділення» (ви знайдете його серед "
"арифметичних режимі змішування у списку). Використання режиму «Ділення» має "
"протилежні наслідки, якщо порівнювати із режимом «Множення» для того самого "
"кольору. Користь з цього полягає у тому, що за допомогою цих двох режимів "
"змішування ви без проблем можете вибирати протилежні кольори для тіней і "
"виблисків на малюнку."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:138
msgid ""
"Do this with all clusters and leaves, and maybe on the whole plant (you will "
"first need to stick it into a group layer given the background is opaque) "
"and you’re done!"
msgstr ""
"Виконайте цю дію для усіх суцвіть та листочків і, можливо, для усієї гілки "
"(спершу її слід зібрати у груповий шар, оскільки тло малюнка є непрозорим), "
"і все!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:140
msgid ""
"Transform masks can be used on paint layers, vector layers, group layers, "
"clone layers and even file layers. I hope this tutorial has given you a nice "
"idea on how to use them, and hope to see much more use of the transform "
"masks in the future!"
msgstr ""
"Масками перетворення можна скористатися для шарів малювання, векторних "
"шарів, шарів групування, шарів-клонів і навіть файлових шарів. Сподіваємося, "
"цей розділ надав вам змогу зрозуміти, як ними користуватися, і ви зможете "
"зробити ще багато корисного за допомогою масок перетворення!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:142
msgid ""
"You can get the file I made `here <https://share.kde.org/public.php?"
"service=files&t=48c601aaf17271d7ca516c44cbe8590e>`_ to examine it further! "
"(Caution: It will freeze up Krita if your version is below 2.9.4. The speed-"
"ups in 2.9.4 are due to this file.)"
msgstr ""
"Ви можете отримати створений файл `тут <https://share.kde.org/public.php?"
"service=files&t=48c601aaf17271d7ca516c44cbe8590e>`_ для подальшого його "
"вивчення! (Попередження: якщо ви скористаєтеся Krita версії, яка є нижчою за "
"2.9.4 програма просто зависне. Пришвидшення роботи версії 2.9.4 є "
"результатом створення цього файла.)"
