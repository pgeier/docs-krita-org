msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../general_concepts/file_formats/file_gih.rst:1
msgid "The Gimp Image Hose file format in Krita."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "Image Hose"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "Gimp Image Hose"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "GIH"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "*.gih"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:15
msgid "\\*.gih"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:17
msgid ""
"The GIMP image hose format. Krita can open and save these, as well as import "
"via the :ref:`predefined brush tab <predefined_brush_tip>`."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:19
msgid ""
"Image Hose means that this file format allows you to store multiple images "
"and then set some options to make it specify how to output the multiple "
"images."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:25
msgid ".. image:: images/brushes/Gih-examples.png"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:25
msgid "From top to bottom: Incremental, Pressure and Random"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:27
msgid "Gimp image hose format options:"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:29
msgid "Constant"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:30
msgid "This'll use the first image, no matter what."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:31
msgid "Incremental"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:32
msgid ""
"This'll paint the image layers in sequence. This is good for images that can "
"be strung together to create a pattern."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:33
msgid "Pressure"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:34
msgid ""
"This'll paint the images depending on pressure. This is good for brushes "
"imitating the hairs of a natural brush."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:35
msgid "Random"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:36
msgid ""
"This'll draw the images randomly. This is good for image-collections used in "
"speedpainting as well as images that generate texture. Or perhaps more "
"graphical symbols."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:38
msgid "Angle"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:38
msgid "This'll use the dragging angle to determine with image to draw."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:40
msgid ""
"When exporting a Krita file as a ``.gih``, you will also get the option to "
"set the default spacing, the option to set the name (very important for "
"looking it up in the UI) and the ability to choose whether or not to "
"generate the mask from the colors."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:43
msgid "Use Color as Mask"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:43
msgid ""
"This'll turn the darkest values of the image as the ones that paint, and the "
"whitest as transparent. Untick this if you are using colored images for the "
"brush."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:45
msgid ""
"We have a :ref:`Krita Brush tip page <brush_tip_animated_brush>` on how to "
"create your own gih brush."
msgstr ""
