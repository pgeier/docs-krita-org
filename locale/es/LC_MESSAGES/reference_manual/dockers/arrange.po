# Spanish translations for docs_krita_org_reference_manual___dockers___arrange.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___dockers___arrange\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-07-14 13:46+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Grouping"
msgstr "Agrupación"

#: ../../reference_manual/dockers/arrange.rst:1
msgid "The arrange docker."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:14
msgid "Arrange"
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:16
msgid ""
"A docker for aligning and arranging vector shapes. When you have the :ref:"
"`shape_selection_tool` active, the following actions will appear on this "
"docker:"
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:19
msgid "Align all selected objects."
msgstr "Alinear todos los objetos seleccionados."

#: ../../reference_manual/dockers/arrange.rst:21
msgid "Align Left"
msgstr "Alinear a la izquierda"

#: ../../reference_manual/dockers/arrange.rst:22
msgid "Horizontally Center"
msgstr "Centrar horizontalmente"

#: ../../reference_manual/dockers/arrange.rst:23
msgid "Align Right"
msgstr "Alinear a la derecha"

#: ../../reference_manual/dockers/arrange.rst:24
msgid "Align Top"
msgstr "Alinear a la parte superior"

#: ../../reference_manual/dockers/arrange.rst:25
msgid "Vertically Center"
msgstr "Centrar verticalmente"

#: ../../reference_manual/dockers/arrange.rst:26
msgid "Align"
msgstr "Alinear"

#: ../../reference_manual/dockers/arrange.rst:26
msgid "Align Bottom"
msgstr "Alinear a la parte inferior"

#: ../../reference_manual/dockers/arrange.rst:29
msgid "Ensure that objects are distributed evenly."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:31
msgid "Distribute left edges equidistantly."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:32
msgid "Distribute centers equidistantly horizontally."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:33
msgid "Distribute right edges equidistantly."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:34
msgid "Distribute top edges equidistantly."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:35
msgid "Distribute centers equidistantly vertically."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:36
msgid "Distribute"
msgstr "Distribuir"

#: ../../reference_manual/dockers/arrange.rst:36
msgid "Distribute bottom edges equidistantly."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:39
msgid "Ensure the gaps between objects are equal."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:41
msgid "Make horizontal gaps between object equal."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:42
msgid "Spacing"
msgstr "Espaciado"

#: ../../reference_manual/dockers/arrange.rst:42
msgid "Make vertical gaps between object equal."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:45
msgid "Change the order of vector objects."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:47
msgid "Bring to front"
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:48
msgid "Raise"
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:49
msgid "Lower"
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:50
msgid "Order"
msgstr "Orden"

#: ../../reference_manual/dockers/arrange.rst:50
msgid "Bring to back"
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:53
msgid "Buttons to group and ungroup vector objects."
msgstr ""
