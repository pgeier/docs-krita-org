# Spanish translations for docs_krita_org_user_manual___introduction_from_other_software.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_user_manual___introduction_from_other_software\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 19:19+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../user_manual/introduction_from_other_software.rst:5
msgid "Introduction Coming From Other Software"
msgstr "Introducción si viene de otro software"

#: ../../user_manual/introduction_from_other_software.rst:7
msgid ""
"Krita is not the only digital painting application in the world. Because we "
"know our users might be approaching Krita with their experience from using "
"other software, we have made guides to illustrate differences."
msgstr ""
"Krita no es la única aplicación de dibujo digital del mundo. Como sabemos "
"que nuestros usuarios han podido acercarse a Krita con su experiencia en el "
"uso de otro software, hemos creado guías para ilustrar las diferencias."

#: ../../user_manual/introduction_from_other_software.rst:10
msgid "Contents:"
msgstr "Contenido:"
