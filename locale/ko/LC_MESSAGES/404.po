# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Shinjo Park <kde@peremen.name>, 2019.
# JungHee Lee <daemul72@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-21 01:15+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../404.rst:None
msgid ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Image of Kiki looking confused through books."
msgstr ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: 책을 찾다가 길을 잃은 Kiki의 그림."

#: ../../404.rst:5
msgid "File Not Found (404)"
msgstr "파일을 찾을 수 없음(404)"

#: ../../404.rst:10
msgid "This page does not exist."
msgstr "이 페이지가 존재하지 않습니다."

#: ../../404.rst:12
msgid "This might be because of the following:"
msgstr "다음과 같은 이유 때문일 수 있습니다:"

#: ../../404.rst:14
msgid ""
"We moved the manual from MediaWiki to Sphinx and with that came a lot of "
"reorganization."
msgstr ""
"사용자 설명서를 미디어위키에서 Sphinx 기반으로 이전하면서 많은 부분이 변경되"
"었습니다."

#: ../../404.rst:15
msgid "The page has been deprecated."
msgstr "페이지가 더 이상 사용되지 않습니다."

#: ../../404.rst:16
msgid "Or a simple typo in the url."
msgstr "URL에 간단한 오타가 있습니다."

#: ../../404.rst:18
msgid ""
"In all cases, you might be able to find the page you're looking for by using "
"the search in the lefthand navigation."
msgstr ""
"언제든지 왼쪽의 탐색 창에서 검색을 사용하여 원하는 페이지를 찾을 수 있습니다."
