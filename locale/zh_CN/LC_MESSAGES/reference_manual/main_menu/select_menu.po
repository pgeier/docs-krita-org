msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___main_menu___select_menu.pot\n"

#: ../../reference_manual/main_menu/select_menu.rst:1
msgid "The select menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:11
msgid "Selection"
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:16
msgid "Select Menu"
msgstr "选择菜单"

#: ../../reference_manual/main_menu/select_menu.rst:19
msgid "Select All"
msgstr "全部选择"

#: ../../reference_manual/main_menu/select_menu.rst:21
msgid "Selects the whole layer."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:22
msgid "Deselect"
msgstr "反选"

#: ../../reference_manual/main_menu/select_menu.rst:24
msgid "Deselects everything (except for active Selection Mask)."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:25
msgid "Reselect"
msgstr "重新选择"

#: ../../reference_manual/main_menu/select_menu.rst:27
msgid "Reselects the previously deselected selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:28
msgid "Invert Selection"
msgstr "反选"

#: ../../reference_manual/main_menu/select_menu.rst:30
msgid "Inverts the selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:31
msgid "Convert to Vector Selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:33
msgid ""
"This converts a raster selection to a vector selection. Any layers of "
"transparency there might have been are removed."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:34
msgid "Convert to Raster Selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:36
msgid "This converts a vector selection to a raster selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:37
msgid "Convert Shapes to Vector Selection"
msgstr "转换形状为矢量选区"

#: ../../reference_manual/main_menu/select_menu.rst:39
msgid "Convert vector shape to vector selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:40
msgid "Convert to shape"
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:42
msgid "Converts vector selection to vector shape."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:43
msgid "Display Selection"
msgstr "显示选区"

#: ../../reference_manual/main_menu/select_menu.rst:45
msgid "Display the selection. If turned off selections will be invisible."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:46
msgid "Show Global Selection Mask"
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:48
msgid ""
"Shows the global selection as a selection mask in the layers docker. This is "
"necessary to be able to select it for painting on."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:49
msgid "Scale"
msgstr "比例"

#: ../../reference_manual/main_menu/select_menu.rst:51
msgid "Scale the selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:52
msgid "Select from Color Range"
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:54
msgid "Select from a certain color range."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:55
msgid "Select Opaque"
msgstr "选择不透明度"

#: ../../reference_manual/main_menu/select_menu.rst:57
msgid ""
"Select all opaque (non-transparent) pixels in the current active layer. If "
"there's already a selection, this will add the new selection to the old one, "
"allowing you to select the opaque pixels of multiple layers into one "
"selection. Semi-transparent (or semi-opaque) pixels will be semi-selected."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:58
msgid "Feather Selection"
msgstr "羽化选区"

#: ../../reference_manual/main_menu/select_menu.rst:60
msgid ""
"Feathering in design means to soften sharp borders. So this adds a soft "
"border to the existing selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:61
msgid "Grow Selection"
msgstr "扩张选区"

#: ../../reference_manual/main_menu/select_menu.rst:63
msgid "Make the selection a few pixels bigger."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:64
msgid "Shrink Selection"
msgstr "收缩选区"

#: ../../reference_manual/main_menu/select_menu.rst:66
msgid "Make the selection a few pixels smaller."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:67
msgid "Border Selection"
msgstr "边界选区"

#: ../../reference_manual/main_menu/select_menu.rst:69
msgid ""
"Take the current selection and remove the insides so you only have a border "
"selected."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:70
msgid "Smooth"
msgstr "平滑"

#: ../../reference_manual/main_menu/select_menu.rst:72
msgid "Make the selection a little smoother. This removes jiggle."
msgstr ""
