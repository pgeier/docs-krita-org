msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-02-27 08:06+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""

#: ../../<rst_epilog>:48
msgid ""
".. image:: images/icons/grid_tool.svg\n"
"   :alt: toolgrid"
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:1
msgid "Krita's grid tool reference."
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:11
#, fuzzy
#| msgid "Grid Tool"
msgid "Grid"
msgstr "Grille"

#: ../../reference_manual/tools/grid_tool.rst:16
msgid "Grid Tool"
msgstr "Grille"

#: ../../reference_manual/tools/grid_tool.rst:18
msgid "|toolgrid|"
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:22
msgid "Deprecated in 3.0, use the :ref:`grids_and_guides_docker` instead."
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:24
msgid ""
"When you click on the edit grid tool, you'll get a message saying that to "
"activate the grid you must press the :kbd:`Enter` key. Press the :kbd:"
"`Enter` key to make the grid visible. Now you must have noticed the tool "
"icon for your pointer has changed to icon similar to move tool."
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:27
msgid ""
"To change the spacing of the grid, press and hold the :kbd:`Ctrl` key and "
"then the |mouseleft| :kbd:`+ drag` shortcut on the canvas. In order to move "
"the grid you have to press the :kbd:`Alt` key and then the |mouseleft| :kbd:`"
"+ drag` shortcut."
msgstr ""
