# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-12 14:18+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/dockers/task_sets.rst:1
msgid "Overview of the task sets docker."
msgstr "Overzicht van de vastzetter Sets met taken."

#: ../../reference_manual/dockers/task_sets.rst:16
msgid "Task Sets Docker"
msgstr "Vastzetter Sets met taken"

#: ../../reference_manual/dockers/task_sets.rst:18
msgid ""
"Task sets are for sharing a set of steps, like a tutorial. You make them "
"with the task-set docker."
msgstr ""
"Sets met taken zijn er voor het delen van een set stappen, zoals een "
"inleiding. U maakt deze met de vastzetter Sets-met-taken."

#: ../../reference_manual/dockers/task_sets.rst:21
msgid ".. image:: images/dockers/Task-set.png"
msgstr ".. image:: images/dockers/Task-set.png"

#: ../../reference_manual/dockers/task_sets.rst:22
msgid ""
"Task sets can record any kind of command also available via the shortcut "
"manager. It can not record strokes, like the macro recorder can. However, "
"you can play macros with the tasksets!"
msgstr ""
"Sets met taken kunnen elk soort commando opnemen ook beschikbaar via de "
"sneltoetsbeheerder. Het kan geen streken opnemen, zoals de macrorecorder "
"kan. U kunt macro's afspelen met de sets met taken!"

#: ../../reference_manual/dockers/task_sets.rst:24
msgid ""
"The tasksets docker has a record button, and you can use this to record a "
"certain workflow. Then use this to let items appear in the taskset list. "
"Afterwards, turn off the record. You can then click any action in the list "
"to make them happen. Press the 'Save' icon to name and save the taskset."
msgstr ""
"De vastzetter sets met taken heeft een knop opnemen en u kunt deze gebruiken "
"om een bepaalde workflow op te nemen. Daarna kunt u deze gebruiken om items "
"te laten verschijnen in de lijst met sets met taken. Schakel daarna het "
"opnemen uit. U kunt dan op elke actie in de lijst klikken om deze uit te "
"voeren. Druk op het pictogram 'opslaan' om de set met taken een naam te "
"geven en op te slaan."
