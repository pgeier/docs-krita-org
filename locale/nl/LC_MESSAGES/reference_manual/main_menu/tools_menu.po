# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-06 12:12+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/main_menu/tools_menu.rst:1
msgid "The tools menu in Krita."
msgstr "Het menu Hulpmiddelen in Krita."

#: ../../reference_manual/main_menu/tools_menu.rst:11
msgid "Macro"
msgstr "Macro"

#: ../../reference_manual/main_menu/tools_menu.rst:11
msgid "Scripts"
msgstr "Scripts"

#: ../../reference_manual/main_menu/tools_menu.rst:17
msgid "Tools Menu"
msgstr "Menu Hulpmiddelen"

#: ../../reference_manual/main_menu/tools_menu.rst:19
msgid "This contains three things."
msgstr "Dit bevat drie dingen."

#: ../../reference_manual/main_menu/tools_menu.rst:22
msgid "Scripting"
msgstr "Scripting"

#: ../../reference_manual/main_menu/tools_menu.rst:24
msgid ""
"When you have python scripting enabled and have scripts toggled, this is "
"where most scripts are stored by default."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:27
msgid "Recording"
msgstr "Opnemen"

#: ../../reference_manual/main_menu/tools_menu.rst:31
msgid "The recording and macro features are unmaintained and buggy."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:33
msgid ""
"Record a macro. You do this by pressing start, drawing something and then "
"pressing stop. This feature can only record brush strokes. The resulting "
"file is stored as a \\*.kritarec file."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:36
msgid "Macros"
msgstr "Macro's"

#: ../../reference_manual/main_menu/tools_menu.rst:38
msgid ""
"Play back or edit a krita rec file. The edit can only change the brush "
"preset on strokes or add and remove filters."
msgstr ""
