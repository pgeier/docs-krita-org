# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-07-02 12:48+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Tangential Pressure"
msgstr "Tangentiële druk"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:1
msgid "Tablet sensors in Krita."
msgstr "Tabletsensors in Krita."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:11
msgid "Tablets"
msgstr "Tablets"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:16
msgid "Sensors"
msgstr "Sensoren"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:18
msgid "Pressure"
msgstr "Druk"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:19
msgid "Uses the pressure in and out values of your stylus."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:20
msgid "PressureIn"
msgstr "Ingaande druk"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:21
msgid ""
"Uses only pressure in values of your stylus. Previous pressure level in same "
"stroke is overwritten *only* by applying more pressure. Lessening the "
"pressure doesn't affect PressureIn."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:22
msgid "X-tilt"
msgstr "X-tilt"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:23
#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:25
msgid "How much the brush is affected by stylus angle, if supported."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:24
msgid "Y-tilt"
msgstr "Y-tilt"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:26
msgid "Tilt-direction"
msgstr "Tilt-richting"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:27
msgid ""
"How much the brush is affected by stylus direction. The pen point pointing "
"towards the user is 0°, and can vary from -180° to +180°."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:28
msgid "Tilt-elevation"
msgstr "Tilt-verhoging"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:29
msgid ""
"How much the brush is affected by stylus perpendicularity. 0° is the stylus "
"horizontal, 90° is the stylus vertical."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:30
msgid "Speed"
msgstr "Snelheid"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:31
msgid "How much the brush is affected by the speed at which you draw."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:32
msgid "Drawing Angle"
msgstr "Tekenhoek"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:33
msgid ""
"How much the brush is affected by which direction you are drawing in. :"
"guilabel:`Lock` will lock the angle to the one you started the stroke with. :"
"guilabel:`Fan corners` will try to smoothly round the corners, with the "
"angle being the angles threshold it'll round. :guilabel:`Angle offset` will "
"add an extra offset to the current angle."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:34
msgid "Rotation"
msgstr "Rotatie"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:35
msgid ""
"How much a brush is affected by how the stylus is rotated, if supported by "
"the tablet."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:36
msgid "Distance"
msgstr "Afstand"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:37
msgid "How much the brush is affected over length in pixels."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:38
msgid "Time"
msgstr "Tijd"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:39
msgid "How much a brush is affected over drawing time in seconds."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:40
msgid "Fuzzy (Dab)"
msgstr "Vage (dip)"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:41
msgid "Basically the random option."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:42
msgid "Fuzzy Stroke"
msgstr "Vage streek"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:43
msgid ""
"A randomness value that is per stroke. Useful for getting color and size "
"variation in on speed-paint brushes."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:44
msgid "Fade"
msgstr "Vervagen"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:45
msgid ""
"How much the brush is affected over length, proportional to the brush size."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:46
msgid "Perspective"
msgstr "Perspectief"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:47
msgid "How much the brush is affected by the perspective assistant."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:49
msgid ""
"How much the brush is affected by the wheel on airbrush-simulating styli."
msgstr ""
