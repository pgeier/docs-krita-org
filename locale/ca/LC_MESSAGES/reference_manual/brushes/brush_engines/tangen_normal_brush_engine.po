# Translation of docs_krita_org_reference_manual___brushes___brush_engines___tangen_normal_brush_engine.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-18 14:38+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_1.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_1.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_2.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_2.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_3.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_3.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutoria_4.png"
msgstr ".. image:: images/brushes/Krita-normals-tutoria_4.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:1
msgid "The Tangent Normal Brush Engine manual page."
msgstr ""
"La pàgina del manual per al motor del pinzell de mapa normal de tangent."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:16
msgid "Tangent Normal Brush Engine"
msgstr "Motor del pinzell de mapa normal de tangent"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Motor de pinzell"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Normal Map"
msgstr "Mapa normal"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:20
msgid ".. image:: images/icons/tangentnormal.svg"
msgstr ".. image:: images/icons/tangentnormal.svg"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:21
msgid ""
"The Tangent Normal Brush Engine is an engine that is specifically designed "
"for drawing normal maps, of the tangent variety. These are in turn used in "
"3d programs and game engines to do all sorts of lightning trickery. Common "
"uses of normal maps include faking detail where there is none, and to drive "
"transformations (Flow Maps)."
msgstr ""
"El Motor del pinzell de mapa normal de tangent és un motor dissenyat "
"específicament per a dibuixar mapes normals, de la varietat tangent. Aquests "
"s'utilitzen al seu torn en programes 3D i motors de jocs per a fer tot tipus "
"de trucs de llamps. Els usos comuns dels mapes normals inclouen detalls "
"falsos on no n'hi ha cap, i per a impulsar transformacions (mapes de flux)."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:23
msgid ""
"A Normal map is an image that holds information for vectors. In particular, "
"they hold information for Normal Vectors, which is the information for how "
"the light bends on a surface. Because Normal Vectors are made up of 3 "
"coordinates, just like colors, we can store and see this information as "
"colors."
msgstr ""
"Un mapa normal és una imatge que conté informació per als vectors. En "
"particular, conté informació per a vectors normals, aquesta és la informació "
"sobre com es corbarà la llum sobre una superfície. A causa que els vectors "
"normals estan formats per 3 coordenades, de la mateixa manera que els "
"colors, podrem emmagatzemar i veure aquesta informació com a colors."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:25
msgid ""
"Normals can be seen similar to the stylus on your tablet. Therefore, we can "
"use the tilt-sensors that are available to some tablets to generate the "
"color of the normals, which can then be used by a 3d program to do lighting "
"effects."
msgstr ""
"Els normals es poden veure de manera similar al llapis sobre la vostra "
"tauleta. Per tant, podem utilitzar els sensors d'inclinació que estan "
"disponibles en algunes tauletes per a generar el color dels normals, els "
"quals després es podran utilitzar en un programa 3D per a fer efectes "
"d'il·luminació."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:27
msgid "In short, you will be able to paint with surfaces instead of colors."
msgstr "En resum, podreu pintar amb superfícies en lloc de colors."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:29
msgid "The following options are available to the tangent normal brush engine:"
msgstr ""
"Hi ha disponibles les següents opcions per al motor del pinzell de mapa "
"normal de tangent:"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:31
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:32
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:33
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:34
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:35
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:36
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:37
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:38
msgid ":ref:`option_softness`"
msgstr ":ref:`option_softness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:39
msgid ":ref:`option_sharpness`"
msgstr ":ref:`option_sharpness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:40
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:41
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:42
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:43
msgid ":ref:`option_texture`"
msgstr ":ref:`option_texture`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:46
msgid "Specific Parameters to the Tangent Normal Brush Engine"
msgstr "Paràmetres específics al motor del pinzell de mapa normal de tangent"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:50
msgid "Tangent Tilt"
msgstr "Tangent inclinada"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:52
msgid ""
"These are the options that determine how the normals are calculated from "
"tablet input."
msgstr ""
"Aquestes són les opcions que determinen com es calculen les normals a partir "
"de l'entrada de la tauleta."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:54
msgid "Tangent Encoding"
msgstr "Codificació de tangent"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:55
msgid ""
"This allows you to set what each color channel means. Different programs set "
"different coordinates to different channels, a common version is that the "
"green channel might need to be inverted (-Y), or that the green channel is "
"actually storing the x-value (+X)."
msgstr ""
"Permetrà establir el que vol dir cada canal de color. Diferents programes "
"estableixen diferents coordenades per a diferents canals, una versió "
"habitual és que el canal verd pot necessitar ser invertit (-Y), o que el "
"canal verd realment emmagatzema el valor de «X» (+X)."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:56
msgid "Tilt Options"
msgstr "Opcions d'inclinació"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:57
msgid "Allows you to choose which sensor is used for the X and Y."
msgstr "Permetrà escollir quins sensor utilitzarà per a la «X» i la «Y»."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:58
msgid "Tilt"
msgstr "Inclinació"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:59
msgid "Uses Tilt for the X and Y."
msgstr "Utilitza la inclinació per a la «X» i la «Y»."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:60
msgid "Direction"
msgstr "Direcció"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:61
msgid ""
"Uses the drawing angle for the X and Y and Tilt-elevation for the Z, this "
"allows you to draw flowmaps easily."
msgstr ""
"Utilitza l'angle de dibuix per a la «X» i la «Y», i l'elevació de la "
"inclinació per a la «Z», permetrà dibuixar mapes de flux amb facilitat."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:62
msgid "Rotation"
msgstr "Gir"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:63
msgid ""
"Uses rotation for the X and Y, and tilt-elevation for the Z. Only available "
"for specialized Pens."
msgstr ""
"Utilitza el gir per a la «X» i la «Y», i l'elevació de la inclinació per a "
"la «Z». Només disponible per a llapis especialitzats."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid "Elevation Sensitivity"
msgstr "Sensibilitat de l'elevació"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid ""
"Allows you to change the range of the normal that are outputted. At 0 it "
"will only paint the default normal, at 1 it will paint all the normals in a "
"full hemisphere."
msgstr ""
"Permetrà canviar l'interval del normal que serà enviat. A 0, només pintareu "
"el normal de manera predeterminada, a 1 es pintaran tots els normals de tot "
"un hemisferi."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:68
msgid "Usage"
msgstr "Ús"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:70
msgid ""
"The Tangent Normal Map Brush Engine is best used with the Tilt Cursor, which "
"can be set in :menuselection:`Settings --> Configure Krita --> General --> "
"Outline Shape --> Tilt Outline`."
msgstr ""
"El motor del pinzell de mapa normal de tangent s'utilitza millor amb el "
"Contorn d'inclinació, el qual es pot establir a :menuselection:`Arranjament "
"--> Configura el Krita --> General --> Forma del contorn --> Contorn "
"d'inclinació`."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:73
msgid "Normal Map authoring workflow"
msgstr "Flux de treball en la creació d'un mapa normal"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:75
msgid "Create an image with a background color of (128, 128, 255) blue/purple."
msgstr "Creeu una imatge amb un color de fons de (128, 128, 255) blau/porpra."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:80
msgid "Setting up a background with the default color."
msgstr "Establiu un fons amb el color predeterminat."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:82
msgid ""
"Set up group with a :guilabel:`Phong Bumpmap` filter mask. Use the :guilabel:"
"`Use Normal map` checkbox on the filter to make it use normals."
msgstr ""
"Establiu el grup amb una màscara de filtratge :guilabel:`Mapa de relleu "
"Phong`. Utilitzeu la casella de selecció :guilabel:`Usa el mapa normal` "
"sobre el filtre per a fer que empri el normal."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:87
msgid ""
"Creating a phong bump map filter layer, make sure to check 'Use Normal map'."
msgstr ""
"En crear una capa de filtratge amb el mapa de relleu Phong, assegureu-vos de "
"marcar «Usa el mapa normal»."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:92
msgid ""
"These settings give a nice daylight-esque lighting setup, with light 1 being "
"the sun, light 3 being the light from the sky, and light 2 being the light "
"from the ground."
msgstr ""
"Aquests ajustaments donen una bona configuració de la il·luminació amb llum "
"diürna, sent la llum 1 el Sol, sent la llum 3 la llum des del cel i sent la "
"llum 2 la llum des del terra."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:94
msgid ""
"Make a :guilabel:`Normalize` filter layer or mask to normalize the normal "
"map before feeding it into the Phong bumpmap filter for the best results."
msgstr ""
"Creeu una capa o màscara de filtratge :guilabel:`Normalitza` per a "
"normalitzar el mapa normal abans d'introduir-lo al filtre mapa de relleu de "
"Phong per obtenir els millors resultats."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:95
msgid "Then, paint on layers in the group to get direct feedback."
msgstr ""
"Després, pinteu sobre les capes en el grup per obtenir retroalimentació "
"directa."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:100
msgid ""
"Paint on the layer beneath the filters with the tangent normal brush to have "
"them be converted in real time."
msgstr ""
"Pinteu sobre la capa que està sota els filtres amb el pinzell de mapa normal "
"de tangent per a convertir-la en temps real."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:102
msgid ""
"Finally, when done, hide the Phong bumpmap filter layer (but keep the "
"Normalize filter layer!), and export the normal map for use in 3d programs."
msgstr ""
"Finalment, una vegada hagueu acabat, oculteu la capa de filtratge mapa de "
"relleu de Phong (però manteniu la capa de filtratge Normalitza!), i exporteu "
"el mapa normal per a emprar-lo en programes 3D."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:105
msgid "Drawing Direction Maps"
msgstr "Dibuixar mapes de direcció"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:107
msgid ""
"Direction maps are made with the :guilabel:`Direction` option in the :"
"guilabel:`Tangent Tilt` options. These normal maps are used to distort "
"textures in a 3d program (to simulate for example, the flow of water) or to "
"create maps that indicate how hair and brushed metal is brushed. Krita can't "
"currently give feedback on how a given direction map will influence a "
"distortion or shader, but these maps are a little easier to read."
msgstr ""
"Els mapes de direcció es creen amb l'opció :guilabel:`Direcció` en les "
"opcions de :guilabel:`Tangent inclinada`. Aquests mapes normals s'empren per "
"a distorsionar les textures en un programa 3D (per exemple, per a simular el "
"flux de l'aigua) o per a crear mapes que indiquin com es pinzella el pèl i "
"el metall. Actualment, el Krita no pot donar retroalimentació sobre com un "
"mapa de direcció determinat influirà en una distorsió o ombrejat, però "
"aquests mapes són una mica més fàcils de llegir."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:109
msgid ""
"Just set the :guilabel:`Tangent Tilt` option to :guilabel:`Direction`, and "
"draw. The direction your brush draws in will be the direction that is "
"encoded in the colors."
msgstr ""
"Simplement establiu l'opció :guilabel:`Tangent inclinada` a la :guilabel:"
"`Direcció` i dibuixeu. La direcció en què dibuixa el pinzell serà la "
"direcció que està codificada en els colors."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:112
msgid "Only editing a single channel"
msgstr "Editar només un sol canal"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:114
msgid ""
"Sometimes you only want to edit a single channel. In that case set the "
"blending mode of the brush to :guilabel:`Copy <channel>`, with <channel> "
"replaced with red, green or blue. These are under the :guilabel:`Misc` "
"section of the blending modes."
msgstr ""
"De vegades només voldreu editar un sol canal. En aquest casos, establiu el "
"mode de barreja del pinzell a :guilabel:`Copia <channel>`, amb el <canal> "
"substituït amb vermell, verd o blau. Aquests estan sota la secció :guilabel:"
"`Miscel·lània` dels modes de barreja."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:116
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to :guilabel:`Copy Red`."
msgstr ""
"Per tant, si voleu que el pinzell només afecti el canal vermell, establiu el "
"mode de barreja a :guilabel:`Copia el vermell`."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid ".. image:: images/brushes/Krita_Filter_layer_invert_greenchannel.png"
msgstr ".. image:: images/brushes/Krita_Filter_layer_invert_greenchannel.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr ""
"Els modes de barreja copia el vermell, verd i blau també funcionen sobre les "
"capes de filtratge."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:123
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with :guilabel:`Copy "
"Green` above it."
msgstr ""
"Això també es pot fer amb les capes de filtratge. Per tant, si voleu canviar "
"ràpidament el canal verd d'una capa, creeu una capa de filtratge invers amb "
"un :guilabel:`Copia el verd` a sobre."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:126
msgid "Mixing Normal Maps"
msgstr "Mesclar mapes normals"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:128
msgid ""
"For mixing two normal maps, Krita has the :guilabel:`Combine Normal Map` "
"blending mode under :guilabel:`Misc`."
msgstr ""
"Per a mesclar dos mapes normals, el Krita té el mode de barreja :guilabel:"
"`Combina mapa normal` sota :guilabel:`Miscel·lània`."
