# Translation of docs_krita_org_reference_manual___image_split.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 21:02+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Autosave on split"
msgstr "Desa automàticament quan es divideix"

#: ../../reference_manual/image_split.rst:1
msgid "The Image Split functionality in Krita"
msgstr "La funcionalitat per a dividir la imatge al Krita"

#: ../../reference_manual/image_split.rst:10
msgid "Splitting"
msgstr "Dividir"

#: ../../reference_manual/image_split.rst:15
msgid "Image Split"
msgstr "Dividir la imatge"

#: ../../reference_manual/image_split.rst:17
msgid ""
"Found under :menuselection:`Image --> Image Split`, the Image Split function "
"allows you to evenly split a document up into several sections. This is "
"useful for splitting up spritesheets for example."
msgstr ""
"Es troba sota :menuselection:`Imatge --> Divideix la imatge`, aquesta funció "
"permet dividir un document de manera uniforme en diverses seccions. Això és "
"útil, per exemple, per a dividir en fulls de franges."

#: ../../reference_manual/image_split.rst:19
msgid "Horizontal Lines"
msgstr "Línies horitzontals"

#: ../../reference_manual/image_split.rst:20
msgid ""
"The amount of horizontal lines to split at. 4 lines will mean that the image "
"is split into 5 horizontal stripes."
msgstr ""
"La quantitat de línies horitzontals per a dividir. 4 línies voldrà dir que "
"la imatge es dividirà en 5 franges horitzontals."

#: ../../reference_manual/image_split.rst:22
msgid "Vertical Lines"
msgstr "Línies verticals"

#: ../../reference_manual/image_split.rst:22
msgid ""
"The amount of vertical lines to split at. 4 lines will mean that the image "
"is split into 5 vertical stripes."
msgstr ""
"La quantitat de línies verticals per a dividir. 4 línies voldrà dir que la "
"imatge es dividirà en 5 franges verticals."

#: ../../reference_manual/image_split.rst:24
msgid "Sort Direction"
msgstr "Direcció de l'ordenació"

#: ../../reference_manual/image_split.rst:28
msgid "Whether to number the files using the following directions:"
msgstr ""
"Quan s'hauran d'enumerar els fitxers mitjançant les següents direccions:"

#: ../../reference_manual/image_split.rst:30
msgid "Horizontal"
msgstr "Horitzontal"

#: ../../reference_manual/image_split.rst:31
msgid "Left to right, top to bottom."
msgstr "D'esquerra a dreta, de dalt a baix."

#: ../../reference_manual/image_split.rst:33
msgid "Vertical"
msgstr "Vertical"

#: ../../reference_manual/image_split.rst:33
msgid "Top to bottom, left to right."
msgstr "De dalt a baix, d'esquerra a dreta."

#: ../../reference_manual/image_split.rst:35
msgid "Prefix"
msgstr "Prefix"

#: ../../reference_manual/image_split.rst:36
msgid ""
"The prefix at which the files should be saved at. By default this is the "
"current document name."
msgstr ""
"El prefix en el qual s'hauran de desar els fitxers. De manera predeterminada "
"aquest serà el nom del document actual."

#: ../../reference_manual/image_split.rst:37
msgid "File Type"
msgstr "Tipus de fitxer"

#: ../../reference_manual/image_split.rst:38
msgid "Which file format to save to."
msgstr "En quin format de fitxer s'haurà de desar."

#: ../../reference_manual/image_split.rst:40
msgid ""
"This will result in all slices being saved automatically using the above "
"prefix. Otherwise Krita will ask the name for each slice."
msgstr ""
"Això resultarà en que totes les divisions es desaran automàticament "
"utilitzant el prefix anterior. Altrament, el Krita us demanarà el nom per a "
"cada divisió."
