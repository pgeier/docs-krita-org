# Translation of docs_krita_org_reference_manual___filters___wavelet_decompose.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-02 18:01+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/filters/wavelet_decompose.rst:None
msgid ".. image:: images/filters/Wavelet_decompose.png"
msgstr ".. image:: images/filters/Wavelet_decompose.png"

#: ../../reference_manual/filters/wavelet_decompose.rst:1
msgid "Overview of the wavelet decompose in Krita."
msgstr "Vista general de descompondre per ondetes en el Krita."

#: ../../reference_manual/filters/wavelet_decompose.rst:11
#: ../../reference_manual/filters/wavelet_decompose.rst:16
msgid "Wavelet Decompose"
msgstr "Descompondre per ondetes"

#: ../../reference_manual/filters/wavelet_decompose.rst:18
msgid ""
"Wavelet decompose uses wavelet scales to turn the current layer into a set "
"of layers with each holding a different type of pattern that is visible "
"within the image. This is used in texture and pattern making to remove "
"unwanted noise quickly from a texture."
msgstr ""
"La descomposició per ondetes utilitza escales per ondetes per a convertir la "
"capa actual a un conjunt de capes, cadascuna amb un tipus diferent de patró "
"que serà visible dins de la imatge. Això s'utilitza en la creació de patrons "
"i textures per eliminar ràpidament el soroll no desitjat d'una textura."

#: ../../reference_manual/filters/wavelet_decompose.rst:20
msgid "You can find it under :menuselection:`Layers`."
msgstr "També les trobareu sota :menuselection:`Capes`."

#: ../../reference_manual/filters/wavelet_decompose.rst:22
msgid ""
"When you select it, it will ask for the amount of wavelet scales. More "
"scales, more different layers. Press :guilabel:`OK`, and it will generate a "
"group layer containing the layers with their proper blending modes:"
msgstr ""
"Quan el seleccioneu, us demanarà la quantitat d'escales per ondetes. A més "
"escales, més capes diferents. Premeu :guilabel:`D'acord` i es generarà una "
"capa de grup que contindrà les capes amb els seus modes de barreja adequats:"

#: ../../reference_manual/filters/wavelet_decompose.rst:27
msgid ""
"Adjust a given layer with middle gray to neutralize it, and merge everything "
"with the :guilabel:`Grain Merge` blending mode to merge it into the end "
"image properly."
msgstr ""
"Ajusteu una capa indicada amb gris mig per a neutralitzar-la, i fusioneu-ho "
"tot amb el mode de barreja :guilabel:`Fusió de gra` per a fusionar "
"correctament dins de la imatge final."
