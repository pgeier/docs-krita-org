# Translation of docs_krita_org_reference_manual___tools___shape_edit.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 16:50+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Close Ellipse"
msgstr "Tanca l'el·lipse"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:12
msgid ""
".. image:: images/icons/shape_edit_tool.svg\n"
"   :alt: toolshapeedit"
msgstr ""
".. image:: images/icons/shape_edit_tool.svg\n"
"   :alt: eina d'edició de la forma"

#: ../../<rst_epilog>:24
msgid ""
".. image:: images/icons/line_tool.svg\n"
"   :alt: toolline"
msgstr ""
".. image:: images/icons/line_tool.svg\n"
"   :alt: eina de línia"

#: ../../<rst_epilog>:26
msgid ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: toolrectangle"
msgstr ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: eina de rectangle"

#: ../../<rst_epilog>:28
msgid ""
".. image:: images/icons/ellipse_tool.svg\n"
"   :alt: toolellipse"
msgstr ""
".. image:: images/icons/ellipse_tool.svg\n"
"   :alt: eina d'el·lipse"

#: ../../<rst_epilog>:30
msgid ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: toolpolygon"
msgstr ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: eina de polígon"

#: ../../<rst_epilog>:32
msgid ""
".. image:: images/icons/polyline_tool.svg\n"
"   :alt: toolpolyline"
msgstr ""
".. image:: images/icons/polyline_tool.svg\n"
"   :alt: eina de polilínia"

#: ../../<rst_epilog>:34
msgid ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: toolbeziercurve"
msgstr ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: eina de corbes de Bézier"

#: ../../<rst_epilog>:36
msgid ""
".. image:: images/icons/freehand_path_tool.svg\n"
"   :alt: toolfreehandpath"
msgstr ""
".. image:: images/icons/freehand_path_tool.svg\n"
"   :alt: eina de camí a mà alçada"

#: ../../reference_manual/tools/shape_edit.rst:1
msgid "Krita's shape edit tool reference."
msgstr "Referència de l'eina Edita la forma del Krita."

#: ../../reference_manual/tools/shape_edit.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/shape_edit.rst:11
msgid "Vector"
msgstr "Vectors"

#: ../../reference_manual/tools/shape_edit.rst:11
msgid "Shape Edit"
msgstr "Edita la forma"

#: ../../reference_manual/tools/shape_edit.rst:16
msgid "Shape Edit Tool"
msgstr "Eina per editar la forma"

#: ../../reference_manual/tools/shape_edit.rst:18
msgid "|toolshapeedit|"
msgstr "|toolshapeedit|"

#: ../../reference_manual/tools/shape_edit.rst:20
msgid ""
"The shape editing tool is for editing vector shapes. In Krita versions "
"before 4.0 it would only show up in the docker when you had a vector shape "
"selected. In Krita 4.0, this tool is always visible and has the Shape "
"Properties docker as a part of it."
msgstr ""
"L'eina per editar les formes és per editar les formes vectorials. En les "
"versions del Krita anteriors a la 4.0, només apareixeria a l'acoblador quan "
"se seleccionava una forma vectorial. En el Krita 4.0, aquesta eina sempre "
"estarà visible i tindrà l'acoblador Propietats de la forma com a part seva."

#: ../../reference_manual/tools/shape_edit.rst:23
msgid ".. image:: images/tools/Shape-editing-tool-example.png"
msgstr ".. image:: images/tools/Shape-editing-tool-example.png"

#: ../../reference_manual/tools/shape_edit.rst:24
msgid ""
"You can access the Edit Shapes tool by clicking on the icon in the toolbox, "
"but you can also access it by pressing the :kbd:`Enter` key when in the "
"Shape Selection tool and having a shape selected that can be most "
"efficiently edited with the edit shapes tool (right now, that's all shapes "
"but text)."
msgstr ""
"Podeu accedir a l'eina Edita les formes fent clic sobre la icona al quadre "
"d'eines, però també podreu accedir-hi prement la tecla :kbd:`Retorn` quan "
"esteu a l'eina Selecció amb formes i tenint seleccionada una forma que es "
"podrà editar de la manera més eficient amb l'eina Edita les formes (ara "
"mateix, això és totes les formes menys el text)."

#: ../../reference_manual/tools/shape_edit.rst:27
msgid "On Canvas Editing of Shapes"
msgstr "Editar les formes sobre el llenç"

#: ../../reference_manual/tools/shape_edit.rst:29
msgid ""
"As detailed further in the Tool Options, there's a difference between path "
"shapes and specialized vector shapes that make it easy to have perfect "
"ellipses, rectangles and more."
msgstr ""
"Com es detalla a les Opcions de l'eina, hi ha una diferència entre les "
"formes de camins i les formes vectorials especialitzades que fan que sigui "
"fàcil tenir el·lipsis perfectes, rectangles i més."

#: ../../reference_manual/tools/shape_edit.rst:32
#: ../../reference_manual/tools/shape_edit.rst:91
msgid "Path Shapes"
msgstr "Formes de camins"

#: ../../reference_manual/tools/shape_edit.rst:34
msgid "Path shapes can be recognized by the different nodes they have."
msgstr ""
"Les formes de camins es poden reconèixer pels diferents nodes que tenen."

# skip-rule: t-acc_obe
#: ../../reference_manual/tools/shape_edit.rst:36
msgid ""
"Paths in Krita are mostly bezier curves, and are made up of nodes. For "
"straight lines, the nodes are connected by a line-segment and that's it. For "
"curved lines, each node has a side handle to allow curving of that segment "
"using the `cubic bezier curve algorithm <https://en.wikipedia.org/wiki/B"
"%C3%A9zier_curve#/media/File:B%C3%A9zier_3_big.gif>`_."
msgstr ""
"Els camins al Krita majoritàriament són corbes de Bézier, i estan formades "
"per nodes. Per a les línies rectes, els nodes estaran connectats per un "
"segment de línia i això és tot. Per a les línies de corba, cada node tindrà "
"una nansa lateral per a permetre la corba d'aquest segment utilitzant "
"l'`algoritme de corba cúbica de Bézier <https://en.wikipedia.org/wiki/B"
"%C3%A9zier_curve#/media/File:B%C3%A9zier_3_big.gif>`_."

#: ../../reference_manual/tools/shape_edit.rst:38
msgid ""
"**What that means, in short, is that moving the side handles into a given "
"direction will make the segment curve in that direction, and the longer the "
"line of the node to the side handle, the stronger the curving.**"
msgstr ""
"**El que vol dir, en resum, és que moure les nanses laterals en una direcció "
"donada farà que el segment es corbi en aquesta direcció, i com més llarga "
"sigui la línia al costat de la nansa, més forta serà la corba.**"

#: ../../reference_manual/tools/shape_edit.rst:41
msgid "Selecting Nodes for Editing"
msgstr "Seleccionar nodes per editar"

#: ../../reference_manual/tools/shape_edit.rst:43
msgid ""
"You can select a single node with |mouseleft|, they will turn bright green "
"if selected."
msgstr ""
"Podreu seleccionar un sol node fent |mouseleft|, si se selecciona es tornarà "
"de color verd brillant."

#: ../../reference_manual/tools/shape_edit.rst:45
msgid ""
"|mouseleft| :kbd:`+ Shift` on unselected nodes will add them to a selection."
msgstr ""
":kbd:`Fer` |mouseleft| :kbd:`+ Majús.` sobre els nodes sense seleccionar els "
"afegirà a una selecció."

#: ../../reference_manual/tools/shape_edit.rst:47
msgid ""
"|mouseleft| + drag will make a selection rectangle. All nodes whose handles "
"are touched by the rectangle will be selected. This combines with the |"
"mouseleft| :kbd:`+ Shift` shortcut above."
msgstr ""
":kbd:`Fer` |mouseleft| :kbd:`+ arrossega` crearà un rectangle de selecció. "
"Se seleccionaran tots els nodes que restin dins del rectangle. Això es "
"combina amb la drecera :kbd:`fent` |mouseleft| :kbd:`+ Majús.` a sobre."

#: ../../reference_manual/tools/shape_edit.rst:50
msgid "Selected Nodes"
msgstr "Nodes seleccionats"

#: ../../reference_manual/tools/shape_edit.rst:52
msgid ""
"You can add and remove side handles from a selected node with the |"
"mouseleft| :kbd:`+ Shift` shortcut."
msgstr ""
"Podeu afegir i eliminar les nanses laterals d'un node seleccionat amb la "
"drecera :kbd:`fent` |mouseleft| :kbd:`Majús.`."

#: ../../reference_manual/tools/shape_edit.rst:54
msgid ""
"Krita has several node-types that allow you control the side handles more "
"efficiently. These are the corner, smooth and symmetric modes."
msgstr ""
"El Krita té diversos tipus de node que permeten controlar les nanses "
"laterals amb el mode més eficient. Aquests són els modes Cantonada, "
"Suavitzat i Simètric."

#: ../../reference_manual/tools/shape_edit.rst:56
msgid "Corner"
msgstr "Cantonada"

#: ../../reference_manual/tools/shape_edit.rst:57
msgid ""
"Represented by a circle, the corner type allows you to have handles that can "
"point in different directions and have different lengths."
msgstr ""
"Representat per un cercle, el tipus de cantonada permet tenir nanses que "
"poden apuntar en diferents direccions i tenir diferents longituds."

#: ../../reference_manual/tools/shape_edit.rst:58
msgid "Smooth"
msgstr "Suavitzat"

#: ../../reference_manual/tools/shape_edit.rst:59
msgid ""
"Represented by a square, the smooth type will ensure a smooth transition by "
"always pointing the handles into opposite directions, but they can still "
"have different lengths."
msgstr ""
"Representat per un quadrat, el tipus suavitzat assegurarà una transició suau "
"apuntant sempre les nanses en direccions oposades, però encara podran tenir "
"longituds diferents."

#: ../../reference_manual/tools/shape_edit.rst:61
msgid "Symmetric"
msgstr "Simètric"

#: ../../reference_manual/tools/shape_edit.rst:61
msgid ""
"Represented by a diamond, the symmetric node will force handles to always "
"point in opposite directions and have the same length."
msgstr ""
"Representat per un diamant, el node simètric forçarà les nanses a que sempre "
"apuntin en direccions oposades i que tinguin la mateixa longitud."

#: ../../reference_manual/tools/shape_edit.rst:63
msgid ""
"|mouseleft| :kbd:`+ Ctrl` on a selected node will cycle between the node-"
"types."
msgstr ""
":kbd:`Fent` |mouseleft| :kbd:`+ Ctrl` sobre un node seleccionat s'alternarà "
"entre els tipus de node."

#: ../../reference_manual/tools/shape_edit.rst:65
msgid ":kbd:`Del` will remove the selected node."
msgstr "La tecla :kbd:`Supr` eliminarà el node seleccionat."

#: ../../reference_manual/tools/shape_edit.rst:68
msgid "Selected Segments"
msgstr "Segments seleccionats"

#: ../../reference_manual/tools/shape_edit.rst:70
msgid ""
"Segments are the lines between nodes. Hovering over a segment will show a "
"dotted line, indicating it can be selected."
msgstr ""
"Els segments són les línies entre els nodes. En passar sobre un segment es "
"mostrarà una línia de punts, el qual indicarà que es pot seleccionar."

#: ../../reference_manual/tools/shape_edit.rst:72
msgid ""
"You can |mouseleft| and drag on a segment to curve it to the mouse point. "
"Clicking on different parts of the segment and dragging will curve it "
"differently."
msgstr ""
"Podeu fer |mouseleft| i arrossegar sobre un segment i es corbarà en la "
"direcció del punter. En fer clic a diferents parts del segment i arrossegant-"
"les, es corbarà de manera diferent."

#: ../../reference_manual/tools/shape_edit.rst:74
msgid ""
"Double |mouseleft| on a segment will add a node on the segment under the "
"mouse cursor. The new node will be selected."
msgstr ""
"Fer doble |mouseleft| sobre un segment afegirà un node al segment sota el "
"cursor del ratolí. Se seleccionarà el node nou."

#: ../../reference_manual/tools/shape_edit.rst:77
msgid "Other Shapes"
msgstr "Altres formes"

#: ../../reference_manual/tools/shape_edit.rst:79
msgid ""
"Shapes that aren't path shapes only have a single type of node: A small "
"diamond like, that changes the specific parameters of that shape on-canvas. "
"For example, you can change the corner radius on rectangles by dragging the "
"nodes, or make the ellipse into a pie-segment."
msgstr ""
"Les formes que no són formes de camins només tenen un sol tipus de node: un "
"diamant petit, que canvia els paràmetres específics d'aquesta forma en el "
"llenç. Per exemple, podreu canviar el radi de la cantonada en els rectangles "
"arrossegant els nodes, o fer que l'el·lipse es converteixi en un segment amb "
"sectors."

#: ../../reference_manual/tools/shape_edit.rst:82
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/shape_edit.rst:85
msgid ".. image:: images/tools/Shape-editing-tool-tool-options.png"
msgstr ".. image:: images/tools/Shape-editing-tool-tool-options.png"

#: ../../reference_manual/tools/shape_edit.rst:86
msgid ""
"Path shapes have options. The top left options are for converting to "
"different anchor point types. The bottom left options are for adding or "
"removing points. The top right options are for converting the line to "
"different types. The bottom right options are for breaking and joining line "
"segments."
msgstr ""
"Les formes de camins tenen opcions. Les opcions de la part superior esquerra "
"són per convertir a diferents tipus de punt d'ancoratge. Les opcions de la "
"part inferior esquerra són per afegir o treure punts. Les opcions de la part "
"superior a la dreta són per convertir la línia a tipus diferents. Les "
"opcions de la part inferior a la dreta són per trencar i unir segments de la "
"línia."

#: ../../reference_manual/tools/shape_edit.rst:88
msgid ""
"The tool options of the Edit Shapes Tool change depending on the type of "
"shape you have selected. With the exception of the path shape, all shapes "
"have a :guilabel:`Convert to Path` action, which converts said shape to a "
"path shape."
msgstr ""
"Les Opcions de l'eina de l'eina Edita les formes canviaran segons el tipus "
"de forma que s'hagi seleccionat. Amb l'excepció de la forma de camins, totes "
"les formes tenen una acció :guilabel:`Converteix a camí`, la qual convertirà "
"aquesta forma en una forma de camins."

#: ../../reference_manual/tools/shape_edit.rst:93
msgid ""
"|toolbeziercurve|, |toolline|, |toolpolyline|, |toolpolygon|, |"
"toolfreehandpath|"
msgstr ""
"|toolbeziercurve|, |toolline|, |toolpolyline|, |toolpolygon|, |"
"toolfreehandpath|"

#: ../../reference_manual/tools/shape_edit.rst:95
msgid ""
"Path shapes are the most common shape and can be made with the following "
"tools:"
msgstr ""
"Les formes de camins són la forma més comuna i es poden crear amb les eines "
"següents:"

#: ../../reference_manual/tools/shape_edit.rst:97
msgid ":ref:`path_tool`"
msgstr ":ref:`path_tool`"

#: ../../reference_manual/tools/shape_edit.rst:98
msgid ":ref:`line_tool`"
msgstr ":ref:`line_tool`"

#: ../../reference_manual/tools/shape_edit.rst:99
msgid ":ref:`polygon_tool`"
msgstr ":ref:`polygon_tool`"

#: ../../reference_manual/tools/shape_edit.rst:100
msgid ":ref:`polyline_tool`"
msgstr ":ref:`polyline_tool`"

#: ../../reference_manual/tools/shape_edit.rst:101
msgid ":ref:`freehand_path_tool`"
msgstr ":ref:`freehand_path_tool`"

#: ../../reference_manual/tools/shape_edit.rst:104
msgid "Edit the nodes."
msgstr "Edita els nodes."

#: ../../reference_manual/tools/shape_edit.rst:106
msgid "Corner Point"
msgstr "Punt de cantonada"

#: ../../reference_manual/tools/shape_edit.rst:107
msgid ""
"Make the selected node a corner or cusp. This means that the side handles "
"can point in different directions and be different lengths."
msgstr ""
"Convertirà el node seleccionat en una cantonada o cúspide. Això vol dir que "
"les nanses laterals poden apuntar en diferents direccions i tenir longituds "
"diferents."

#: ../../reference_manual/tools/shape_edit.rst:108
msgid "Smooth Point"
msgstr "Punt suau"

#: ../../reference_manual/tools/shape_edit.rst:109
msgid ""
"Make the selected node smooth. The two side handles will always point in "
"opposite directions, but their length can be different."
msgstr ""
"Farà suau el node seleccionat. Les dues nanses laterals sempre apuntaran en "
"direccions oposades, però la seva longitud podrà ser diferent."

#: ../../reference_manual/tools/shape_edit.rst:110
msgid "Symmetric Point"
msgstr "Punt simètric"

#: ../../reference_manual/tools/shape_edit.rst:111
msgid ""
"Make the selected node smooth. The two side handles will always point in "
"opposite directions, and their length will stay the same."
msgstr ""
"Farà suau el node seleccionat. Les dues nanses laterals sempre apuntaran en "
"direccions oposades, però la seva longitud es mantindrà la mateixa."

#: ../../reference_manual/tools/shape_edit.rst:112
msgid "Insert Point"
msgstr "Insereix un punt"

#: ../../reference_manual/tools/shape_edit.rst:113
msgid "Insert a new node into the middle of the selected segment."
msgstr "Introduirà un node nou al mig del segment seleccionat."

#: ../../reference_manual/tools/shape_edit.rst:115
msgid "Node Editing"
msgstr "Editar el node"

#: ../../reference_manual/tools/shape_edit.rst:115
msgid "Remove Point"
msgstr "Elimina el punt"

#: ../../reference_manual/tools/shape_edit.rst:115
msgid "Remove the selected node."
msgstr "Eliminarà el node seleccionat."

#: ../../reference_manual/tools/shape_edit.rst:118
msgid "Edit line segments between nodes."
msgstr "Editar els segments de la línia entre els nodes."

#: ../../reference_manual/tools/shape_edit.rst:120
msgid "Segment To Line"
msgstr "Segment a la línia"

#: ../../reference_manual/tools/shape_edit.rst:121
msgid "Make the current segment a straight line."
msgstr "Convertirà el segment actual en una línia recta."

#: ../../reference_manual/tools/shape_edit.rst:122
msgid "Segment To Curve"
msgstr "Segment a la corba"

#: ../../reference_manual/tools/shape_edit.rst:123
msgid ""
"Make the current segment a curve: It'll add side handles for this segment to "
"the nodes attached to it."
msgstr ""
"Convertirà el segment actual en una corba: afegirà nanses laterals per a "
"aquest segment al costat dels nodes."

#: ../../reference_manual/tools/shape_edit.rst:124
msgid "Make Line Point"
msgstr "Fes punt de línia"

#: ../../reference_manual/tools/shape_edit.rst:125
msgid ""
"Turn the selected node into a sharp corner: This will remove the side "
"handles."
msgstr ""
"Convertirà el node seleccionat en una cantonada més definida: Això eliminarà "
"les nanses laterals."

#: ../../reference_manual/tools/shape_edit.rst:126
msgid "Make Curve Point"
msgstr "Fes punt de corba"

#: ../../reference_manual/tools/shape_edit.rst:127
msgid ""
"Turn the selected node into one that can curve: This will add side handles "
"to the node."
msgstr ""
"Convertirà el node seleccionat en un que es podrà corbar: Això afegirà "
"nanses laterals al node."

#: ../../reference_manual/tools/shape_edit.rst:128
msgid "Break at Point"
msgstr "Trenca al punt"

#: ../../reference_manual/tools/shape_edit.rst:129
msgid "Break the path at this point."
msgstr "Trenca el camí en aquest punt."

#: ../../reference_manual/tools/shape_edit.rst:130
msgid "Break Segment"
msgstr "Trenca al segment"

#: ../../reference_manual/tools/shape_edit.rst:131
msgid "Break the path at the selected segment."
msgstr "Trenca el camí al segment seleccionat."

#: ../../reference_manual/tools/shape_edit.rst:132
msgid "Join with Segment"
msgstr "Uneix amb un segment"

#: ../../reference_manual/tools/shape_edit.rst:133
msgid "Join two nodes that are only attached on one side with a segment."
msgstr "Uneix dos nodes que només estan units per un costat amb un segment."

#: ../../reference_manual/tools/shape_edit.rst:135
msgid "Line Segment Editing"
msgstr "Editar els segments a la línia"

#: ../../reference_manual/tools/shape_edit.rst:135
msgid "Merge Points"
msgstr "Fusiona els punts"

#: ../../reference_manual/tools/shape_edit.rst:135
msgid ""
"Merge two nodes into one, if the nodes are adjacent or if both nodes are "
"only attached on one side with a segment."
msgstr ""
"Fusiona dos nodes en un, si els nodes són adjacents o si ambdós només estan "
"units per un costat amb un segment."

#: ../../reference_manual/tools/shape_edit.rst:138
msgid "Rectangle Shapes"
msgstr "Formes de rectangle"

#: ../../reference_manual/tools/shape_edit.rst:140
msgid "|toolrectangle|"
msgstr "|toolrectangle|"

#: ../../reference_manual/tools/shape_edit.rst:142
msgid ""
"Rectangle shapes are the ones made with the :ref:`rectangle_tool`. It has "
"extra options to make rounded corners easy."
msgstr ""
"Les formes de rectangle són les fetes amb l':ref:`rectangle_tool`. Disposa "
"d'opcions addicionals per a crear cantonades arrodonides."

#: ../../reference_manual/tools/shape_edit.rst:144
msgid "Corner radius x"
msgstr "X del radi del vèrtex"

#: ../../reference_manual/tools/shape_edit.rst:145
msgid "The radius of the x-axis of the corner curve."
msgstr "El radi de l'eix «X» de la corba de cantonada."

#: ../../reference_manual/tools/shape_edit.rst:147
msgid "Corner radius y"
msgstr "Y del radi del vèrtex"

#: ../../reference_manual/tools/shape_edit.rst:147
msgid "The radius of the y-axis of the corner curve."
msgstr "El radi de l'eix «Y» de la corba de cantonada."

#: ../../reference_manual/tools/shape_edit.rst:150
msgid "Ellipse Shapes"
msgstr "Formes d'el·lipse"

#: ../../reference_manual/tools/shape_edit.rst:152
msgid "|toolellipse|"
msgstr "|toolellipse|"

#: ../../reference_manual/tools/shape_edit.rst:154
msgid "Ellipse shapes are the ones made with the :ref:`ellipse_tool`."
msgstr "Les formes d'el·lipse són les que es creen amb l':ref:`ellipse_tool`."

#: ../../reference_manual/tools/shape_edit.rst:157
msgid "The type of ellipse shape it is."
msgstr "El tipus de forma que serà l'el·lipse."

#: ../../reference_manual/tools/shape_edit.rst:159
msgid "Arc"
msgstr "Arc"

#: ../../reference_manual/tools/shape_edit.rst:160
msgid "An arc shape will keep the path open when it isn't fully circular."
msgstr ""
"Una forma d'arc mantindrà el camí obert quan no sigui completament circular."

#: ../../reference_manual/tools/shape_edit.rst:161
msgid "Pie"
msgstr "Sector"

# skip-rule: kct-cut
#: ../../reference_manual/tools/shape_edit.rst:162
msgid ""
"A pie shape will add two extra lines to the center when the shape isn't "
"fully circular, like how one cuts out a piece from a pie."
msgstr ""
"Una forma de sector afegirà dues línies addicionals al centre quan la forma "
"no sigui completament circular, com si es tallés una peça d'un pastís."

#: ../../reference_manual/tools/shape_edit.rst:164
msgid "Type"
msgstr "Tipus"

#: ../../reference_manual/tools/shape_edit.rst:164
msgid "Cord"
msgstr "Cordó"

#: ../../reference_manual/tools/shape_edit.rst:164
msgid ""
"A cord shape will add a straight line between the two ends if the path isn't "
"fully circular, as if a cord is being strung between the two points."
msgstr ""
"Una forma de cordó afegirà una línia recta entre els dos extrems si el camí "
"no és completament circular, com si es tingués una corda entre els dos punts."

#: ../../reference_manual/tools/shape_edit.rst:166
msgid "Start Angle"
msgstr "Angle inicial"

#: ../../reference_manual/tools/shape_edit.rst:167
msgid "The angle at which the shape starts."
msgstr "L'angle en el que comença la forma."

#: ../../reference_manual/tools/shape_edit.rst:168
msgid "End Angle"
msgstr "Angle final"

#: ../../reference_manual/tools/shape_edit.rst:169
msgid "The angle at which the shape ends."
msgstr "L'angle en el que finalitza la forma."

#: ../../reference_manual/tools/shape_edit.rst:171
msgid "An action to quickly make the ellipse fully circular."
msgstr "Una acció per a crear ràpidament una el·lipse completament circular."
